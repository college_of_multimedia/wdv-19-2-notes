var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    jsmin = require('gulp-jsmin');

gulp.task('javascript', function () {
    return gulp.src('assets/javascript/*.js')
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./javascript/'))
});

gulp.task('styles', function () {
    return gulp.src('assets/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./css/'))
});

gulp.task('watch', function () {
    gulp.watch('assets/sass/*.scss', gulp.series('styles'));
    gulp.watch('assets/javascript/*.js', gulp.series('javascript'));
});

gulp.task('default', gulp.parallel('styles', 'javascript', 'watch'));