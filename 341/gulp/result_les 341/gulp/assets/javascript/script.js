var maxLeft = window.innerWidth - 200,
    maxTop = window.innerHeight - 50,
    mouseTarget = document.getElementById('button');

mouseTarget.addEventListener('mouseenter', e => {

    let leftPos = Math.floor(Math.random() * maxLeft),
        topPos = Math.floor(Math.random() * maxTop);

    console.log('left: ' + leftPos + ', top: ' + topPos);

    mouseTarget.style.position = 'absolute';
    mouseTarget.style.top = topPos + 'px';
    mouseTarget.style.left = leftPos + 'px';
    mouseTarget.style.border = '1px dotted #f00';
});

mouseTarget.addEventListener('mouseleave', e => {
    mouseTarget.style.border = '1px solid #333';
});

console.log('Gulp init');
