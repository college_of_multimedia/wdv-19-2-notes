<?php

$start = 33;
$eind = 543;

// optimale versie met maar 4 vragen
for ($teller = $start; $teller <= $eind; $teller++){
	// maak een lege string
	$uitvoer = '';
	if ($teller % 5 == 0) $uitvoer .= 'PHP';
	// als deelbaar door 5 breidt de string uit met PHP
	if ($teller % 9 == 0) $uitvoer .= 'MySQL';
	// als deelbaar door 9 breidt de string uit met MySQL
	if (!$uitvoer) $uitvoer = $teller;
	// als geen tekst dan dus nummer
	if ($teller % 2 == 0) {
	// als deelbaar door 2 gebruik bold
		$uitvoer = '<b>' . $uitvoer . '</b>';
	}
	echo $uitvoer . "<br>\n";
} 


// iets minder optimale versie met maar 4 vragen
for ($teller = $start; $teller <= $eind; $teller++){
	$uitvoer = $teller;
	if ($teller % 9 == 0 && $teller % 5 == 0) $uitvoer = 'PHPMySQL';
	// als beide dan dus string met PHPMySQL
	elseif ($teller % 5 == 0) $uitvoer = 'PHP';
	// als deelbaar door 5 vult de string met PHP
	elseif ($teller % 9 == 0) $uitvoer = 'MySQL';
	// als deelbaar door 9 vult de string met MySQL
	
	if ($teller % 2 == 0) {
	// als deelbaar door 2 gebruik bold
		$uitvoer = '<b>' . $uitvoer . '</b>';
	}
	echo $uitvoer . "<br>\n";
} 


