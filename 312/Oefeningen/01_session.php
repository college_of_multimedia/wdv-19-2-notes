<h1>Sessies</h1>
<h2>Opdracht 1</h2>
<p>
	Maak op een nieuwe php pagina een HTML formulier waarbij ik een naam en wachtwoord kan invoeren.<br>
	Als ik op 'verstuur' klik dan moet ik op dezelfde pagina komen.<br>
</p>

<h2>Opdracht 2</h2>
<p>
	Na het vertsuren moeten de ingevulde waardes weer zichtbaar zijn in het formulier.<br>
</p>

<h2>Opdracht 3</h2>
<p>
	Als ik opnieuw op deze pagina kom moeten de ingevulde gegevens nog steeds zichtbaar zijn.<br>
    Dus niet na een page refresh, maar als je opnieuw naar dezelfde url gaat zonder de browser te sluiten.
</p>

<h2>Opdracht 4</h2>
<p>
	Vergelijk de ingevuld gegevens met een standaard waarde om zo te zien of de juiste gegevens ingevuld zijn<br>
    Als de naam of het wachtwoord neit overeen komen dan moet je hiervan een melding geven.
</p>


