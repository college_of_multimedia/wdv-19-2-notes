<?php require_once '../Library/settings.php' ?>
<?php  
	if (!session_id()) session_start();

	if (isset($_GET['logout'])){
		// dus NU uitloggen!!!
		// de cookie reset!!!
		setcookie('bgcolor', '', time() - 42000);
		// de sessie variabelen reset!
		$_SESSION = array();

		if (ini_get('session.use_cookies')){
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000, 
				$params['path'], $params['domain'],
				$params['secure'], $params['httponly']
			);
		}

		session_destroy();
		$securePage  = false;
	
		if ($securePage){
			header( 'Location: ' . WEB_URL );
		} else {
			header( 'Location: ' . htmlspecialchars($_SERVER['PHP_SELF']) );
		}
		// dus ZONDER ?logout in de url
		exit();
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<h1>Sessies</h1>
<h2>Opdracht 1</h2>
<p>
	Maak op een nieuwe php pagina een HTML formulier waarbij ik een naam en wachtwoord kan invoeren.<br>
	Als ik op 'verstuur' klik dan moet ik op dezelfde pagina komen.<br>
</p>




<form method="post">
	username: <input type="text" name="username" value="<?= getUsername(); ?>">
	password: <input type="password" name="password">
	<button>login</button>
</form>
<?php  

if (isset($_POST['username']) && !empty($_POST['username']) 
	&& isset($_POST['password']) && !empty($_POST['password']) ){
	// en hier testen in de database !!!!
	$_SESSION['user_credentials']['user'] = $_POST['username'];
	$_SESSION['user_credentials']['user_id'] = 23;
	$_SESSION['logged_in'] = true;
	setcookie('bgcolor', 'green', time()+6000000 ); // month
}

if ( isLoggedIn() ){
	echo "<pre>";
	print_r($_SESSION);
	echo "</pre>";
}

function getUsername() {
	if ( isLoggedIn() ){
		return $_SESSION['user_credentials']['user'];
	} 
	return '';
}

function isLoggedIn() {
	if (isset($_SESSION['logged_in'])){
		return true;
	}
	return false;
}

/*
unset ($_SESSION['user']);
unset ($_SESSION['user_id']);
unset ($_SESSION['logged_in']);
*/
	if (isLoggedIn()) {
		$niceName = ucfirst(trim($_SESSION['user_credentials']['user']));
	} else {
		$niceName = 'Gast';
	}
echo "<h2>Welkom $niceName op deze pagina</h2>";
echo "<h3>session_id: " . session_id() . "</h3>";

$bgcolor = 'red';
if (isset($_COOKIE['bgcolor'])){
	$bgcolor = $_COOKIE['bgcolor'];
}

?>

<h2>Opdracht 2</h2>
<p style="padding:5px; background-color: <?= $bgcolor ?>;">
	Na het vertsuren moeten de ingevulde waardes weer zichtbaar zijn in het formulier.<br>
</p>

<h2>Opdracht 3</h2>
<p>
	Als ik opnieuw op deze pagina kom moeten de ingevulde gegevens nog steeds zichtbaar zijn.<br>
    Dus niet na een page refresh, maar als je opnieuw naar dezelfde url gaat zonder de browser te sluiten.
</p>

<h2>Opdracht 4</h2>
<p>
	Vergelijk de ingevuld gegevens met een standaard waarde om zo te zien of de juiste gegevens ingevuld zijn<br>
    Als de naam of het wachtwoord neit overeen komen dan moet je hiervan een melding geven.
</p>

<form action="" method="get">
	<button name="logout">uitloggen</button>
</form>	

<!-- of zoals hieronder -->

<a href="?logout">uitloggen</a>

</body>
</html>


