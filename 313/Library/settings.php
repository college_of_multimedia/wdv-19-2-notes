<?php
$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);
// error_reporting(E_ALL & ~E_NOTICE); // alles behalve de notices

$br = "<br>\n";

define('LIB_ROOT', 		realpath( __DIR__ ) 					);
// dit is ~/Sites/deSlager/Library
define('SITE_ROOT',		realpath( LIB_ROOT . '/..' ) 			);
// dit is ~/Sites/deSlager

// we zoeken nu de rootfolder! (public, public_html, htdocs)
$rootfolder = explode('/', substr($_SERVER['SCRIPT_FILENAME'], strlen(SITE_ROOT)+1))[0];
// array('pubblic','index.php') dus public

define('DOCUMENT_ROOT',	realpath( SITE_ROOT . '/' . $rootfolder )	);
// dit is ~/Sites/deSlager/public
define('IMG_ROOT',		realpath( DOCUMENT_ROOT . '/images' ) 		);
define('INC', 			realpath( LIB_ROOT . '/includes' ) 			);

// dit is ~/Sites/deSlager/public/www2

$web_path = '';
if (DOCUMENT_ROOT !== $_SERVER['DOCUMENT_ROOT'])
	$web_path = substr(DOCUMENT_ROOT , strlen($_SERVER['DOCUMENT_ROOT']));

$scheme = (isset($_SERVER['REQUEST_SCHEME'])) ? $_SERVER['REQUEST_SCHEME'] . '://' : '//';

define('WEB_URL', 	$scheme . $_SERVER['HTTP_HOST'] . $web_path	);
// auto http:// of https://
define('IMG_URL', 	WEB_URL . '/images'			);
define('CSS_URL', 	WEB_URL . '/css'			);
define('JS_URL', 	WEB_URL . '/js'				);
define('PAGE_URL', 	WEB_URL . '/pages'			);

// __DIR__ is het path van het huidige bestand!!
// __FILE__ is het path en naam van het huidige bestand!!
// $_SERVER['SCRIPT_FILENAME'] is het volledige path naar je pagina deSlager/public/index.php
// zie PATH_SEPARATOR voor / of \

session_start();
// require_once './functions.php'; // algemene functies







