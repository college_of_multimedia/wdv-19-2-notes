<?php



// Met deze manier maak je single line comments...

// $naam is om ... voor neerzetten waarom....
$naam = 'Harald'; 
$leeftijd = '23';
/* ook dit dus
meerdere regels met 
commentaar vullen
enzo
 */

/**
 * Doc Block
 *
 * 
 */

# jaja ook deze mag in PHP

$kids = ['Davey', 'Robin'];

$br = "<br>\n";

echo $leeftijd++       ; // achteraf!!!
echo ++$leeftijd       ; // meteen!!!

echo $leeftijd       ;

$leeftijd += 10;     // $leeftijd = $leeftijd + 10;

// $naam .= " Schilling";

echo $br;

echo "$naam Schilling";

echo $br;

echo '$naam Schilling';

echo $br;

var_dump( $kids  );

echo $br;

print_r( $kids );

echo $br;

var_dump( $leeftijd == '35' ); 
// loose test op waarde niet data-type

echo $br;

var_dump( $leeftijd === '35' ); 
// exacte test op waarde en data-type


echo $br;

var_dump( $leeftijd == '35' AND 23 == '21'); 
// && == AND == and

echo $br;

var_dump( $leeftijd == '35' OR 23 == '21'); 
// || == OR == or

// if ($leeftijd >= 53) echo $br . 'Hoi het is waar' . $br;

if ($leeftijd >= 25) 
	echo 	$br 
			. 'Hoi het is waar' 
			. $br;
echo 'Maar deze regel zie je altijd!!!' . $br;


if ($leeftijd >= 55) {
	echo 	$br 
			. 'Hoi het is waar' 
			. $br;
	echo 'Maar deze regel zie je niet altijd!!!' . $br;
}


if ($leeftijd >= 25):
	echo 	$br 
			. 'Hoi het is toch waar' 
			. $br;
	echo 'Maar deze regel zie je toch niet altijd!!!' . $br;
endif;


if ($leeftijd >= 25):
	// jump in-out van php -> html en weer terug....
?>
	<!-- dit is een conditioneel html blok -->
	<h2>Hee een h2!</h2>
	<p>Dit is een paragraaf van <?= $naam ?></p>
	<!-- dit is einde conditioneel html blok -->
<?php

endif;


if ($leeftijd < 23) {
	echo 'het is te jong';
} elseif ($leeftijd > 65) {
	echo 'het is te oud';
} else {
	echo 'het is te goed';
}


$groet = ($order >= 2) ? 'Welkom back' : 'Welkom';

$style = $getal % 2 ? ' class="even" ' : '';

echo "<p$style>blabla</p>";
//<p>blabla</p>
//<p class="even">blabla</p>













