<?php require_once ('../Library/settings.php'); ?>

<h1>HTML Formulier</h1>
<h2>Opdracht 1</h2>
<p>
	Maak een HTML formulier waarbij ik een voornaam, tussenvoegsel, achternaam, email en postcode kan invoeren<br>
	Ook wil ik een woonplaats kunnen selecteren <em>( 3 opties zijn genoeg )</em><br>
	Als ik op 'verstuur' klik dan moet ik op dezelfde pagina komen<br>
</p>

<h2>Opdracht 2</h2>
<p>
	Na het versturen moeten de ingevulde waardes weer zichtbaar zijn in het formulier.<br>
    Ook de juiste selectie optie moet geselecteerd worden
</p>

<h2>Opdracht 3</h2>
<p>
	Laat het formulier nu naar een nieuwe pagina verwijzen<br>
    Geef op deze nieuwe pagina de ingevulde waardes weer.
</p>

<!-- opdracht 1 kaal formulier -->
<?php
// dit is opdracht 2:

/**
 * testValue controleert en saniteert de waarde van een formulierveld
 * @param  string $naamVeld naam van het veld
 * @return string           waarde van het veld of lege string
 */
function testValue($naamVeld){
	if ( isset($_GET[$naamVeld]) && !empty($_GET[$naamVeld]) ) {
		return trim( $_GET[$naamVeld] );
	}
	return '';
}

/**
 * testOption controleert een select option
 * @param  string $naamVeld naam van het veld
 * @param  string $value    waarde van het veld
 * @return string           selected of lege string
 */
function testOption($naamVeld, $value){
	if ( isset($_GET[$naamVeld]) && !empty($_GET[$naamVeld]) && $value == $_GET[$naamVeld] ) {
		return ' selected ';
	}
	return '';
}

/**
 * testRadio controleert een radiobutton
 * @param  string $naamVeld naam van het veld
 * @param  string $value    waarde van het veld
 * @return string           checked of lege string
 */
function testRadio($naamVeld, $value){
	if ( isset($_GET[$naamVeld]) && !empty($_GET[$naamVeld]) && $value == $_GET[$naamVeld] ) {
		return ' checked ';
	}
	return '';
}

/**
 * testCheck controleert een checkbox in een Array
 * @param  string $naamVeld naam van het veld, moet een Array zijn !
 * @param  string $value    waarde van het veld
 * @return string           checked of lege string
 */
function testCheck($naamVeld, $value){
	if ( isset($_GET[$naamVeld]) && !empty($_GET[$naamVeld]) && in_array($value, $_GET[$naamVeld] ) ) {
		return ' checked ';
	}
	return '';
}


?>
<style>
	label {
		display: inline-block;
		width: 100px;
	}
</style>

<form action="" method="get" name="myForm" id="myForm">
	
	<label for="voornaam">Voornaam:</label>
	<input type="text" name="voornaam" id="voornaam" value="<?= testValue('voornaam'); ?>"><br>
	

	<label for="tussenvoegsel">Tussenvoegsel:</label>
	<input type="text" name="tussenvoegsel" id="tussenvoegsel" value="<?= testValue('tussenvoegsel'); ?>"><br>
	

	<label for="achternaam">Achternaam:</label>
	<input type="text" name="achternaam" id="achternaam" value="<?= testValue('achternaam'); ?>"><br>
	

	<label for="email">Email:</label>
	<input type="text" name="email" id="email" value="<?= testValue('email'); ?>"><br>
	

	<label for="postcode">Postcode:</label>
	<input type="text" name="postcode" id="postcode" value="<?= testValue('postcode'); ?>"><br>
	

	<label for="woonplaats">Woonplaats</label>
	<select name="woonplaats">
		<option value="Amsterdam"<?= testOption('woonplaats','Amsterdam') ?>>Amsterdam</option>
		<option value="Rotterdam"<?= testOption('woonplaats','Rotterdam') ?>>Rotterdam</option>
		<option value="Utrecht"<?= testOption('woonplaats','Utrecht') ?>>Utrecht</option>
	</select>
	<br>
	

	<label for="gender">Gender:</label>
	<input type="radio" name="gender" id="gender_m" value="Man"<?= testRadio('gender','Man') ?>>Man - 
	<input type="radio" name="gender" id="gender_v" value="Vrouw"<?= testRadio('gender','Vrouw') ?>>Vrouw - 
	<input type="radio" name="gender" id="gender_n" value="Neutraal"<?= testRadio('gender','Neutraal') ?>>Neutraal<br>
		
	<label for="mac">Computer:</label>
	<input type="checkbox" name="computers[]" id="mac" value="Mac"<?= testCheck('computers','Mac') ?>>Mac<br>
	<label for="pc"></label>
	<input type="checkbox" name="computers[]" id="pc" value="PC"<?= testCheck('computers','PC') ?>>PC<br> 
	<label for="unix"></label>
	<input type="checkbox" name="computers[]" id="unix" value="Unix"<?= testCheck('computers','Unix') ?>>Unix<br>
	<br>

	<label for="verstuur"></label>
	<input type="submit" name="verstuur" id="verstuur" value="Verstuur"> 
	<input type="reset" name="reset" id="reset" value="Reset"><br>
	<br>

	<br>

</form>
