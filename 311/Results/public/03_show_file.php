<h1>Bestanden versturen</h1>
<h2>Opdracht 1</h2>
<p>
    Maak een nieuwe php pagina met daarin een html formulier<br>
    In dit formulier moet het mogelijk zijn om een bestand te selecteren en te versturen<br>
</p>

<style>
	label {
		display: inline-block;
		width: 50px;
	}
</style>

<!-- action="" of action=" htmlspecialchars($_SERVER['PHP_SELF']); (als php echo!)"-->
<form 
	action="<?= htmlspecialchars($_SERVER['PHP_SELF']); ?>"
	method="post"
	enctype="multipart/form-data"
	id="mijnForm"
	nam="mijnForm" >
	<label for="bestand">File:</label>
	<input type="file" name="bestand" id="bestand"><br>
	<label for="verstuur"></label>
	<input type="submit" name="verstuur" id= "verstuur" value="Verstuur"><br>
</form>

<pre>
<?php
	// var_dump($_POST);
	var_dump($_FILES);
?>
</pre>

<h2>Opdracht 2</h2>
<p>
    Sla het verstuurde bestand op als het verstuurd is<br>
    v.b.: <br>
    move_uploaded_file( $_FILES['bestandje']['tmp_name'], $_FILES['bestandje']['name'] );
</p>
<?php
	// $file = $_FILES['bestand'];
	// move_uploaded_file($file['tmp_name'], $uploaddir . $file['name']);
?>


<h2>Opdracht 3</h2>
<p>
    Zorg ervoor dat er alleen png en jpg bestanden verstuurd kunnen worden.<br>
    array('image/jpeg', 'image/gif', 'image/png');
</p>

<?php

$uploaddir = __DIR__ . '/upload/';
$max_filesize = 15240;
$naam_klant = "harald"; // de username uit de database
$validMimeTypes = array('image/jpeg', 'image/gif', 'image/png');

// $validMimeTypes = array('image/jpeg' => '.jpg' , 'image/gif' => '.gif', 'image/png' => '.png');

if (isset($_FILES['bestand']) && 0 === $_FILES['bestand']['error']){
	$file = $_FILES['bestand'];

	if ( in_array($file['type'], $validMimeTypes )
		 && $file['size'] <= $max_filesize){
		$nieuwenaam = $uploaddir . $naam_klant . '_' . $file['name'];
		if (file_exists($nieuwenaam)){
			echo '<p>bestand! bestaat al!<p>' . "\n";
		} else {
			move_uploaded_file($file['tmp_name'], $nieuwenaam );
		}
	} else {
		echo '<p>Ongeldig bestandstype! of te groot bestand! <p>' . "\n";
	}
}

?>


<h2>Opdracht 4</h2>
<p>
    Zorg ervoor dat de bestanden nooit overschreven worden.<br>
</p>

<?php
// file_exists ( string $filename ) 

?>