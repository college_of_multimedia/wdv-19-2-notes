<h1>HTML Formulier Validatie</h1>
<h2>Opdracht 1</h2>
<p>
	Op basis van opgave 01_html_formulier komen nu de volgende validaties:<br>
	- Is het email een geldig email adres?<br>
	- Als het niet een echt email adres is dan moet er een waarschuwing weergegeven worden bij het email veld.<br>
	- maar ... het veld moet wel ingevuld blijven met de waarde die ik verstuurd heb<br>
    filter_var( $email, FILTER_VALIDATE_EMAIL );
</p>


<h2>Opdracht 2</h2>
<p>
	Als de ingevoerde data valid is, dus door de controles heen komt dan:<br>
	Moet je een bedankt pagina laten zien, anders zie je dus nog steeds het formulier.<br>
	Zorg dat deze pagina een losse php pagina is zodat die goed opgemaakt kan worden.<br>
</p>