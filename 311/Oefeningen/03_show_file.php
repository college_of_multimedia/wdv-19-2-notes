<h1>Bestanden versturen</h1>
<h2>Opdracht 1</h2>
<p>
    Maak een nieuwe php pagina met daarin een html formulier<br>
    In dit formulier moet het mogelijk zijn om een bestand te selecteren en te versturen<br>
</p>

<h2>Opdracht 2</h2>
<p>
    Sla het verstuurde bestand op als het verstuurd is<br>
    v.b.: <br>
    move_uploaded_file( $_FILES['bestandje']['tmp_name'], $_FILES['bestandje']['name'] );
</p>

<h2>Opdracht 3</h2>
<p>
    Zorg ervoor dat er alleen png en jpg bestanden verstuurd kunnen worden.<br>
    array('image/jpeg', 'image/gif', 'image/png');
</p>

<h2>Opdracht 4</h2>
<p>
    Zorg ervoor dat de bestanden nooit overschreven worden.<br>
</p>