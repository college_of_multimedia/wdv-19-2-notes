<?php 

$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";
$nl = "\n";
$tb = "\t";

define ('DB_HOST', 'localhost'	);
define ('DB_USER', 'root'		);
define ('DB_PASS', 'root'		);
define ('DB_NAME', 'cmm_autos'	);

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
	if ( $_mysqli->connect_errno ) {
		die ('Failed to connect to database ' . $_mysqli->connect_errno);
	}
	return $_mysqli;
}

/**
 * getAuto Haalt een auto op uit de database en stuurt deze als array terug
 * 
 * @param 	\mysqli $mysqli 	de verbinding met de database
 * @param  	integer $auto_id 	het id van de auto
 * 
 * @return 	array          		de parameters van de auto in een array
 * @throws 	\Exception
 */
function getAuto($mysqli, $auto_id){
	if ( ! is_int($auto_id) || empty($auto_id) ){
		throw new Exception('Ongeldige Auto');
	}
	
	$query = 'SELECT * FROM autos WHERE id=?';

	if ( ! $stmt = $mysqli->prepare($query) ) {
		throw new Exception('Prepare Failed: ' . $mysqli->errno);
	}

	if ( ! $stmt->bind_param('i', $auto_id) ) {
		throw new Exception('Bind Failed: ' . $mysqli->errno);
	}

	if ( ! $stmt->execute() ) {
		throw new Exception('Execute Failed: ' . $mysqli->errno);
	}

	$result = $stmt->get_result();
	
	if ( empty($result->num_rows) ){
		// throw new Exception('Geen auto gevonden');
		return [];
	}
	
	$auto = $result->fetch_assoc();
	return $auto;
}



/**
 * sanit_input functie maakt de value van een formulier veilger
 * en geeft dat resultaat terug
 * 
 * @param  string $data 
 * @return string       
 */
function sanit_input($data){
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Show a car</title>
	<style type="text/css">
		label {
			display:inline-block;
			width: 100px;
		}
	</style>

</head>
<body>
<?php
$id = 0;

if (isset( $_GET['id'])){
	$id = intval( sanit_input( $_GET['id'] ) );
}

try {
	$mysqli = connectToDB();
	$auto = getAuto($mysqli, $id);
} catch (Exception $e) {
	echo 'opgevangen fout: ', $e->getMessage(), $nl;
	// die('</body></html>');
}

// var_dump($auto);


// IS LEVENSGEVAAAAAARLIJK
// in het echt NOOOIT gebruiken
if (isset($_POST['id'])){
	$auto = $_POST;
}


if (! empty($auto)){
	echo '<form method="post" name="mijnForm" id="mijnForm">';
	foreach ($auto as $key => $value){

		if ('submit' == $key) continue;

		if ('id' == $key) {
		 	echo '<input type="hidden" name="' . $key . '" id="' . $key . '" value="' . $value . '">', $nl;
		 	continue;
		} 

		echo $tb,'<label for="' . $key . '">' . $key . '</label>',$nl;
		echo $tb,'<input type="text" name="' . $key . '" id="' . $key . '" value="' . $value . '">', $br;
	}
	echo $tb,'<input type="submit" name="submit" value="Input">';
	echo $tb,'<input type="submit" name="submit" value="Update">';
	echo $tb,'<input type="submit" name="submit" value="Delete">';

	echo '</form>';

} else {
	// null autos ???
}

echo '<pre>';
print_r($_GET);
print_r($_POST);
echo '</pre>';

?>	
</body>
</html>

