<?php 

$debug = true;

ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";

echo "<pre>\n";

// mysqli_connect(host, user, password, database, port, socket);
$mysqlConnection = mysqli_connect('localhost', 'root', 'root', 'cmm_nieuwsberichten');

if ( mysqli_connect_errno() ) {
	die('Er ging iets mis bla bla, namelijk: ' . mysqli_connect_errno($mysqlConnection) );
	// die('Er ging iets mis bla bla, namelijk: ' . mysqli_connect_error($mysqlConnection) );
}

// $query = 'SELECT * FROM `schrijvers` ORDER BY `name`';
$query = 'SELECT * FROM schrijvers ORDER BY naam';

$mysqlResult = mysqli_query($mysqlConnection, $query);

echo 'Aantal schrijvers: ' . mysqli_num_rows($mysqlResult) . $br;

// print_r(mysqli_fetch_assoc($mysqlResult));
$schrijversLijst = [];
while ($row = mysqli_fetch_assoc($mysqlResult)) {
	// ZOLANG er records kunnnen worden opgehaald!
	// print_r ($row);
	$schrijversLijst[] = $row;
}

// var_dump($schrijversLijst);

echo "</pre>\n";

var_dump($mysqlConnection);


