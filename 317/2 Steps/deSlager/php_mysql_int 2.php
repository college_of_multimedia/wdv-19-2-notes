<?php 

$debug = true;

ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";

echo "<pre>\n";

// mysqli_connect(host, user, password, database, port, socket);
$mysqlConnection = mysqli_connect('localhost', 'root', 'root', 'CMM_test');

if ( mysqli_connect_errno() ) {
	die('Er ging iets mis bla bla, namelijk: ' . mysqli_connect_errno($mysqlConnection) );
	// die('Er ging iets mis bla bla, namelijk: ' . mysqli_connect_error($mysqlConnection) );
}
/*
$query = 'SELECT name, fear
		 FROM people AS p, person_fear AS pf, fears AS f
		 WHERE p.personID = pf.personID 
		 AND pf.fearID = f.fearID
		 ORDER BY name';


if ( ! $prepare = $mysqlConnection->prepare( $query )){
	// DIE
	throw new Exception( 'Error: Prepare Failed: '. $mysqlConnection->error );
}

if ( ! $prepare->execute()){
	// DIE
	throw new Exception( 'Error: Execute Failed: '. $mysqlConnection->errno );
}
*/

$naam = 'Harald';
$num = 10;

if ( ! $statement = $mysqlConnection->prepare('INSERT INTO people(personID,name) VALUES(?,?)') ) {
	throw new Exception( 'Error: Prepare Failed: '. $mysqlConnection->error );
}

if ( ! $statement->bind_param('is', $num, $naam) ) {
	throw new Exception( 'Error: Bind Failed: '. $mysqlConnection->errno );
}

if ( ! $statement->execute() ) {
	throw new Exception( 'Error: Execute Failed: '. $mysqlConnection->errno );
}

$naam = 'Nog een';
$num = 11;

if ( ! $statement->bind_param('is', $num, $naam) ) {
	throw new Exception( 'Error: Bind Failed: '. $mysqlConnection->errno );
}

if ( ! $statement->execute() ) {
	throw new Exception( 'Error: Execute Failed: '. $mysqlConnection->errno );
}









