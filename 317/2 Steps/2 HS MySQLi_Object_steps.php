###### 1- maak een MySQL verbinding
# https://www.php.net/manual/en/mysqli.construct.php
###### met controle
# https://www.php.net/manual/en/mysqli.connect-error.php
# https://www.php.net/manual/en/mysqli.connect-errno.php
######

###### 2- maak een MySQL query om de data op te halen
# https://www.php.net/manual/en/mysqli.query.php
######

###### 3- bereid een query voor om uit te voeren
# https://www.php.net/manual/en/mysqli-stmt.prepare.php
######

###### 4- bind de parameters en geef aan welk type het item is
# https://www.php.net/manual/en/mysqli-stmt.bind-param.php
######

###### 5- voer de query uit
# https://www.php.net/manual/en/mysqli-stmt.execute.php
######

###### 6- haal de gegevens op
# https://www.php.net/manual/en/mysqli-stmt.get-result.php
######

###### 7- controleer of je data hebt gekregen of lege info
# https://www.php.net/manual/en/mysqli-result.num-rows.php
######

###### 8- vertaal het resultaat naar een Associatieve Array
# https://www.php.net/manual/en/mysqli-result.fetch-assoc.php
######


###### Belangrijke objecten
# https://www.php.net/manual/en/class.mysqli.php
# https://www.php.net/manual/en/class.mysqli-stmt.php
# https://www.php.net/manual/en/class.mysqli-result.php
######

###### Cleanup!
# https://www.php.net/manual/en/mysqli.close.php
# https://www.php.net/manual/en/mysqli-stmt.close.php
# https://www.php.net/manual/en/mysqli-result.free.php
###### 