<?php 

$debug = true;
ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$br = "<br>\n";
$nl = "\n";
$tb = "\t";

define ('DB_HOST', 'localhost'	);
define ('DB_USER', 'root'		);
define ('DB_PASS', 'root'		);
define ('DB_NAME', 'cmm_wd317_autos'	);

/**
 * Verbind met de database
 * @return \mysqli Object
 */
function connectToDB(){
	$_mysqli = new mysqli( DB_HOST, DB_USER, DB_PASS, DB_NAME );
	if ( $_mysqli->connect_errno ) {
		die ('Failed to connect to database ' . $_mysqli->connect_errno);
	}
	return $_mysqli;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Autos 1</title>
</head>
<body>

<h1>Gecombineerde tabellen weergeven</h1>
<p>
    Op basis van php_mysql_voorbeeld.php <br>
    wil ik uiteindelijk een combinatie zien van autos en merken<br>
</p>

<h2>Opdracht 1</h2>
<p>
	Importeer de database uit php_mysql_voorbeeld.php<br>
	Done!
</p>

<h2>Opdracht 2</h2>
<p>
	Maak verbinding met deze database en laat de merken zien.<br>
<?php 
$mysqliObject = connectToDB();

// die (var_dump($mysqliObject));

$query = 'SELECT * FROM `merken` ORDER BY `naam`';
echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

// die (var_dump($mysqliResult));

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>id</th>
		<th>merk</th>
		<th>land</th>
	</tr>', $nl;

while ($row = $mysqliResult->fetch_assoc() ){
	echo'	<tr>', $nl;
	echo'		<td>' . $row['id'] . '</td>', $nl;
	echo'		<td>' . $row['naam'] . '</td>', $nl;
	echo'		<td>' . $row['land'] . '</td>', $nl;
	echo'	</tr>', $nl;
}
echo '</table>', $nl;


?>


</p>

<h2>Opdracht 3</h2>
<p>
	Laat nu alle auto's zien in een overzicht<br>
<?php 
$query = 'SELECT * FROM `autos` ORDER BY `title`';
echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

// die (var_dump($mysqliResult));

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>id</th</th>
		<th>merk_id</th>
		<th>title</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr>', $nl;

while ($row = $mysqliResult->fetch_assoc() ){
	echo "	<tr>$nl";
	echo "		<td>{$row['id']}</td>", $nl;
	echo "		<td>{$row['merk_id']}</td>", $nl;
	echo "		<td>{$row['title']}</td>", $nl;
	echo "		<td>{$row['type']}</td>", $nl;
	echo "		<td>{$row['kleur']}</td>", $nl;
	echo "		<td>{$row['brandstof']}</td>", $nl;
	echo "		<td>{$row['zitplaatsen']}</td>", $nl;
	echo "		<td>&euro;{$row['prijs']}</td>", $nl;
	echo'	</tr>', $nl;
}
echo '</table>', $nl;

?>

</p>

<h2>Opdracht 4</h2>
<p>
	Maak een 3e tabel op de html pagina.<br>
    Hier moet een combinatie zichtbaar zijn van de auto's en merken, bv:<br>
<?php 
$query = 'SELECT autos.*, merken.naam AS merk, merken.land AS land 
		 FROM autos	
		 LEFT JOIN merken ON merk_id = merken.id';

echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

// die (var_dump($mysqliResult));

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>title</th>
		<th>merk</th>
		<th>land</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr>', $nl;

while ($row = $mysqliResult->fetch_assoc() ){
	echo'	<tr>', $nl;
	echo'		<td>' . $row['title'] . '</td>', $nl;
	echo'		<td>' . $row['merk'] . '</td>', $nl;
	echo'		<td>' . $row['land'] . '</td>', $nl;
	echo'		<td>' . $row['type'] . '</td>', $nl;
	echo'		<td>' . $row['kleur'] . '</td>', $nl;
	echo'		<td>' . $row['brandstof'] . '</td>', $nl;
	echo'		<td>' . $row['zitplaatsen'] . '</td>', $nl;
	echo'		<td>&euro;' . $row['prijs'] . '</td>', $nl;
	echo'	</tr>', $nl;
}
echo '</table>', $nl;

?>


<h2>Opdracht 5</h2>
<p>
	Kun je ervoor zorgen dat de sortering van de tabel aangepast door de een get variabele mee te sturen<br>
    Je url wordt dan bijvoorbeeld: http://cmm-lessen.dev/01_autos.php?sort=title<br>
    of http://cmm-lessen.dev/01_autos.php?sort=prijs
<?php
	$order = (isset($_GET['sort'])) ? $_GET['sort'] : 'id';

$query = 'SELECT autos.*, merken.naam AS merk, merken.land AS land 
		 FROM autos	
		 LEFT JOIN merken ON merk_id = merken.id
		 ORDER BY ' . $order;

echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

// die (var_dump($mysqliResult));

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th>title</th>
		<th>merk</th>
		<th>land</th>
		<th>type</th>
		<th>kleur</th>
		<th>brandstof</th>
		<th>zitplaatsen</th>
		<th>prijs</th>
	</tr>', $nl;

while ($row = $mysqliResult->fetch_assoc() ){
	echo'	<tr>', $nl;
	echo'		<td>' . $row['title'] . '</td>', $nl;
	echo'		<td>' . $row['merk'] . '</td>', $nl;
	echo'		<td>' . $row['land'] . '</td>', $nl;
	echo'		<td>' . $row['type'] . '</td>', $nl;
	echo'		<td>' . $row['kleur'] . '</td>', $nl;
	echo'		<td>' . $row['brandstof'] . '</td>', $nl;
	echo'		<td>' . $row['zitplaatsen'] . '</td>', $nl;
	echo'		<td>&euro;' . $row['prijs'] . '</td>', $nl;
	echo'	</tr>', $nl;
}
echo '</table>', $nl;


?>
</p>

<h2>Opdracht 6</h2>
<p>
	Kun je ervoor zorgen dat als je op de titel van een kolom klikt je sorteert op die kolom?<br>
<?php
	$order = (isset($_GET['sort'])) ? $_GET['sort'] : 'id';

$query = 'SELECT autos.*, merken.naam AS merk, merken.land AS land 
		 FROM autos	
		 LEFT JOIN merken ON merk_id = merken.id
		 ORDER BY ' . $order;

echo 'Query: ', $query, $br;

$mysqliResult = $mysqliObject->query($query);

// die (var_dump($mysqliResult));

echo 'Aantal items = ', $mysqliResult->num_rows, $br;

echo '
<table border="1" cellspacing="0" cellpadding="2">
	<tr>
		<th><a href="?sort=title">title</a></th>
		<th><a href="?sort=merk">merk</a></th>
		<th><a href="?sort=land">land</a></th>
		<th><a href="?sort=type">type</a></th>
		<th><a href="?sort=kleur">kleur</a></th>
		<th><a href="?sort=brandstof">brandstof</a></th>
		<th><a href="?sort=zitplaatsen">zitplaatsen</a></th>
		<th><a href="?sort=prijs">prijs</a></th>
	</tr>', $nl;

while ($row = $mysqliResult->fetch_assoc() ){
	echo'	<tr>', $nl;
	echo'		<td>' . $row['title'] . '</td>', $nl;
	echo'		<td>' . $row['merk'] . '</td>', $nl;
	echo'		<td>' . $row['land'] . '</td>', $nl;
	echo'		<td>' . $row['type'] . '</td>', $nl;
	echo'		<td>' . $row['kleur'] . '</td>', $nl;
	echo'		<td>' . $row['brandstof'] . '</td>', $nl;
	echo'		<td>' . $row['zitplaatsen'] . '</td>', $nl;
	echo'		<td>&euro;' . $row['prijs'] . '</td>', $nl;
	echo'	</tr>', $nl;
}
echo '</table>', $nl;


?>


</p>

	
</body>
</html>

