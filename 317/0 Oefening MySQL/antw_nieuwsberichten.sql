-- Opdracht 1
USE cmm_nieuwsberichten;
SELECT * FROM nieuwsberichten;
SELECT titel, user_id, publicatie_datum FROM nieuwsberichten;

-- Opdracht 2
SELECT n.titel, s.naam, n.publicatie_datum 
FROM nieuwsberichten AS n
INNER JOIN schrijvers AS s
ON n.user_id = s.id;

-- Opdracht 3
SELECT n.titel, s.naam, n.publicatie_datum 
FROM nieuwsberichten AS n
INNER JOIN schrijvers AS s
ON n.user_id = s.id
WHERE s.naam LIKE 'jij%';

-- Opdracht 4
SELECT COUNT(*) AS 'Aantal berichten'
FROM nieuwsberichten AS n
INNER JOIN schrijvers AS s
ON n.user_id = s.id
WHERE s.naam LIKE 'ik';

