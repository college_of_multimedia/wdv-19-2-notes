<?php
/*
Plugin Name: CookieCuttr WordPress
Plugin URI: http://cookiecuttr.com/wordpress-plugin/faqs/
Description: Enables CookieCuttr functionality to help achieve compliance with EU Cookie Legislation
Version: 1.0.17
Author: 2nd Floor
Author URI: http://weare2ndfloor.com
License: 
*/

function cookiecuttr_install()
{

    add_option("cookiecuttr_version", "1.0.17");
    
    add_option("cookiecuttr_hidefeature", "false");
    add_option("cookiecuttr_hidedeclineonly", "false");
    add_option("cookiecuttr_analytics", "true");
    add_option("cookiecuttr_declinebutton", "false");
    add_option("cookiecuttr_resetbutton", "false");
    add_option("cookiecuttr_overlayenabled", "false");
    add_option("cookiecuttr_policylink", "/privacy-policy/");
    add_option("cookiecuttr_cookiemessage", "We use cookies on this website, you can <a href=\"{{cookiePolicyLink}}\" title=\"read about our cookies\">read about them here</a>. To use the website as intended please...");
    add_option("cookiecuttr_analyticsmessage", "We use cookies, just to track visits to our website, we store no personal details. To use the website as intended please...");
    add_option("cookiecuttr_errormessage", "We\'re sorry, this feature places cookies in your browser and has been disabled. <br>To continue using this functionality, please");
    add_option("cookiecuttr_whataretheylink", "http://www.allaboutcookies.org/");    
    add_option("cookiecuttr_disable", "");    
    add_option("cookiecuttr_googleanalyticsid", "");
    add_option("cookiecuttr_showanalyticsuntildeclined", "false");
    add_option("cookiecuttr_footerjs", "false");
    add_option("cookiecuttr_includejquerywp", "false");
    add_option("cookiecuttr_acceptcookiesmessage", "Accept Cookies");
    add_option("cookiecuttr_declinecookiesmessage", "Decline Cookies");
    add_option("cookiecuttr_resetcookiesmessage", "Reset Cookies");
    add_option("cookiecuttr_whatarecookiesmessage", "What are Cookies?");
    add_option("cookiecuttr_notificationlocationbottom", "false");
    add_option("cookiecuttr_cookieacceptbutton", "false");
    add_option("cookiecuttr_policypagemessage", "Please read the information below and then choose from the following options");    
    add_option("cookiecuttr_discreetlink", "false");    
    add_option("cookiecuttr_discreetreset", "false");    
    add_option("cookiecuttr_discreetlinktext", "Cookies?");    
    add_option("cookiecuttr_discreetlinkposition", "topright");   
    add_option("cookiecuttr_nomessage", "false");    
    add_option("cookiecuttr_domain", ""); 
}

function cookiecuttr_plugin_menu() 
{
    add_options_page('CookieCuttr', 'CookieCuttr', 'manage_options', 'cookiecuttr-plugin-menu', 'cookiecuttr_plugin_options');
}

function cookiecuttr_plugin_options() 
{
    if (!current_user_can('manage_options'))  
    {
        wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    // if postback, update settings using update_option here
    if(isset($_POST["is_postback"]))
    {
        update_option("cookiecuttr_hidefeature", ($_POST["cookiecuttr_hidefeature"]=="true"?"true":"false"));
        update_option("cookiecuttr_hidedeclineonly", ($_POST["cookiecuttr_hidedeclineonly"]=="true"?"true":"false"));
        update_option("cookiecuttr_analytics", ($_POST["cookiecuttr_analytics"]=="true"?"true":"false"));
        update_option("cookiecuttr_declinebutton", ($_POST["cookiecuttr_declinebutton"]=="true"?"true":"false"));
        update_option("cookiecuttr_cookieacceptbutton", ($_POST["cookiecuttr_cookieacceptbutton"]=="true"?"true":"false"));
        update_option("cookiecuttr_resetbutton", ($_POST["cookiecuttr_resetbutton"]=="true"?"true":"false"));
        update_option("cookiecuttr_overlayenabled", ($_POST["cookiecuttr_overlayenabled"]=="true"?"true":"false"));
        update_option("cookiecuttr_policylink", $_POST["cookiecuttr_policylink"]);
        update_option("cookiecuttr_cookiemessage", $_POST["cookiecuttr_cookiemessage"]);
        update_option("cookiecuttr_analyticsmessage", $_POST["cookiecuttr_analyticsmessage"]);
        update_option("cookiecuttr_errormessage", $_POST["cookiecuttr_errormessage"]);
        update_option("cookiecuttr_whataretheylink", $_POST["cookiecuttr_whataretheylink"]); 
        update_option("cookiecuttr_disablemessage", $_POST["cookiecuttr_disablemessage"]); 
        update_option("cookiecuttr_googleanalyticsid", $_POST["cookiecuttr_googleanalyticsid"]);
        update_option("cookiecuttr_showanalyticsuntildeclined", ($_POST["cookiecuttr_showanalyticsuntildeclined"]=="true"?"true":"false"));
        update_option("cookiecuttr_footerjs", ($_POST["cookiecuttr_footerjs"]=="true"?"true":"false"));
        update_option("cookiecuttr_includejquerywp", ($_POST["cookiecuttr_includejquerywp"]=="true"?"true":"false"));
        update_option("cookiecuttr_acceptcookiesmessage", $_POST["cookiecuttr_acceptcookiesmessage"]);
        update_option("cookiecuttr_declinecookiesmessage", $_POST["cookiecuttr_declinecookiesmessage"]);
        update_option("cookiecuttr_resetcookiesmessage", $_POST["cookiecuttr_resetcookiesmessage"]);
        update_option("cookiecuttr_whatarecookiesmessage", $_POST["cookiecuttr_whatarecookiesmessage"]);
        update_option("cookiecuttr_notificationlocationbottom", ($_POST["cookiecuttr_notificationlocationbottom"]=="true"?"true":"false"));
        update_option("cookiecuttr_policypagemessage", $_POST["cookiecuttr_policypagemessage"]);
        update_option("cookiecuttr_discreetlink", ($_POST["cookiecuttr_discreetlink"]=="true"?"true":"false"));
        update_option("cookiecuttr_discreetreset", ($_POST["cookiecuttr_discreetreset"]=="true"?"true":"false"));
        update_option("cookiecuttr_discreetlinktext", $_POST["cookiecuttr_discreetlinktext"]);
        update_option("cookiecuttr_discreetlinkposition", $_POST["cookiecuttr_discreetlinkposition"]);
        update_option("cookiecuttr_nomessage", ($_POST["cookiecuttr_nomessage"]=="true"?"true":"false"));
        update_option("cookiecuttr_domain", $_POST["cookiecuttr_domain"]);
    ?>
            <div class="updated"><p><strong><?php _e('settings saved.', 'menu-test' ); ?></strong></p></div>
        <?php
    }
    
    $path = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));

    ?>

<style text="text/css">
@import "http://fonts.googleapis.com/css?family=Exo:200|Lato:200";
div.cookiecuttr { position: relative; float: left; font-family: 'Lato', Arial, Helvetica, sans-serif; color: #4b4031; text-shadow: 0 1px 0 rgba(255,255,255,0.75); background: url(<?php echo $path ?>/bg.jpg) 0 0 repeat #e0e0d9; padding: 2em 2%; width: 92%; margin-bottom: 2em; font-size: 15px; border: 1px solid #ccc; }
div.cookiecuttr h2, div.cookiecuttr-col h2 { font-size: 24px; font-family: 'Exo', Arial, Helvetica, sans-serif; color: #4b4031; font-weight: 200;}
div.cookiecuttr-col h2 { border-top: 1px solid #ccc; border-top: 1px solid rgba(255,255,255,0.6) ; box-shadow: 0 -1px 0 rgba(104,104,94,0.3); padding-top: 1em; float: left; width: 100%; }
div.cookiecuttr-col h2.cc-top {  padding-top: 0; border: none; box-shadow: none; }
div.cookiecuttr .icon32 { background:url(<?php echo $path ?>/logo.png) 0 0 no-repeat !important; width: 140px; height: 280px; }
ul.cookiecuttr {  font-family: 'Lato', Arial, Helvetica, sans-serif; color: #4b4031; text-shadow: 0 1px 0 rgba(255,255,255,0.75); font-size: 15px; }
input.cookiecuttr-input, textarea.cookiecuttr-textarea { border-radius: 3px; padding: 0.5em; width: 96%; }
textarea.cookiecuttr-textarea { height: 120px; }
div.cookiecuttr-col { font-family: 'Lato', Arial, Helvetica, sans-serif; color: #4b4031; text-shadow: 0 1px 0 rgba(255,255,255,0.75); background: url(<?php echo $path ?>/bg.jpg) 0 0 repeat #e0e0d9; padding: 2em 2%; float: left; width: 43%; margin-right: 2%; border: 1px solid #ccc; font-size: 15px;  }
input.cookiecuttr-button { margin-top: 1em; font-size: 20px; border: none; text-transform: uppercase; color: #fff; text-decoration: none; background: #f15b00; padding: 0.75em 2.25em; border-radius: 3px; box-shadow: 0 0 2px rgba(0,0,0,0.25); -o-transition: background 0.5s ease-in; -webkit-transition: background 0.25s ease-in; -moz-transition: background 0.25s ease-in; }
input:hover.cookiecuttr-button { background: #000; color: #fff; -o-transition: background 0.5s ease-in; -webkit-transition: background 0.25s ease-in; -moz-transition: background 0.25s ease-in; cursor: pointer; }
div.cookiecuttr .updated { position: absolute; left: 0; top: -4px; width: 96%; margin-left: 1%; font-size: 14px; }
.cookiecuttr-submit { padding: 2em 0; float: left; width: 43%; margin-right: 2%; }
span.cookiecuttr-tooltip { background: #f1f1f1; padding: 0.5em; display: block; border: 1px solid #ccc; font-size: 12px; width: 90%; border-radius: 5px; }
.cookiecuttr-col ul { padding-bottom: 0; margin-bottom: 0;  }
.cookiecuttr-col ul.cookiecuttr-three { width: 100%; clear: both; float: left; }
.cookiecuttr-col ul.cookiecuttr-three li { display: inline !important; margin-right: 2em; float: left; }
</style>

        <div class="wrap cookiecuttr">
        <div id="icon-options-general" class="icon32"></div>
        <h2>CookieCuttr Options</h2>
        <p>
            You can edit all of the various options for CookieCuttr below, you will then need to wrap the following code around the script block...</p>
<p>
if($.cookieCuttr.accepted) { <br />
&nbsp;&nbsp;&nbsp;// this code will not run until cookies have been accepted <br />
} <br /> <br />

if($.cookieCuttr.declined) { <br />
&nbsp;&nbsp;&nbsp;// this code will not run until cookies have been declined <br />
}
</p>
<p>
To activate a different message on your policy page, allowing the website visitor to opt in/out just copy and paste the following shortcode anywhere in your pages content.<br /><br />
[cookiecuttrprivacy]
</p>
 
        </div>
        <form name="form1" method="post" action="">
            <input type="hidden" name="is_postback" value="1" />
            
            	 <div class="cookiecuttr-col">
            	 <h2 class="cc-top">Button &amp; Link Text</h2>
            	 <ul>
            	 <li>
            	     <label for="cookiecuttr_acceptcookiesmessage">Change 'Accept Cookies' button text</label><br />
            	     <input class="cookiecuttr-input" type="text" id="cookiecuttr_acceptcookiesmessage" name="cookiecuttr_acceptcookiesmessage" value="<?php echo(get_option("cookiecuttr_acceptcookiesmessage"));?>" />
            	 </li>
            	 <li>
            	     <label for="cookiecuttr_declinecookiesmessage">Change 'Decline Cookies' button text</label><br />
            	     <input class="cookiecuttr-input" type="text" id="cookiecuttr_declinecookiesmessage" name="cookiecuttr_declinecookiesmessage" value="<?php echo(get_option("cookiecuttr_declinecookiesmessage"));?>" />
            	 </li>
            	 <li>
            	     <label for="cookiecuttr_resetcookiesmessage">Change 'Reset Cookies' button text</label><br />
            	     <input class="cookiecuttr-input" type="text" id="cookiecuttr_resetcookiesmessage" name="cookiecuttr_resetcookiesmessage" value="<?php echo(get_option("cookiecuttr_resetcookiesmessage"));?>" />
            	 </li>
            	 <li>
            	     <label for="cookiecuttr_whatarecookiesmessage">Change 'What are Cookies' link text</label><br />
            	     <input class="cookiecuttr-input" type="text" id="cookiecuttr_whatarecookiesmessage" name="cookiecuttr_whatarecookiesmessage" value="<?php echo(get_option("cookiecuttr_whatarecookiesmessage"));?>" />
            	 </li>
            	 <li>
            	     <label for="cookiecuttr_discreetlinktext">Change 'Discreet Link' text</label><br />
            	     <input class="cookiecuttr-input" type="text" id="cookiecuttr_policypagemessage" name="cookiecuttr_discreetlinktext" value="<?php echo(stripslashes(get_option("cookiecuttr_discreetlinktext")));?>" />
            	 </li>   
            	 </ul>
            	 <h2>Messages</h2>
            	<ul>
                <li>
                    <label for="cookiecuttr_analyticsmessage">Analytics message (this is the default message)</label><br />
                    <textarea class="cookiecuttr-textarea" id="cookiecuttr_analyticsmessage" name="cookiecuttr_analyticsmessage"><?php echo(stripslashes(get_option("cookiecuttr_analyticsmessage")));?></textarea>
                </li>
                <li>
                    <label for="cookiecuttr_cookiemessage">'Cookie Privacy link' Message (used when Analytics tick box is off)</label><br />
                    <textarea class="cookiecuttr-textarea" id="cookiecuttr_cookiemessage" name="cookiecuttr_cookiemessage"><?php echo(stripslashes(get_option("cookiecuttr_cookiemessage")));?></textarea>
                    <br /><br />
                    <span class="cookiecuttr-tooltip">Please keep the {{cookiePolicyLink}} in tact for this message to work properly</span>
                    <br />
                </li>
                <li>
                    <label for="cookiecuttr_errormessage">'Error' message (used when Enable div etc. hide feature is on)</label><br />
                    <textarea class="cookiecuttr-textarea" id="cookiecuttr_errormessage" name="cookiecuttr_errormessage"><?php echo(stripslashes(get_option("cookiecuttr_errormessage")));?></textarea>
                </li>
                <li>
                    <label for="cookiecuttr_policypagemessage">'Privacy Policy Page' message</label><br />
                    <input class="cookiecuttr-input" type="text" id="cookiecuttr_policypagemessage" name="cookiecuttr_policypagemessage" value="<?php echo(stripslashes(get_option("cookiecuttr_policypagemessage")));?>" />
                </li>
                </ul>
                                 
                <h2>Links</h2>
                <ul>
                <li>
                    <label for="cookiecuttr_policylink">Your Privacy policy / Cookie policy link</label><br />
                    <input class="cookiecuttr-input" type="text" id="cookiecuttr_policylink" name="cookiecuttr_policylink" value="<?php echo(get_option("cookiecuttr_policylink"));?>" />
                </li>
                <li>
                    <label for="cookiecuttr_whataretheylink">'What are Cookies' link</label><br />
                    <input class="cookiecuttr-input" type="text" id="cookiecuttr_whataretheylink" name="cookiecuttr_whataretheylink" value="<?php echo(get_option("cookiecuttr_whataretheylink"));?>" />
                </li>
                </ul>
                <h2>Cookie Bar / Discreet Link Positioning</h2>   
                <ul>
                <li>
                    <label for="cookiecuttr_notificationlocationbottom">Display the notification at the bottom of the page?</label>
                    <input type="checkbox" id="cookiecuttr_notificationlocationbottom" name="cookiecuttr_notificationlocationbottom" <?php echo(get_option("cookiecuttr_notificationlocationbottom")=="true"?"checked":""); ?> value="true"/><br /><br />
                </li>              
                <li>
                    <label for="cookiecuttr_discreetlinkposition">'Discreet' position (This positions Discreet Link and Discreet Reset)</label><br /><br />
                    <select id="cookiecuttr_discreetlinkposition" name="cookiecuttr_discreetlinkposition">
                        <option value="topright" <?php echo(get_option("cookiecuttr_discreetlinkposition") == "topright"?" selected":""); ?>>Top Right</option>
                        <option value="topleft" <?php echo(get_option("cookiecuttr_discreetlinkposition") == "topleft"?" selected":""); ?>>Top Left</option>
                        <option value="bottomright" <?php echo(get_option("cookiecuttr_discreetlinkposition") == "bottomright"?" selected":""); ?>>Bottom Right</option>
                        <option value="bottomleft" <?php echo(get_option("cookiecuttr_discreetlinkposition") == "bottomleft"?" selected":""); ?>>Bottom Left</option>
                    </select>
                </li>                    
                </ul>
                <h2>Disable Page elements</h2>
                <ul>
                	<li>
                        <label for="cookiecuttr_hidefeature">Enable the page element disabler</label>
                        <input type="checkbox" id="cookiecuttr_hidefeature" name="cookiecuttr_hidefeature" <?php echo(get_option("cookiecuttr_hidefeature")=="true"?"checked":""); ?> value="true"/>
                    	<br /><br />
                    </li>
                    <li>
                        <label for="cookiecuttr_hidedeclineonly">Only activate page element disabler when visitor Declines cookies (implied consent)</label>
                        <input type="checkbox" id="cookiecuttr_hidedeclineonly" name="cookiecuttr_hidedeclineonly" <?php echo(get_option("cookiecuttr_hidedeclineonly")=="true"?"checked":""); ?> value="true"/>
                    	<br /><br />
                    	<span class="cookiecuttr-tooltip">For this option to work, "Enable the page element disabler" must be ticked</span>
                    	<br />
                    </li>
                    <li>
                        <label for="cookiecuttr_disablemessage">Enter the CSS Selectors (comma separated) that you want to disable e.g. .comments, #footer, span.advert, section.advert</label><br />
                        <input class="cookiecuttr-input" id="cookiecuttr_disablemessage" name="cookiecuttr_disablemessage" type="text" value="<?php echo(stripslashes(get_option("cookiecuttr_disablemessage")));?>" />
                    </li>
                </ul>    
            </div>
            
            <div class="cookiecuttr-col">
            <h2 class="cc-top">Google Analytics</h2>
            <ul>
            <li>
                <label for="cookiecuttr_googleanalyticsid">Enter your Google Analytics ID</label>
                <input class="cookiecuttr-input" type="text" id="cookiecuttr_googleanalyticsid" name="cookiecuttr_googleanalyticsid" value="<?php echo get_option("cookiecuttr_googleanalyticsid");?>" />
                <br /><br />
                <span class="cookiecuttr-tooltip">You need to disable any other plugins that are currently running your Google Analytics and remove any Google Analytics code you have in your templates for this option to work.</span><br />
            </li>
            <li>
                <label for="cookiecuttr_showanalyticsuntildeclined">Enable Google Analytics until the website visitor has declined cookies? (Implied consent)</label>
                <input type="checkbox" id="cookiecuttr_showanalyticsuntildeclined" name="cookiecuttr_showanalyticsuntildeclined" <?php echo(get_option("cookiecuttr_showanalyticsuntildeclined")=="true"?"checked":""); ?> value="true"/><br /><br />
            </li>  
<li>
                <label for="cookiecuttr_domain">Enter your domain name (no http://www.)</label>
                <input class="cookiecuttr-input" type="text" id="cookiecuttr_domain" name="cookiecuttr_domain" value="<?php echo get_option("cookiecuttr_domain");?>" />
                <br /><br />
                <span class="cookiecuttr-tooltip">If you'd like us to remove Google Analytics cookies when a user declines cookies, enter your domain name in above with no www. e.g. if your domain name is http://www.cookiecuttr.com please enter in cookiecuttr.com</span><br />
            </li>                  
            </ul>
            
            <h2>Disable all cookie notification bars</h2>
            <ul>
            <li>
                <label for="cookiecuttr_nomessage">Yes please</label>
                <input type="checkbox" id="cookiecuttr_nomessage" name="cookiecuttr_nomessage" <?php echo(get_option("cookiecuttr_nomessage")=="true"?"checked":""); ?> value="true"/>
            	<br /><br />
            	<span class="cookiecuttr-tooltip">This option should be used in conjunction with adding the shortcode [cookiecuttrprivacy] to your policy page as detailed above. You must make your policy page clear and obvious in your menu/template. This option overrides all the below options. This does not hide the 'Privacy Policy Page Only' message.</span><br />
            </li>
            </ul>
            
            <h2>Show these buttons...</h2>
            <ul class="cookiecuttr-three">
            	<li>
            	    <label for="cookiecuttr_cookieacceptbutton">Accept</label>
            	    <input type="checkbox" id="cookiecuttr_cookieacceptbutton" name="cookiecuttr_cookieacceptbutton" <?php echo(get_option("cookiecuttr_cookieacceptbutton")=="true"?"checked":""); ?> value="true"/>
            	</li>
                <li>
                    <label for="cookiecuttr_declinebutton">Decline</label>
                    <input type="checkbox" id="cookiecuttr_declinebutton" name="cookiecuttr_declinebutton" <?php echo(get_option("cookiecuttr_declinebutton")=="true"?"checked":""); ?> value="true"/>
                </li>
                <li>
                    <label for="cookiecuttr_resetbutton">Reset</label>
                    <input type="checkbox" id="cookiecuttr_resetbutton" name="cookiecuttr_resetbutton" <?php echo(get_option("cookiecuttr_resetbutton")=="true"?"checked":""); ?> value="true"/>
                </li>
            </ul>
            
            <h2>Cookie Bar style</h2>
            <ul>
            <li>
                <label for="cookiecuttr_discreetlink">Display the "Discreet Link"?</label>
                                <input type="checkbox" id="cookiecuttr_discreetlink" name="cookiecuttr_discreetlink" <?php echo(get_option("cookiecuttr_discreetlink")=="true"?"checked":""); ?> value="true"/><br /><br />
            <span class="cookiecuttr-tooltip">Selecting this option will automatically switch off the other messages on the website and replace with your discreet link text and link, this option should be used in conjunction with adding the shortcode [cookiecuttrprivacy] to your policy page as detailed above.</span><br />
            </li>
            <li>
                <label for="cookiecuttr_discreetreset">Make the reset button discreet ?</label>
                <input type="checkbox" id="cookiecuttr_discreetreset" name="cookiecuttr_discreetreset" <?php echo(get_option("cookiecuttr_discreetreset")=="true"?"checked":""); ?> value="true"/>
            	<br /><br />
            </li>
            <li>
                <label for="cookiecuttr_overlayenabled">Don't want a discreet toolbar? Give me a big overlay!</label>
                <input type="checkbox" id="cookiecuttr_overlayenabled" name="cookiecuttr_overlayenabled" <?php echo(get_option("cookiecuttr_overlayenabled")=="true"?"checked":""); ?> value="true"/>
            </li>
            </ul>
            <h2>Toggle Messages</h2>
                <ul>
                <li>
                    <label for="cookiecuttr_analytics">Switch on for 'Analytics' message and off for 'Cookie Privacy Link' message</label>
                    <input type="checkbox" id="cookiecuttr_analytics" name="cookiecuttr_analytics" <?php echo(get_option("cookiecuttr_analytics")=="true"?"checked":""); ?> value="true"/>
                </li>
                </ul>
                
                <h2>Technical implementation</h2>
                <ul>
                <li>
                    <label for="cookiecuttr_footerjs">Output JavaScript in footer?</label>
                    <input type="checkbox" id="cookiecuttr_footerjs" name="cookiecuttr_footerjs" <?php echo(get_option("cookiecuttr_footerjs")=="true"?"checked":""); ?> value="true"/><br /><br />
                </li>                    
                <li>
                    <label for="cookiecuttr_includejquerywp">Include jQuery via WordPress?</label>
                    <input type="checkbox" id="cookiecuttr_includejquerywp" name="cookiecuttr_includejquerywp" <?php echo(get_option("cookiecuttr_includejquerywp")=="true"?"checked":""); ?> value="true"/><br /><br />
                    <span class="cookiecuttr-tooltip">The above 2 options can be checked if the cookie bar is not displaying or you are getting JavaScript errors.</span>
                    <br />
                </li>                    
             </ul>
            </div>
            <p class="cookiecuttr-submit">
                    <input type="submit" name="Submit" class="cookiecuttr-button" value="Save Changes" />
            </p>
        </form>
    <?php
}

function cookiecuttr_load_css()
{
    $path = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
    wp_register_style("cookiecuttr_main", $path."cookiecuttr.css", false, false);
    wp_enqueue_style("cookiecuttr_main");
}

function cookiecuttr_load_js()
{
    $path = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
    
    wp_deregister_script("cookiecuttr_js");
    
    if(get_option("cookiecuttr_includejquerywp") == "true")
    {
        wp_register_script("cookiecuttr_js", $path . "jquery.cookiecuttr.js", array('jquery'), '1.0.17');
        wp_enqueue_script("cookiecuttr_js", $path . "jquery.cookiecuttr.js", array("jquery"), "1.0.17", get_option("cookiecuttr_footerjs") == "true");
    }
    else
    {
        wp_register_script("cookiecuttr_js", $path . "jquery.cookiecuttr.js", "", '1.0.17');
        wp_enqueue_script("cookiecuttr_js", $path . "jquery.cookiecuttr.js", "", "1.0.17", get_option("cookiecuttr_footerjs") == "true");
    }

    wp_localize_script("cookiecuttr_js", 'defaults', 
            array
                ( 
                    'cookieCutter' => (get_option("cookiecuttr_hidefeature") == "true"?true:false),
                    'cookieCutterDeclineOnly' => (get_option("cookiecuttr_hidedeclineonly") == "true"?true:false),
                    'cookieAnalytics' => (get_option("cookiecuttr_analytics") == "true"?true:false),
                    'cookieAcceptButton' => (get_option("cookiecuttr_cookieacceptbutton") == "true"?true:false),
                    'cookieDeclineButton' => (get_option("cookiecuttr_declinebutton") == "true"?true:false),
                    'cookieResetButton' => (get_option("cookiecuttr_resetbutton") == "true"?true:false),
                    'cookieOverlayEnabled' => (get_option("cookiecuttr_overlayenabled") == "true"?true:false),
                    'cookiePolicyLink' => get_option("cookiecuttr_policylink"),
                    'cookieMessage' => stripslashes(get_option("cookiecuttr_cookiemessage")),
                    'cookieAnalyticsMessage' => stripslashes(get_option("cookiecuttr_analyticsmessage")),
                    'cookieErrorMessage' => stripslashes(get_option("cookiecuttr_errormessage")),
                    'cookieWhatAreTheyLink' => get_option("cookiecuttr_whataretheylink"),
                    'cookieDisable' => get_option("cookiecuttr_disablemessage"),
                    'cookieAnalyticsId' => get_option("cookiecuttr_googleanalyticsid"),
                    'cookieAcceptButtonText' => get_option("cookiecuttr_acceptcookiesmessage"),
                    'cookieDeclineButtonText' => get_option("cookiecuttr_declinecookiesmessage"),
                    'cookieResetButtonText' => get_option("cookiecuttr_resetcookiesmessage"),
                    'cookieWhatAreLinkText' => get_option("cookiecuttr_whatarecookiesmessage"),
                    'cookieNotificationLocationBottom' => (get_option("cookiecuttr_notificationlocationbottom") == "true"?true:false),
                    'cookiePolicyPageMessage' => stripslashes(get_option("cookiecuttr_policypagemessage")),
                    'cookieDiscreetLink' => (get_option("cookiecuttr_discreetlink") == "true"?true:false),
                    'cookieDiscreetReset' => (get_option("cookiecuttr_discreetreset") == "true"?true:false),            
                    'cookieDiscreetLinkText' => stripslashes(get_option("cookiecuttr_discreetlinktext")),
                    'cookieDiscreetPosition' => get_option("cookiecuttr_discreetlinkposition"),
                    'cookieNoMessage' => (get_option("cookiecuttr_nomessage") == "true"?true:false),
		    		'cookieDomain' => get_option("cookiecuttr_domain")
        )
            );
}  

function cookiecuttr_output_analytics()
{
    $googleAnalyticsId = get_option("cookiecuttr_googleanalyticsid");
    
    if(strlen($googleAnalyticsId) > 0)
    {
        if($_COOKIE["cc_cookie_accept"] == "cc_cookie_accept" || (get_option("cookiecuttr_showanalyticsuntildeclined") == "true" && ! $_COOKIE["cc_cookie_decline"] == "cc_cookie_decline"))
        {
        ?>
<script language="javascript" type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', '<?php echo($googleAnalyticsId); ?>']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
        <?php
        }
    }
}

function cookiecuttr_privacypage_shortcode($attributes)
{
    return "<input type='hidden' id='cookiecuttr_privacypage' value='1' />";
}

// hooks
register_activation_hook(__FILE__,'cookiecuttr_install');
// add_action('admin_head', 'custom_cookie_css');
add_action('admin_menu', 'cookiecuttr_plugin_menu');
add_action('wp_enqueue_scripts', 'cookiecuttr_load_js');
add_action('wp_print_styles', 'cookiecuttr_load_css');
add_action('wp_print_scripts', 'cookiecuttr_output_analytics');
add_shortcode( 'cookiecuttrprivacy', 'cookiecuttr_privacypage_shortcode' );
?>