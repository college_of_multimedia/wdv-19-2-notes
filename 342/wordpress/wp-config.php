<?php

// BEGIN iThemes Security - Deze regel niet wijzigen of verwijderen
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Deactiveer bestandsbewerker - Beveiliging > Instellingen > WordPress tweaks > Bestandsbewerker
// END iThemes Security - Deze regel niet wijzigen of verwijderen

/**
 * Default config for the website
 */
switch ( $_SERVER['HTTP_HOST'] ) {
	case 'webshop.test':
		define( 'DB_NAME',      'wordpress_cmm' );
		define( 'DB_USER',      'homestead' );
		define( 'DB_PASSWORD',  'secret' );
		define( 'DB_HOST',      'localhost' );
		define( 'DB_CHARSET',   'utf8' );
		define( 'DB_COLLATE',   '' );

		define( 'WP_CONTENT_DIR',   dirname( __FILE__ ) . '/public_html/content' );
		define( 'WP_CONTENT_URL',   '/content' );
		// define( 'WP_TEMP_DIR',      WP_CONTENT_DIR . '/uploads/tmp' );
		$table_prefix = 'cmm_';

		define( 'WP_DEBUG',             true );
		define( 'WP_DEBUG_DISPLAY',     false );
		define( 'WP_DEBUG_LOG',         false );
		ini_set( 'display_errors',      1 );
		break;

	case 'wordpress.cmm.nl':
	default:
		define( 'DB_NAME',      'webshop' );
		define( 'DB_USER',      'root' );
		define( 'DB_PASSWORD',  'qfoFaF7nHX' );
		define( 'DB_HOST',      'localhost' );
		define( 'DB_CHARSET',   'utf8' );
		define( 'DB_COLLATE',   '' );

		define( 'WP_CONTENT_DIR',   dirname( __FILE__ ) . '/public_html/content' );
		define( 'WP_CONTENT_URL',   '/content' );
		define( 'WP_TEMP_DIR',      WP_CONTENT_DIR . '/uploads' );
		$table_prefix = 'cmm_';

		define( 'WP_DEBUG',             false );
		define( 'WP_DEBUG_DISPLAY',     false );
		define( 'WP_DEBUG_LOG',         false );
		break;
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key
 * service} You can change these at any point in time to invalidate all existing cookies. This will force all users to
 * have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=8E56GRJ3>Ni|5ndHd|+B-J$zCShoQ_5J`GwIxI52BDIequGvi)d+~?Yio*pXkn/');
define('SECURE_AUTH_KEY',  '`ww~zRg6Bw%A|jLXXAv%H(h#i6ViWm4#X1EX9/TO4N^L?nOZb|Q~$Y@f0oty+Hb?');
define('LOGGED_IN_KEY',    ':|h}M:p,*H5]<i!+E#b*# L;CI$Jp+yXXbD#T8*fbAQ50ohAffp$!YqQ_Q<:3R P');
define('NONCE_KEY',        'JD6n-7W)^iFm9:x5F!Dehp&1kb7+kz~O-OG,nvzSC2}y+[CH!m4][J*!ysZH5L2d');
define('AUTH_SALT',        '>]%i3?^T8&e3x;][6|?2L.m6+^Ot;iJYek8zj+Nak)M{21RJ&O?)-|wy56?/@;Uo');
define('SECURE_AUTH_SALT', 'U%Peqcz;4=6E|G[!$DPf0pj[gX*h[=#{;,a^728-XH%X!SSkW:SE^]]JzEMIDfCl');
define('LOGGED_IN_SALT',   '-:7<TQd+?elSXmv~C.`20kBMe>ZtvquRJ,lP-=Pp?iVM(Z,Ga2cWA+0|$*L:U0>d');
define('NONCE_SALT',       'T~lc,H9}_a+$bD|,+=A|+ISkX.rJ.Xo}oPbD7k=v@?pY?BaIneB~i.JHePO ohQ-');

/**#@-*/

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define( 'WPLANG', 'nl_NL' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}


/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );