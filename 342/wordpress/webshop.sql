-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 25, 2020 at 03:01 PM
-- Server version: 8.0.18
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `cmm_actionscheduler_actions`
--

CREATE TABLE `cmm_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_actionscheduler_actions`
--

INSERT INTO `cmm_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(72, 'action_scheduler/migration_hook', 'pending', '2020-03-25 14:46:35', '2020-03-25 14:46:35', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585147595;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585147595;}', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(73, 'wc_admin_unsnooze_admin_notes', 'pending', '2019-10-04 13:34:05', '2019-10-04 13:34:05', '[]', 'O:32:\"ActionScheduler_IntervalSchedule\":5:{s:22:\"\0*\0scheduled_timestamp\";i:1570196045;s:18:\"\0*\0first_timestamp\";i:1570196045;s:13:\"\0*\0recurrence\";i:3600;s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1570196045;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}', 2, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(74, 'woocommerce_run_update_callback', 'pending', '2020-03-25 15:00:34', '2020-03-25 15:00:34', '[\"wc_admin_update_0201_order_status_index\"]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585148434;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585148434;}', 3, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(75, 'woocommerce_run_update_callback', 'pending', '2020-03-25 15:00:35', '2020-03-25 15:00:35', '[\"wc_admin_update_0201_db_version\"]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585148435;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585148435;}', 3, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(76, 'woocommerce_run_update_callback', 'pending', '2020-03-25 15:00:36', '2020-03-25 15:00:36', '[\"wc_admin_update_0230_rename_gross_total\"]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585148436;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585148436;}', 3, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(77, 'woocommerce_run_update_callback', 'pending', '2020-03-25 15:00:37', '2020-03-25 15:00:37', '[\"wc_admin_update_0230_db_version\"]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585148437;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585148437;}', 3, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(78, 'woocommerce_run_update_callback', 'pending', '2020-03-25 15:00:38', '2020-03-25 15:00:38', '[\"wc_admin_update_0251_remove_unsnooze_action\"]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585148438;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585148438;}', 3, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL),
(79, 'woocommerce_run_update_callback', 'pending', '2020-03-25 15:00:39', '2020-03-25 15:00:39', '[\"wc_admin_update_0251_db_version\"]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1585148439;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1585148439;}', 3, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_actionscheduler_claims`
--

CREATE TABLE `cmm_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_actionscheduler_groups`
--

CREATE TABLE `cmm_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_actionscheduler_groups`
--

INSERT INTO `cmm_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-notes'),
(3, 'woocommerce-db-updates');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_actionscheduler_logs`
--

CREATE TABLE `cmm_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_actionscheduler_logs`
--

INSERT INTO `cmm_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 72, 'actie aangemaakt', '2020-03-25 14:46:35', '2020-03-25 14:46:35'),
(2, 73, 'action created', '2019-10-04 13:34:05', '2019-10-04 13:34:05'),
(3, 73, 'Migrated action with ID 6 in ActionScheduler_wpPostStore to ID 73 in ActionScheduler_DBStoreMigrator', '2020-03-25 14:46:35', '2020-03-25 14:46:35'),
(4, 74, 'actie aangemaakt', '2020-03-25 15:00:34', '2020-03-25 15:00:34'),
(5, 75, 'actie aangemaakt', '2020-03-25 15:00:34', '2020-03-25 15:00:34'),
(6, 76, 'actie aangemaakt', '2020-03-25 15:00:34', '2020-03-25 15:00:34'),
(7, 77, 'actie aangemaakt', '2020-03-25 15:00:34', '2020-03-25 15:00:34'),
(8, 78, 'actie aangemaakt', '2020-03-25 15:00:34', '2020-03-25 15:00:34'),
(9, 79, 'actie aangemaakt', '2020-03-25 15:00:34', '2020-03-25 15:00:34');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_commentmeta`
--

CREATE TABLE `cmm_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_comments`
--

CREATE TABLE `cmm_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_comments`
--

INSERT INTO `cmm_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Een WordPress commentator', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-10-04 13:29:44', '2019-10-04 13:29:44', 'Hoi, dit is een reactie.\nOm te beginnen met beheren, bewerken en verwijderen van reacties, ga je naar het Reacties scherm op het dashboard.\nAvatars van auteurs komen van <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_itsec_distributed_storage`
--

CREATE TABLE `cmm_itsec_distributed_storage` (
  `storage_id` bigint(20) UNSIGNED NOT NULL,
  `storage_group` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `storage_key` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `storage_chunk` int(11) NOT NULL DEFAULT '0',
  `storage_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `storage_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_itsec_fingerprints`
--

CREATE TABLE `cmm_itsec_fingerprints` (
  `fingerprint_id` bigint(20) UNSIGNED NOT NULL,
  `fingerprint_user` bigint(20) UNSIGNED NOT NULL,
  `fingerprint_hash` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fingerprint_created_at` datetime NOT NULL,
  `fingerprint_approved_at` datetime NOT NULL,
  `fingerprint_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fingerprint_snapshot` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fingerprint_last_seen` datetime NOT NULL,
  `fingerprint_uses` int(11) NOT NULL DEFAULT '0',
  `fingerprint_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fingerprint_uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_itsec_geolocation_cache`
--

CREATE TABLE `cmm_itsec_geolocation_cache` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_host` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_lat` decimal(10,8) NOT NULL,
  `location_long` decimal(11,8) NOT NULL,
  `location_label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_credit` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_itsec_lockouts`
--

CREATE TABLE `cmm_itsec_lockouts` (
  `lockout_id` bigint(20) UNSIGNED NOT NULL,
  `lockout_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockout_start` datetime NOT NULL,
  `lockout_start_gmt` datetime NOT NULL,
  `lockout_expire` datetime NOT NULL,
  `lockout_expire_gmt` datetime NOT NULL,
  `lockout_host` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `lockout_user` bigint(20) UNSIGNED DEFAULT NULL,
  `lockout_username` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `lockout_active` int(1) NOT NULL DEFAULT '1',
  `lockout_context` text COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_itsec_logs`
--

CREATE TABLE `cmm_itsec_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `module` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'notice',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `init_timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `memory_current` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `memory_peak` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blog_id` bigint(20) NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `remote_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_itsec_logs`
--

-- --------------------------------------------------------

--
-- Table structure for table `cmm_itsec_opaque_tokens`
--

CREATE TABLE `cmm_itsec_opaque_tokens` (
  `token_id` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token_hashed` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token_type` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token_data` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token_created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_itsec_temp`
--

CREATE TABLE `cmm_itsec_temp` (
  `temp_id` bigint(20) UNSIGNED NOT NULL,
  `temp_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `temp_date` datetime NOT NULL,
  `temp_date_gmt` datetime NOT NULL,
  `temp_host` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `temp_user` bigint(20) UNSIGNED DEFAULT NULL,
  `temp_username` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_itsec_temp`
--

INSERT INTO `cmm_itsec_temp` (`temp_id`, `temp_type`, `temp_date`, `temp_date_gmt`, `temp_host`, `temp_user`, `temp_username`) VALUES
(1, 'brute_force', '2020-03-25 14:34:16', '2020-03-25 14:34:16', '127.0.0.1', NULL, NULL),
(2, 'brute_force', '2020-03-25 14:34:16', '2020-03-25 14:34:16', NULL, NULL, 'Jasper00'),
(3, 'brute_force', '2020-03-25 14:34:21', '2020-03-25 14:34:21', '127.0.0.1', NULL, NULL),
(4, 'brute_force', '2020-03-25 14:34:21', '2020-03-25 14:34:21', NULL, 1, 'jasper');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_links`
--

CREATE TABLE `cmm_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_options`
--

CREATE TABLE `cmm_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_options`
--

INSERT INTO `cmm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://webshop.test', 'yes'),
(2, 'home', 'https://webshop.test', 'yes'),
(3, 'blogname', 'CMM test weshop', 'yes'),
(4, 'blogdescription', 'Dit is mijn webshop', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'jasper@cmm.nl', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j F Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:41:\"better-wp-security/better-wp-security.php\";i:1;s:39:\"woocommerce-admin/woocommerce-admin.php\";i:2;s:27:\"woocommerce/woocommerce.php\";i:3;s:24:\"wordpress-seo/wp-seo.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'storefront', 'yes'),
(41, 'stylesheet', 'storefront', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '45805', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:41:\"better-wp-security/better-wp-security.php\";a:2:{i:0;s:10:\"ITSEC_Core\";i:1;s:16:\"handle_uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '61', 'yes'),
(84, 'page_on_front', '62', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'cmm_user_roles', 'a:9:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:115:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:38:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;s:23:\"view_site_health_checks\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'nl_NL', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:8:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:21:{i:1570195786;a:4:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1570195798;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570195877;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570196018;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570196044;a:2:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1570196236;a:1:{s:30:\"wp_1_wc_regenerate_images_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:39:\"wp_1_wc_regenerate_images_cron_interval\";s:4:\"args\";a:0:{}s:8:\"interval\";i:300;}}}i:1570196313;a:1:{s:31:\"woocommerce_flush_rewrite_rules\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1570196495;a:1:{s:25:\"wpseo_ping_search_engines\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1570231895;a:1:{s:15:\"itsec_cron_test\";a:1:{s:32:\"51f32ea705c0e6493146c45b8856d014\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:1:{i:0;i:1570231895;}}}}i:1570282703;a:1:{s:26:\"importer_scheduled_cleanup\";a:1:{s:32:\"a33bdbdff47548655d9bd9c05ea3930a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:1:{i:0;i:11;}}}}i:1585147592;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1585147603;a:2:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1585147653;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}i:1585148439;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1585148444;a:1:{s:30:\"generate_category_lookup_table\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1585151193;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1585158393;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1585169193;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1585169803;a:1:{s:42:\"wpseo_send_tracking_data_after_core_update\";a:1:{s:32:\"431014e4a761ea216e9a35f20aaec61c\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";b:1;}}}i:1585180800;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, '_transient_doing_cron', '1585148460.4913799762725830078125', 'yes'),
(141, 'db_upgraded', '', 'yes'),
(150, 'recently_activated', 'a:0:{}', 'yes'),
(166, 'woocommerce_store_address', 'Egelenburg 150-152', 'yes'),
(167, 'woocommerce_store_address_2', '', 'yes'),
(168, 'woocommerce_store_city', 'Amsterdam', 'yes'),
(169, 'woocommerce_default_country', 'NL:*', 'yes'),
(170, 'woocommerce_store_postcode', '1081 GK', 'yes'),
(171, 'woocommerce_allowed_countries', 'all', 'yes'),
(172, 'woocommerce_all_except_countries', '', 'yes'),
(173, 'woocommerce_specific_allowed_countries', '', 'yes'),
(174, 'woocommerce_ship_to_countries', '', 'yes'),
(175, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(176, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(177, 'woocommerce_calc_taxes', 'no', 'yes'),
(178, 'woocommerce_enable_coupons', 'yes', 'yes'),
(179, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(180, 'woocommerce_currency', 'EUR', 'yes'),
(181, 'woocommerce_currency_pos', 'left', 'yes'),
(182, 'woocommerce_price_thousand_sep', ',', 'yes'),
(183, 'woocommerce_price_decimal_sep', '.', 'yes'),
(184, 'woocommerce_price_num_decimals', '2', 'yes'),
(185, 'woocommerce_shop_page_id', '7', 'yes'),
(186, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(187, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(188, 'woocommerce_placeholder_image', '5', 'yes'),
(189, 'woocommerce_weight_unit', 'kg', 'yes'),
(190, 'woocommerce_dimension_unit', 'cm', 'yes'),
(191, 'woocommerce_enable_reviews', 'yes', 'yes'),
(192, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(193, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(194, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(195, 'woocommerce_review_rating_required', 'yes', 'no'),
(196, 'woocommerce_manage_stock', 'yes', 'yes'),
(197, 'woocommerce_hold_stock_minutes', '60', 'no'),
(198, 'woocommerce_notify_low_stock', 'yes', 'no'),
(199, 'woocommerce_notify_no_stock', 'yes', 'no'),
(200, 'woocommerce_stock_email_recipient', 'jasper@cmm.nl', 'no'),
(201, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(202, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(203, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(204, 'woocommerce_stock_format', '', 'yes'),
(205, 'woocommerce_file_download_method', 'force', 'no'),
(206, 'woocommerce_downloads_require_login', 'no', 'no'),
(207, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(208, 'woocommerce_prices_include_tax', 'no', 'yes'),
(209, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(210, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(211, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(212, 'woocommerce_tax_classes', '', 'yes'),
(213, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(214, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(215, 'woocommerce_price_display_suffix', '', 'yes'),
(216, 'woocommerce_tax_total_display', 'itemized', 'no'),
(217, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(218, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(219, 'woocommerce_ship_to_destination', 'billing', 'no'),
(220, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(221, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(222, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(223, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(224, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(225, 'woocommerce_registration_generate_username', 'yes', 'no'),
(226, 'woocommerce_registration_generate_password', 'yes', 'no'),
(227, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(228, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(229, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(230, 'woocommerce_registration_privacy_policy_text', 'Je persoonlijke gegevens worden gebruikt om je ervaring op deze site te ondersteunen, om toegang tot je account te beheren en voor andere doeleinden zoals omschreven in onze [privacy_policy].', 'yes'),
(231, 'woocommerce_checkout_privacy_policy_text', 'Je persoonlijke gegevens zullen worden gebruikt om je bestelling te verwerken, om je beleving op deze website te optimaliseren en voor andere doeleinden zoals beschreven in onze [privacy_policy].', 'yes'),
(232, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(233, 'woocommerce_trash_pending_orders', '', 'no'),
(234, 'woocommerce_trash_failed_orders', '', 'no'),
(235, 'woocommerce_trash_cancelled_orders', '', 'no'),
(236, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(237, 'woocommerce_email_from_name', 'cmmtestshop', 'no'),
(238, 'woocommerce_email_from_address', 'jasper@cmm.nl', 'no'),
(239, 'woocommerce_email_header_image', '', 'no'),
(240, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(241, 'woocommerce_email_base_color', '#96588a', 'no'),
(242, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(243, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(244, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(245, 'woocommerce_cart_page_id', '8', 'no'),
(246, 'woocommerce_checkout_page_id', '9', 'no'),
(247, 'woocommerce_myaccount_page_id', '10', 'no'),
(248, 'woocommerce_terms_page_id', '', 'no'),
(249, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(250, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(251, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(252, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(253, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(254, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(255, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(256, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(257, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(258, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(259, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(260, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(261, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(262, 'woocommerce_api_enabled', 'no', 'yes'),
(263, 'woocommerce_allow_tracking', 'no', 'no'),
(264, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(265, 'woocommerce_single_image_width', '600', 'yes'),
(266, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(267, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(268, 'woocommerce_demo_store', 'no', 'no'),
(269, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:17:\"product-categorie\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(270, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(271, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(274, 'default_product_cat', '15', 'yes'),
(278, 'woocommerce_db_version', '3.7.0', 'yes'),
(279, 'wpseo', 'a:20:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:4:\"13.3\";s:20:\"disableadvanced_meta\";b:1;s:17:\"ryte_indexability\";b:1;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:1;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:1;s:26:\"enable_cornerstone_content\";b:1;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:1;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1570196018;s:13:\"myyoast-oauth\";b:0;}', 'yes'),
(280, 'wpseo_titles', 'a:96:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-dash\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, auteur op %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:67:\"Je hebt gezocht naar %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:41:\"Pagina niet gevonden %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:57:\"Het bericht %%POSTLINK%% verscheen eerst op %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:30:\"404-fout: pagina niet gevonden\";s:29:\"breadcrumbs-display-blog-page\";b:1;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:14:\"Archieven voor\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:13:\"Je zocht naar\";s:15:\"breadcrumbs-sep\";s:7:\"&raquo;\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:1;s:23:\"post_types-page-maintax\";i:0;s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";i:0;s:13:\"title-product\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:16:\"metadesc-product\";s:0:\"\";s:15:\"noindex-product\";b:0;s:16:\"showdate-product\";b:0;s:26:\"display-metabox-pt-product\";b:1;s:26:\"post_types-product-maintax\";i:0;s:23:\"title-ptarchive-product\";s:51:\"Archief %%pt_plural%% %%page%% %%sep%% %%sitename%%\";s:26:\"metadesc-ptarchive-product\";s:0:\"\";s:25:\"bctitle-ptarchive-product\";s:0:\"\";s:25:\"noindex-ptarchive-product\";b:0;s:18:\"title-tax-category\";s:54:\"%%term_title%% Archieven %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:1;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:54:\"%%term_title%% Archieven %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:54:\"%%term_title%% Archieven %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:1;s:23:\"noindex-tax-post_format\";b:1;s:21:\"title-tax-product_cat\";s:54:\"%%term_title%% Archieven %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-product_cat\";s:0:\"\";s:31:\"display-metabox-tax-product_cat\";b:1;s:23:\"noindex-tax-product_cat\";b:0;s:29:\"taxonomy-product_cat-ptparent\";i:0;s:21:\"title-tax-product_tag\";s:54:\"%%term_title%% Archieven %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-product_tag\";s:0:\"\";s:31:\"display-metabox-tax-product_tag\";b:1;s:23:\"noindex-tax-product_tag\";b:0;s:29:\"taxonomy-product_tag-ptparent\";i:0;s:32:\"title-tax-product_shipping_class\";s:54:\"%%term_title%% Archieven %%page%% %%sep%% %%sitename%%\";s:35:\"metadesc-tax-product_shipping_class\";s:0:\"\";s:42:\"display-metabox-tax-product_shipping_class\";b:1;s:34:\"noindex-tax-product_shipping_class\";b:0;s:40:\"taxonomy-product_shipping_class-ptparent\";i:0;}', 'yes'),
(281, 'wpseo_social', 'a:19:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}', 'yes'),
(282, 'wpseo_flush_rewrite', '1', 'yes'),
(283, 'itsec-storage', 'a:5:{s:6:\"global\";a:33:{s:15:\"lockout_message\";s:4:\"fout\";s:20:\"user_lockout_message\";s:72:\"Je bent uitgesloten naar aanleiding van teveel ongeldige inlog pogingen.\";s:25:\"community_lockout_message\";s:75:\"Je IP-adres is aangemerkt als bedreiging door het iThemes Security netwerk.\";s:9:\"blacklist\";b:1;s:15:\"blacklist_count\";i:3;s:16:\"blacklist_period\";i:7;s:14:\"lockout_period\";i:15;s:18:\"lockout_white_list\";a:0:{}s:12:\"log_rotation\";i:60;s:17:\"file_log_rotation\";i:180;s:8:\"log_type\";s:8:\"database\";s:12:\"log_location\";s:66:\"/Volumes/Web/cmm-webshop/www/content/uploads/ithemes-security/logs\";s:8:\"log_info\";s:0:\"\";s:14:\"allow_tracking\";b:0;s:11:\"write_files\";b:1;s:10:\"nginx_file\";s:39:\"/Volumes/Web/cmm-webshop/www/nginx.conf\";s:24:\"infinitewp_compatibility\";b:0;s:11:\"did_upgrade\";b:0;s:9:\"lock_file\";b:0;s:5:\"proxy\";s:9:\"automatic\";s:12:\"proxy_header\";s:20:\"HTTP_X_FORWARDED_FOR\";s:14:\"hide_admin_bar\";b:0;s:16:\"show_error_codes\";b:0;s:19:\"show_security_check\";b:0;s:5:\"build\";i:4116;s:13:\"initial_build\";i:4114;s:20:\"activation_timestamp\";i:1570196015;s:11:\"cron_status\";i:0;s:8:\"use_cron\";b:0;s:14:\"cron_test_time\";i:1570231895;s:19:\"enable_grade_report\";b:0;s:10:\"server_ips\";a:1:{i:0;s:9:\"127.0.0.1\";}s:13:\"feature_flags\";a:0:{}}s:19:\"network-brute-force\";a:5:{s:7:\"api_key\";s:32:\"UIyM0eX68iK7Dk1z6Kk1ZkB2owpX9B44\";s:10:\"api_secret\";s:128:\"g9l5I8q8V5yKjEiogSRQ6dlaxGWE5nZg1PC7TgK1ndvfS51DE1209h80G15Zi3c8y53p60Mind0b59E0ThcY4E7JH7KCySOle2HONhd62Gf81T582lgrM8gap60E8KlN\";s:10:\"enable_ban\";b:1;s:13:\"updates_optin\";b:0;s:7:\"api_nag\";b:0;}s:21:\"password-requirements\";a:2:{s:20:\"enabled_requirements\";a:1:{s:8:\"strength\";b:1;}s:20:\"requirement_settings\";a:1:{s:8:\"strength\";a:1:{s:4:\"role\";s:13:\"administrator\";}}}s:16:\"wordpress-tweaks\";a:13:{s:18:\"wlwmanifest_header\";b:0;s:14:\"edituri_header\";b:0;s:12:\"comment_spam\";b:0;s:11:\"file_editor\";b:1;s:14:\"disable_xmlrpc\";i:0;s:22:\"allow_xmlrpc_multiauth\";b:0;s:8:\"rest_api\";s:15:\"restrict-access\";s:12:\"login_errors\";b:0;s:21:\"force_unique_nicename\";b:0;s:27:\"disable_unused_author_pages\";b:0;s:16:\"block_tabnapping\";b:0;s:21:\"valid_user_login_type\";s:4:\"both\";s:26:\"patch_thumb_file_traversal\";b:1;}s:19:\"notification-center\";a:8:{s:9:\"last_sent\";a:1:{s:6:\"digest\";i:1585146830;}s:9:\"resend_at\";a:0:{}s:4:\"data\";a:1:{s:6:\"digest\";a:0:{}}s:15:\"last_mail_error\";s:0:\"\";s:13:\"notifications\";a:3:{s:6:\"digest\";a:5:{s:8:\"schedule\";s:5:\"daily\";s:7:\"subject\";N;s:7:\"enabled\";b:1;s:9:\"user_list\";a:1:{i:0;s:18:\"role:administrator\";}s:14:\"recipient_type\";s:7:\"default\";}s:7:\"lockout\";a:4:{s:7:\"subject\";N;s:7:\"enabled\";b:1;s:9:\"user_list\";a:1:{i:0;s:18:\"role:administrator\";}s:14:\"recipient_type\";s:7:\"default\";}s:6:\"backup\";a:2:{s:7:\"subject\";N;s:10:\"email_list\";a:1:{i:0;s:13:\"jasper@cmm.nl\";}}}s:12:\"admin_emails\";a:0:{}s:10:\"from_email\";s:0:\"\";s:18:\"default_recipients\";a:1:{s:9:\"user_list\";a:1:{i:0;s:18:\"role:administrator\";}}}}', 'yes'),
(284, 'woocommerce_admin_notices', 'a:4:{i:0;s:6:\"update\";i:1;s:14:\"template_files\";i:2;s:19:\"maxmind_license_key\";i:3;s:23:\"regenerating_thumbnails\";}', 'yes'),
(285, '_transient_timeout_wpseo_link_table_inaccessible', '1601732018', 'no'),
(286, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(287, '_transient_timeout_wpseo_meta_table_inaccessible', '1601732018', 'no'),
(288, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(289, 'itsec_temp_whitelist_ip', 'a:1:{s:9:\"127.0.0.1\";i:1585233309;}', 'no'),
(290, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(291, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(292, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(293, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(294, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(295, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(296, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(297, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(298, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(299, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(300, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(301, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(302, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(305, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(320, 'wc_admin_last_orders_milestone', '0', 'yes'),
(321, '_transient_product_query-transient-version', '1585147595', 'yes'),
(325, '_transient_as_comment_count', 'O:8:\"stdClass\":7:{s:8:\"approved\";s:1:\"1\";s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(326, 'woocommerce_product_type', 'both', 'yes'),
(327, 'woocommerce_stripe_settings', 'a:3:{s:7:\"enabled\";s:2:\"no\";s:14:\"create_account\";b:0;s:5:\"email\";b:0;}', 'yes'),
(328, 'woocommerce_klarna_checkout_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(329, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:\"reroute_requests\";b:0;s:5:\"email\";b:0;}', 'yes'),
(330, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(331, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:3:\"yes\";}', 'yes'),
(332, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(333, '_transient_shipping-transient-version', '1570196220', 'yes'),
(334, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:\"title\";s:11:\"Vast Tarief\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:4:\"6,95\";}', 'yes'),
(335, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:\"title\";s:11:\"Vast Tarief\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:5:\"23,45\";}', 'yes'),
(336, 'theme_mods_twentyseventeen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1570196235;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(337, 'current_theme', 'Storefront', 'yes'),
(338, 'theme_mods_storefront', 'a:7:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:65;s:12:\"header_image\";s:46:\"/content/uploads/2019/10/cropped-pennant-1.jpg\";s:17:\"header_image_data\";O:8:\"stdClass\":5:{s:13:\"attachment_id\";i:68;s:3:\"url\";s:46:\"/content/uploads/2019/10/cropped-pennant-1.jpg\";s:13:\"thumbnail_url\";s:46:\"/content/uploads/2019/10/cropped-pennant-1.jpg\";s:6:\"height\";i:205;s:5:\"width\";i:800;}s:34:\"storefront_header_background_color\";s:7:\"#e8e8e8\";}', 'yes'),
(339, 'theme_switched', '', 'yes'),
(340, 'storefront_nux_fresh_site', '0', 'yes'),
(341, 'woocommerce_catalog_rows', '4', 'yes'),
(342, 'woocommerce_catalog_columns', '3', 'yes'),
(343, 'woocommerce_maybe_regenerate_images_hash', '27acde77266b4d2a3491118955cb3f66', 'yes'),
(344, 'wp_1_wc_regenerate_images_batch_2fba83ddb7822e21fbb8bf21761451f6', 'a:1:{i:0;a:1:{s:13:\"attachment_id\";s:1:\"5\";}}', 'no'),
(346, '_transient_product-transient-version', '1570196338', 'yes'),
(353, 'product_cat_children', 'a:1:{i:17;a:4:{i:0;i:18;i:1;i:19;i:2;i:20;i:3;i:21;}}', 'yes'),
(361, 'pa_size_children', 'a:0:{}', 'yes'),
(364, 'pa_color_children', 'a:0:{}', 'yes'),
(387, 'storefront_nux_dismissed', '1', 'yes'),
(388, 'storefront_nux_guided_tour', '1', 'yes'),
(394, '_site_transient_timeout_itsec_wp_upload_dir', '1585233231', 'no'),
(395, '_site_transient_itsec_wp_upload_dir', 'a:6:{s:4:\"path\";s:169:\"/Users/jasper/Dropbox/dragonet/CMM/09_web developer/Development/Teacher Files/2019-1/343-344 - ecommerce/Lesson Files/start/wordpress/public_html/content/uploads/2020/03\";s:3:\"url\";s:24:\"/content/uploads/2020/03\";s:6:\"subdir\";s:8:\"/2020/03\";s:7:\"basedir\";s:161:\"/Users/jasper/Dropbox/dragonet/CMM/09_web developer/Development/Teacher Files/2019-1/343-344 - ecommerce/Lesson Files/start/wordpress/public_html/content/uploads\";s:7:\"baseurl\";s:16:\"/content/uploads\";s:5:\"error\";b:0;}', 'no'),
(396, '_transient_timeout_external_ip_address_127.0.0.1', '1585751632', 'no'),
(397, '_transient_external_ip_address_127.0.0.1', '212.187.88.55', 'no'),
(400, 'itsec_scheduler_page_load', 'a:2:{s:6:\"single\";a:0:{}s:9:\"recurring\";a:4:{s:17:\"purge-log-entries\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:10:\"last_fired\";i:1585146840;s:4:\"data\";a:0:{}}s:14:\"purge-lockouts\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:10:\"last_fired\";i:1585146840;s:4:\"data\";a:0:{}}s:11:\"clear-locks\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:10:\"last_fired\";i:1585146840;s:4:\"data\";a:0:{}}s:12:\"health-check\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:10:\"last_fired\";i:1585062871;s:4:\"data\";a:0:{}}}}', 'no'),
(406, '_transient_is_multi_author', '0', 'yes'),
(407, '_transient_timeout_wc_term_counts', '1587738840', 'no'),
(408, '_transient_wc_term_counts', 'a:8:{i:15;s:1:\"0\";i:21;s:1:\"5\";i:17;s:2:\"14\";i:23;s:1:\"1\";i:19;s:1:\"3\";i:22;s:1:\"2\";i:20;s:1:\"1\";i:18;s:1:\"4\";}', 'no'),
(420, '_transient_timeout_wc_admin_unsnooze_admin_notes_checked', '1585150474', 'no'),
(421, '_transient_wc_admin_unsnooze_admin_notes_checked', 'yes', 'no'),
(432, '_site_transient_timeout_theme_roots', '1585150209', 'no'),
(433, '_site_transient_theme_roots', 'a:2:{s:10:\"storefront\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}', 'no'),
(434, '_transient_timeout__woocommerce_helper_updates', '1585190110', 'no'),
(435, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1585146910;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(436, '_site_transient_timeout_browser_bc0f63895d19ea73b5e95c1feeb05709', '1585751712', 'no'),
(437, '_site_transient_browser_bc0f63895d19ea73b5e95c1feeb05709', 'a:10:{s:4:\"name\";s:6:\"Safari\";s:7:\"version\";s:6:\"13.0.5\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.apple.com/safari/\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/safari.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/safari.png?1\";s:15:\"current_version\";s:2:\"11\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(438, '_site_transient_timeout_php_check_9236ad8f2e178e4ce7b4ef5302b7fae9', '1585751712', 'no'),
(439, '_site_transient_php_check_9236ad8f2e178e4ce7b4ef5302b7fae9', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(440, '_transient_timeout_wc_low_stock_count', '1587738912', 'no'),
(441, '_transient_wc_low_stock_count', '0', 'no'),
(442, '_transient_timeout_wc_outofstock_count', '1587738912', 'no'),
(443, '_transient_wc_outofstock_count', '0', 'no'),
(444, '_transient_timeout_wc_report_sales_by_date', '1585233313', 'no'),
(445, '_transient_wc_report_sales_by_date', 'a:8:{s:32:\"dbb38305756b6a4dea4de5ffbf80d4c6\";a:0:{}s:32:\"dd27605f5b4db86c1b70ca970933125a\";a:0:{}s:32:\"4aae9d5f402a3032614fb8fd0958b235\";a:0:{}s:32:\"6ae3e43a333a8290d606271b33445328\";N;s:32:\"a7479657fbd6fbb09f8b109e1d6d091a\";a:0:{}s:32:\"af1ec2240faea9c254b964ff36b56489\";a:0:{}s:32:\"1e52c22e214ce619393abedc9dda37bd\";a:0:{}s:32:\"71ef97c597ce9c08a69f0a851fea48e8\";a:0:{}}', 'no'),
(446, '_transient_timeout_wc_admin_report', '1585233313', 'no'),
(447, '_transient_wc_admin_report', 'a:1:{s:32:\"813e8f58614364c0d71e5b5f39059c78\";a:0:{}}', 'no'),
(456, '_site_transient_timeout_community-events-1aecf33ab8525ff212ebdffbb438372e', '1585190116', 'no'),
(457, '_site_transient_community-events-1aecf33ab8525ff212ebdffbb438372e', 'a:3:{s:9:\"sandboxed\";b:0;s:8:\"location\";a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}s:6:\"events\";a:5:{i:0;a:8:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:45:\"Wordpress Gouda - ONLINE Fly-in Meetup Maart!\";s:3:\"url\";s:55:\"https://www.meetup.com/wordpressgouda/events/269530224/\";s:6:\"meetup\";s:15:\"WordPress Gouda\";s:10:\"meetup_url\";s:38:\"https://www.meetup.com/wordpressgouda/\";s:4:\"date\";s:19:\"2020-03-28 14:00:00\";s:8:\"end_date\";s:19:\"2020-03-28 16:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:18:\"Gouda, Netherlands\";s:7:\"country\";s:2:\"nl\";s:8:\"latitude\";d:52.012359619141;s:9:\"longitude\";d:4.710666179657;}}i:1;a:8:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:39:\"[CANCELLED] WordPress Meetup Amersfoort\";s:3:\"url\";s:54:\"https://www.meetup.com/WordPress-033/events/269321490/\";s:6:\"meetup\";s:35:\"Amersfoort WordPress Meetup #WPM033\";s:10:\"meetup_url\";s:37:\"https://www.meetup.com/WordPress-033/\";s:4:\"date\";s:19:\"2020-03-31 19:00:00\";s:8:\"end_date\";s:19:\"2020-03-31 21:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:23:\"Amersfoort, Netherlands\";s:7:\"country\";s:2:\"nl\";s:8:\"latitude\";d:52.191940307617;s:9:\"longitude\";d:5.407458782196;}}i:2;a:8:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:23:\"WordPress Meetup Zwolle\";s:3:\"url\";s:68:\"https://www.meetup.com/Zwolle-WordPress-Meetup/events/nsmmmrybcgbmb/\";s:6:\"meetup\";s:23:\"WordPress Meetup Zwolle\";s:10:\"meetup_url\";s:47:\"https://www.meetup.com/Zwolle-WordPress-Meetup/\";s:4:\"date\";s:19:\"2020-04-09 18:45:00\";s:8:\"end_date\";s:19:\"2020-04-09 20:55:00\";s:8:\"location\";a:4:{s:8:\"location\";s:19:\"Zwolle, Netherlands\";s:7:\"country\";s:2:\"nl\";s:8:\"latitude\";d:52.508583068848;s:9:\"longitude\";d:6.0933017730713;}}i:3;a:8:{s:4:\"type\";s:8:\"wordcamp\";s:5:\"title\";s:19:\"WordCamp Paris 2020\";s:3:\"url\";s:31:\"https://2020.paris.wordcamp.org\";s:6:\"meetup\";N;s:10:\"meetup_url\";N;s:4:\"date\";s:19:\"2020-04-17 00:00:00\";s:8:\"end_date\";s:19:\"2020-04-17 00:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:5:\"Paris\";s:7:\"country\";s:2:\"FR\";s:8:\"latitude\";d:48.883588;s:9:\"longitude\";d:2.3095841;}}i:4;a:8:{s:4:\"type\";s:6:\"meetup\";s:5:\"title\";s:25:\"WordPress Meetup Fryslân\";s:3:\"url\";s:65:\"https://www.meetup.com/WordPress-Meetup-Fryslan/events/269057668/\";s:6:\"meetup\";s:25:\"WordPress Meetup Fryslân\";s:10:\"meetup_url\";s:48:\"https://www.meetup.com/WordPress-Meetup-Fryslan/\";s:4:\"date\";s:19:\"2020-04-23 19:00:00\";s:8:\"end_date\";s:19:\"2020-04-23 21:00:00\";s:8:\"location\";a:4:{s:8:\"location\";s:23:\"Leeuwarden, Netherlands\";s:7:\"country\";s:2:\"nl\";s:8:\"latitude\";d:53.199237823486;s:9:\"longitude\";d:5.7741169929504;}}}}', 'no'),
(458, '_transient_timeout_feed_c326b61060938210a1df3d05d623467e', '1585190117', 'no');
INSERT INTO `cmm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(459, '_transient_feed_c326b61060938210a1df3d05d623467e', 'a:4:{s:5:\"child\";a:1:{s:0:\"\";a:1:{s:3:\"rss\";a:1:{i:0;a:6:{s:4:\"data\";s:2:\"\n\n\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:7:\"version\";s:3:\"2.0\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:1:{s:0:\"\";a:1:{s:7:\"channel\";a:1:{i:0;a:6:{s:4:\"data\";s:17:\"\n	\n	\n	\n	\n	\n	\n	\n	\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:3:{s:0:\"\";a:5:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:20:\"\n	Reacties op: Blog	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:24:\"https://nl.wordpress.org\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:13:\"lastBuildDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Tue, 05 Jan 2016 13:19:04 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:9:\"generator\";a:1:{i:0;a:5:{s:4:\"data\";s:40:\"https://wordpress.org/?v=5.5-alpha-47506\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:27:\"http://www.w3.org/2005/Atom\";a:1:{s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:1:{s:0:\"\";a:3:{s:4:\"href\";s:35:\"https://nl.wordpress.org/news/feed/\";s:3:\"rel\";s:4:\"self\";s:4:\"type\";s:19:\"application/rss+xml\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:44:\"http://purl.org/rss/1.0/modules/syndication/\";a:2:{s:12:\"updatePeriod\";a:1:{i:0;a:5:{s:4:\"data\";s:9:\"\n	hourly	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:15:\"updateFrequency\";a:1:{i:0;a:5:{s:4:\"data\";s:4:\"\n	1	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}}}}}}}}s:4:\"type\";i:128;s:7:\"headers\";O:42:\"Requests_Utility_CaseInsensitiveDictionary\":1:{s:7:\"\0*\0data\";a:8:{s:6:\"server\";s:5:\"nginx\";s:4:\"date\";s:29:\"Wed, 25 Mar 2020 14:35:17 GMT\";s:12:\"content-type\";s:34:\"application/rss+xml; charset=UTF-8\";s:6:\"x-olaf\";s:3:\"⛄\";s:13:\"last-modified\";s:29:\"Wed, 19 Feb 2020 10:16:02 GMT\";s:4:\"link\";a:2:{i:0;s:61:\"<https://nl.wordpress.org/wp-json/>; rel=\"https://api.w.org/\"\";i:1;s:48:\"<https://nl.wordpress.org/?p=473>; rel=shortlink\";}s:15:\"x-frame-options\";s:10:\"SAMEORIGIN\";s:4:\"x-nc\";s:9:\"HIT ord 2\";}}s:5:\"build\";s:14:\"20190927172710\";}', 'no'),
(460, '_transient_timeout_feed_mod_c326b61060938210a1df3d05d623467e', '1585190117', 'no'),
(461, '_transient_feed_mod_c326b61060938210a1df3d05d623467e', '1585146917', 'no'),
(463, '_transient_timeout_feed_26b0d8e18ed25a5313e8c7eb9c687d1b', '1585190118', 'no');
INSERT INTO `cmm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(464, '_transient_feed_26b0d8e18ed25a5313e8c7eb9c687d1b', 'a:4:{s:5:\"child\";a:1:{s:0:\"\";a:1:{s:3:\"rss\";a:1:{i:0;a:6:{s:4:\"data\";s:3:\"\n\n\n\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:7:\"version\";s:3:\"2.0\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:1:{s:0:\"\";a:1:{s:7:\"channel\";a:1:{i:0;a:6:{s:4:\"data\";s:49:\"\n	\n	\n	\n	\n	\n	\n	\n	\n	\n	\n		\n		\n		\n		\n		\n		\n		\n		\n		\n	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:3:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:13:\"WordPress.org\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:24:\"https://nl.wordpress.org\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:13:\"lastBuildDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Mon, 16 Dec 2019 16:18:14 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"language\";a:1:{i:0;a:5:{s:4:\"data\";s:2:\"nl\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:9:\"generator\";a:1:{i:0;a:5:{s:4:\"data\";s:40:\"https://wordpress.org/?v=5.5-alpha-47506\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"item\";a:10:{i:0;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:28:\"WordPress 5.3 is vrijgegeven\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:65:\"https://nl.wordpress.org/2019/11/13/wordpress-5-3-is-vrijgegeven/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:74:\"https://nl.wordpress.org/2019/11/13/wordpress-5-3-is-vrijgegeven/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 13 Nov 2019 10:11:19 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Release\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://nl.wordpress.org/?p=1064\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:407:\"We introduceren onze meest verfijnde gebruikerservaring met de verbeterde blokeditor in WordPress 5.3! WordPress 5.3 maakt de blokeditor, die is geïntroduceerd in WordPress 5.0, nog beter en het heeft nu ook een nieuwe blok, meer intuïtieve interacties en verbeterde toegankelijkheid. Nieuwe functies in de editor vergroten de ontwerp vrijheid, bieden extra lay-outopties en stijlvariaties zodat [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Remkus de Vries\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:5228:\"\n<p><strong>We introduceren onze meest verfijnde gebruikerservaring met de verbeterde blokeditor in WordPress 5.3!</strong></p>\n\n\n\n<p>WordPress 5.3 maakt de blokeditor, die is geïntroduceerd in WordPress 5.0, nog beter en het heeft nu ook een nieuwe blok, meer intuïtieve interacties en verbeterde toegankelijkheid. Nieuwe functies in de editor vergroten de ontwerp vrijheid, bieden extra lay-outopties en stijlvariaties zodat ontwerpers volledige controle hebben over het uiterlijk van hun website. </p>\n\n\n\n<p>Deze release introduceert ook het Twenty Twenty thema waardoor de gebruiker meer ontwerpflexibiliteit en integratie met de blokeditor heeft. Het maken van prachtige webpagina&#8217;s en geavanceerde lay-outs is nog nooit zo eenvoudig geweest.</p>\n\n\n\n<span id=\"more-1064\"></span>\n\n\n\n<h2>Verbeteringen blok-editor</h2>\n\n\n\n<p>Deze op verbeteringen gerichte update introduceert meer dan 150 nieuwe functies en gebruiksverbeteringen, waaronder verbeterde ondersteuning van grote afbeeldingen voor het uploaden van niet-geoptimaliseerde foto&#8217;s met een hoge resolutie die zijn gemaakt met je smartphone of andere hoogwaardige camera&#8217;s. In combinatie met grotere standaard afbeeldingsformaten zien foto&#8217;s er altijd op hun best uit.</p>\n\n\n\n<p>De verbeteringen van de toegankelijkheid omvat de integratie van stijlen voor blokeditor in de beheerinterface. Deze verbeterde stijlen lossen veel toegankelijkheidsproblemen op: kleurcontrast op formuliervelden en knoppen, consistentie tussen editor- en admin-interfaces, nieuwe notificaties, standaardisatie naar het standaard WordPress-kleurenschema en de introductie van animatie om interactie met je blokken snel en natuurlijk aan te laten voelen. </p>\n\n\n\n<p>Voor mensen die een toetsenbord gebruiken om door het dashboard te navigeren, heeft de blokeditor nu ook een navigatiemodus. Hiermee kan je van blok naar blok springen zonder door elk deel van de blokbesturing te bladeren.</p>\n\n\n\n<h2>Uitgebreide design flexibiliteit</h2>\n\n\n\n<p>WordPress 5.3 voegt nog meer gereedschappen toe om geweldige ontwerpen mee te maken.</p>\n\n\n\n<ul><li>Het nieuwe Groep blok staat je toe om jouw pagina makkelijk op te delen in kleurrijke secties.</li><li>Het Kolom blok ondersteund vanaf nu vaste kolombreedtes</li><li>De nieuwe vooraf gedefinieerde lay-outs maken het heel simpel om inhoud in geavanceerde lay-outs te ontwerpen</li><li>De tekstkleur van titelblokken is nu instelbaar</li><li>Met extra stijlopties kan je de gewenste stijl instellen voor elke blok dat deze functie ondersteunt</li></ul>\n\n\n\n<h2>Introductie Twenty Twenty</h2>\n\n\n\n<figure class=\"wp-block-image size-large\"><img src=\"https://s.w.org/images/core/5.3/twentytwenty-desktop.png\" alt=\"\" /></figure>\n\n\n\n<p>Terwijl de blokeditor zijn eerste verjaardag viert, zijn we er trots op dat Twenty Twenty is ontworpen met flexibiliteit in de kern. Pronk met je diensten of producten met een combinatie van kolommen, groepen en mediablokken. Stel je inhoud in op breed of volledige uitlijning voor dynamische en boeiende lay-outs.</p>\n\n\n\n<p>Twenty Twenty heeft, zoals het hoort, een sterke focus op overzichtbaarheid en leesbaarheid. Het thema bevat het typeface&nbsp;<a href=\"https://rsms.me/inter/\">Inter</a>&nbsp;wat ontworpen is door Rasmus Andersson. Inter is beschikbaar als een Variable Font versie, voor het eerst in een standaard thema, wat maakt dat de laadtijd kort houd door alle stijlen van Inter in slechts twee font bestanden beschikbaar zijn.</p>\n\n\n\n<h2>Verbeteringen voor iedereen</h2>\n\n\n\n<h3>Automatische afbeeldingsrotatie</h3>\n\n\n\n<p>Je afbeeldingen zullen nu op de correcte manier gedraaid zijn wanneer je ze uploadt gebaseerd op de orientatiedata. Deze verbetering is negen jaar geleden voorgesteld en mogelijk gemaakt door het doorzettingsvermogen van vele volhardende bijdragers.</p>\n\n\n\n<h3>Sitediagnose controles</h3>\n\n\n\n<p>De verbeteringen die geïntroduceerd worden in 5.3 maken het zelfs makkelijker om problemen te detecteren. Uitgebreide aanbevelingen brengen lichten bepaalde onderdelen uit die aandacht nodig hebben in het Health Check scherm.</p>\n\n\n\n<h3>Admin e-mail verificatie</h3>\n\n\n\n<p>Je wordt nu periodiek gevraagd om je admin email adres te bevestigen wanneer je ingelogd bent als een administrator. Dit maakt de kans een stuk kleiner dat je niet meer in je site kunt komen wanneer je je email adres wijzigt.</p>\n\n\n\n<h2>Voor ontwikkelaars</h2>\n\n\n\n<h3>Date/Time component verbeteringen</h3>\n\n\n\n<p>Ontwikkelaars kunnen nu werken met&nbsp;<a href=\"https://make.wordpress.org/core/2019/09/23/date-time-improvements-wp-5-3/\">data en tijdzones</a>&nbsp;in een betrouwbaardere manier. Datum en tijd functionaliteit heeft een aantal nieuwe API functies toegevoegd gekregen voor unified timezone retrieval en PHP interoperability en daarnaast zijn er ook een hoop bug fixes doorgevoerd.</p>\n\n\n\n<h3>PHP 7.4 compatibiliteit</h3>\n\n\n\n<p>WordPress 5.3 ondersteunt PHP 7.4 volledig. Deze release bevat&nbsp;<a href=\"https://make.wordpress.org/core/2019/10/11/wordpress-and-php-7-4/\">meerdere wijzigingen</a>&nbsp;die verouderde functionaliteit verwijderen en de compatibiliteit verzekeren. WordPress blijft gebruikers aanbevelen om de laatste en beste versie van PHP te gebruiken.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:70:\"https://nl.wordpress.org/2019/11/13/wordpress-5-3-is-vrijgegeven/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"1\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:1;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:28:\"WordPress 5.2 is vrijgegeven\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:65:\"https://nl.wordpress.org/2019/05/08/wordpress-5-2-is-vrijgegeven/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:74:\"https://nl.wordpress.org/2019/05/08/wordpress-5-2-is-vrijgegeven/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 08 May 2019 08:11:04 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Release\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://nl.wordpress.org/?p=1041\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:337:\"Met deze update is het gemakkelijker dan ooit om je site te repareren als er iets misgaat. Houd je site veilig WordPress 5.2 geeft je nog meer robuuste gereedschappen voor het identificeren en repareren van configuratieproblemen en fatale fouten. Of je nu een ontwikkelaar bent die klanten helpt of dat je zelf je site beheert, [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Remkus de Vries\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:3632:\"\n<p>Met deze update is het gemakkelijker dan ooit om je site te repareren als er iets misgaat.</p>\n\n\n\n<h2>Houd je site veilig</h2>\n\n\n\n<p>WordPress 5.2 geeft je nog meer robuuste gereedschappen voor het identificeren en repareren van configuratieproblemen en fatale fouten. Of je nu een ontwikkelaar bent die klanten helpt of dat je zelf je site beheert, deze gereedschappen kunnen je aan de juiste informatie helpen op het moment dat je het nodig hebt.</p>\n\n\n\n<figure class=\"wp-block-image\"><img src=\"https://s.w.org/images/core/5.2/about_maintain-wordpress-v2.svg\" alt=\"\" /></figure>\n\n\n\n<span id=\"more-1041\"></span>\n\n\n\n<h3>Sitediagnose controle</h3>\n\n\n\n<div class=\"wp-block-image\"><figure class=\"alignright\"><img src=\"https://s.w.org/images/core/5.2/about_site-health.svg\" alt=\"\" /></figure></div>\n\n\n\n<p>Voortbordurend op&nbsp;<a href=\"https://wordpress.org/news/2019/02/betty/\">de Sitediagnose features geïntroduceerd in 5.1</a>, voegt deze release twee nieuwe pagina&#8217;s toe om je te helpen veelvoorkomende problemen in de configuratie op te lossen. Er wordt ook een ruimte toegevoegd waar ontwikkelaars informatie kunnen toevoegen voor mensen die sites onderhouden.&nbsp;Ontdek meer of je site status, en&nbsp;leer hoe je issues kunt oplossen.</p>\n\n\n\n<h3>PHP foutbescherming</h3>\n\n\n\n<div class=\"wp-block-image\"><figure class=\"alignright\"><img src=\"https://s.w.org/images/core/5.2/about_error-protection.svg\" alt=\"\" /></figure></div>\n\n\n\n<p>Deze update met een focus op beheerders laat je op een veilige manier fatale fouten herstellen of beheren, zonder dat het ontwikkeltijd kost. Er is een betere afhandeling van de zogenaamde &#8220;white screen of death&#8221; en een manier om in herstelmodus te komen. De herstelmodus pauzeert plugins of thema&#8217;s die voor fouten zorgen.</p>\n\n\n\n<h3>Verbeteringen voor iedereen</h3>\n\n\n\n<h4>Toegankelijkheid updates</h4>\n\n\n\n<p>Een aantal verbeteringen werken samen om de contextuele positie en flow van de toetsenbordnavigatie te verbeteren voor zij die screenreaders en andere ondersteunende technologie gebruiken.</p>\n\n\n\n<h4>Nieuwe dashboard iconen</h4>\n\n\n\n<p>Dertien nieuwe icoontjes, inclusief Instagram, een aantal BuddyPress icoontjes en verschillende versies van de wereldbol zijn toegevoegd. Je vindt ze terug in het Dashboard. Veel plezier ermee!</p>\n\n\n\n<h3>Hier worden ontwikkelaars blij van</h3>\n\n\n\n<h4><a href=\"https://make.wordpress.org/core/2019/03/26/coding-standards-updates-for-php-5-6/\">PHP versie update</a></h4>\n\n\n\n<p>De minimum ondersteunde PHP versie is nu 5.6.20. Vanaf WordPress 5.2 kunnen thema&#8217;s en plugins veilig gebruik maken van de voordelen van namespaces, anonymous functions, en meer!</p>\n\n\n\n<h4><a href=\"https://make.wordpress.org/core/2019/04/24/developer-focused-privacy-updates-in-5-2/\">Privacy updates</a></h4>\n\n\n\n<p>Een nieuwe thema pagina template, een conditionele functie en twee CSS classes maken het ontwerpen en aanpassen van de Privacy Policy pagina eenvoudiger.</p>\n\n\n\n<h4><a href=\"https://make.wordpress.org/core/2019/04/24/miscellaneous-developer-updates-in-5-2/\">Nieuwe Body Tag hook</a></h4>\n\n\n\n<p>5.2 introduceert een&nbsp;<code>wp_body_open</code>&nbsp;hook die thema&#8217;s toestaat code toe te voegen helemaal aan het begin van het&nbsp;<code>&lt;body&gt;</code>&nbsp;element.</p>\n\n\n\n<h4><a href=\"https://make.wordpress.org/core/2019/03/25/building-javascript/\">JavaScript</a></h4>\n\n\n\n<p>Met de toevoeging van webpack en Babel instellingen in het wordpress/scripts package hoeven ontwikkelaars niet meer bezig te zijn om uit te zoeken hoe ze complexe build tools moeten gebruiken om moderne JavaScript the schrijven.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:70:\"https://nl.wordpress.org/2019/05/08/wordpress-5-2-is-vrijgegeven/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"1\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:2;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:28:\"WordPress 5.1 is vrijgegeven\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:65:\"https://nl.wordpress.org/2019/02/22/wordpress-5-1-is-vrijgegeven/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:73:\"https://nl.wordpress.org/2019/02/22/wordpress-5-1-is-vrijgegeven/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Fri, 22 Feb 2019 08:11:27 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Release\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://nl.wordpress.org/?p=1027\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:440:\"In navolging op WordPress 5.0—een grote release waar de nieuwe blok editor werd geïntroduceerd—is 5.1 vooral toegespitst op verfijning, specifiek performance-verbeteringen van de editor. Ook baant deze release een weg naar een betere, snellere en veiligere WordPress installatie met een aantal essentiële gereedschappen voor sitebeheerders en ontwikkelaars. Sitediagnose Deze release introduceert, met veiligheid en snelheid [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Remkus de Vries\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:3876:\"\n<p>In navolging op <a href=\"https://nl.wordpress.org/2018/12/07/wordpress-5-0-is-vrijgegeven/\">WordPress 5.0</a>—een grote release waar de nieuwe blok editor werd geïntroduceerd—is 5.1 vooral toegespitst op verfijning, specifiek performance-verbeteringen van de editor. Ook baant deze release een weg naar een betere, snellere en veiligere WordPress installatie met een aantal essentiële gereedschappen voor sitebeheerders en ontwikkelaars.</p>\n\n\n\n<h3>Sitediagnose</h3>\n\n\n\n<div class=\"wp-block-image\"><figure class=\"alignright\"><img src=\"https://s.w.org/images/core/5.1/site-health.svg\" alt=\"\" /></figure></div>\n\n\n\n<p>Deze release introduceert, met veiligheid en snelheid in het achterhoofd, de eerste&nbsp;<a href=\"https://make.wordpress.org/core/2019/01/14/php-site-health-mechanisms-in-5-1/\">Sitediagnose</a>&nbsp;kenmerken. Zo zal WordPress berichten gaan tonen aan beheerders van sites die zeer oude versies van PHP draaien, de programmeertaal waar WordPress in gebouwd is.</p>\n\n\n\n<p>Wanneer je nieuwe plugins installeert zal de Sitediagnose functie in WordPress controleren of een plugin een PHP versie nodig heeft die niet werkt met jouw site. Indien dit het geval is zal WordPress je niet toestaan de plugin te activeren.</p>\n\n\n\n<span id=\"more-1027\"></span>\n\n\n\n<h3>Snelheidsverbeteringen voor de editor</h3>\n\n\n\n<div class=\"wp-block-image\"><figure class=\"alignright\"><img src=\"https://s.w.org/images/core/5.1/editor-performance.svg\" alt=\"\" /></figure></div>\n\n\n\n<p>De nieuwe blok editor die in WordPress 5.0 is geïntroduceerd is verder verbeterd. De grootste verbetering is de snelheid waarmee de editor nu werkt in WordPress 5.1. De editor zou nu iets sneller moeten opstarten en typen zou vloeiender moeten voelen. Los daarvan, in de komende versies zullen er meer snelheidsverbeteringen doorgevoerd worden.</p>\n\n\n\n<h3>Hier worden ontwikkelaars blij van</h3>\n\n\n\n<h4>Multisite metagegevens</h4>\n\n\n\n<p>5.1 introduceert een nieuwe databasetabel om metadata op te slaan die geassocieerd is met sites en het mogelijk maakt om arbitraire site data op te slaan die relevant kan zijn in een multisite / netwerkcontext.	<br><a href=\"https://make.wordpress.org/core/2019/01/28/multisite-support-for-site-metadata-in-5-1/\">Lees meer.</a></p>\n\n\n\n<h4>Cron API</h4>\n\n\n\n<p>De Cron API is bijgewerkt met nieuwe functies voor returning data en bevat ook nieuwe filters voor het aanpassen van de cron storage. Andere wijzigingen hebben effect op cron spawning op servers die FastCGI en PHP-FPM versions 7.0.16 en daarboven draaien.	<br><a href=\"https://make.wordpress.org/core/2019/01/09/cron-improvements-with-php-fpm-in-wordpress-5-1/\">Lees meer.</a></p>\n\n\n\n<h4>Nieuw JS Build Proces</h4>\n\n\n\n<p>WordPress 5.1 introduceert een nieuw JavaScript build optie als gevolg van de grote reorganizatie van code die begon met de 5.0 release. <br><a href=\"https://make.wordpress.org/core/2018/05/16/preparing-wordpress-for-a-javascript-future-part-1-build-step-and-folder-reorganization/\">Lees meer.</a></p>\n\n\n\n<h4>Overige fijne zaken voor ontwikkelaars</h4>\n\n\n\n<p>Verschillende verbeteringen zoals updates voor de waarden die je kunt gebruiken in in de&nbsp;<code>WP_DEBUG_LOG</code>&nbsp;constant, een nieuwe test config file constant in de test suite, nieuwe plugin action hooks, short-circuit filters voor&nbsp;<code>wp_unique_post_slug()</code>&nbsp;en&nbsp;<code>WP_User_Query</code>&nbsp;en&nbsp;<code>count_users()</code>, een nieuwe&nbsp;<code>human_readable_duration</code>&nbsp;functie, verbeterde taxonomy metabox opschoning, gelimiteerde&nbsp;<code>LIKE</code>&nbsp;support voor meta keys wanneer je&nbsp;<code>WP_Meta_Query</code>&nbsp;gebruikt en een nieuwe “doing it wrong” melding wanneer je REST API endpoints registreert, en nog veel meer!	<br><a href=\"https://make.wordpress.org/core/2019/01/23/miscellaneous-developer-focused-changes-in-5-1/\">Lees meer.</a></p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:70:\"https://nl.wordpress.org/2019/02/22/wordpress-5-1-is-vrijgegeven/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:3;a:6:{s:4:\"data\";s:75:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n\n\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:8:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:28:\"WordPress 5.0 is vrijgegeven\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:65:\"https://nl.wordpress.org/2018/12/07/wordpress-5-0-is-vrijgegeven/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:73:\"https://nl.wordpress.org/2018/12/07/wordpress-5-0-is-vrijgegeven/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Fri, 07 Dec 2018 10:11:31 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Release\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:32:\"https://nl.wordpress.org/?p=1004\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:371:\"WordPress 5.0 is vrijgegeven. Deze nieuwe versie brengt één hele grote vernieuwing. We hebben namelijk een aantal grote veranderingen gemaakt in de editor. Onze nieuwe, op blokken gebaseerde editor, is de eerste stap richting een nieuwe toekomst met een gestroomlijnde editor ervaring op je site. Je hebt meer flexibiliteit over de manier hoe inhoud weergeven [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:9:\"enclosure\";a:2:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:1:{s:0:\"\";a:3:{s:3:\"url\";s:52:\"https://s.w.org/images/core/5.0/videos/add-block.mp4\";s:6:\"length\";s:7:\"8086508\";s:4:\"type\";s:9:\"video/mp4\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:1;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:1:{s:0:\"\";a:3:{s:3:\"url\";s:48:\"https://s.w.org/images/core/5.0/videos/build.mp4\";s:6:\"length\";s:7:\"2623964\";s:4:\"type\";s:9:\"video/mp4\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Remkus de Vries\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:7069:\"\n<figure class=\"wp-block-image\"><img src=\"https://s.w.org/images/core/5.0/header/Gutenberg1x.jpg\" alt=\"\" /></figure>\n\n\n\n<p>WordPress 5.0 is vrijgegeven. Deze nieuwe versie brengt één hele grote vernieuwing. We hebben namelijk een aantal grote veranderingen gemaakt in de editor. Onze nieuwe, op blokken gebaseerde editor, is de eerste stap richting een nieuwe toekomst met een gestroomlijnde editor ervaring op je site. Je hebt meer flexibiliteit over de manier hoe inhoud weergeven wordt. Of je nou je eerste site aan het maken bent, je blog nieuw leven inblaast of code schrijft als baan.</p>\n\n\n\n<span id=\"more-1004\"></span>\n\n\n\n<div class=\"wp-block-columns has-2-columns\">\n<div class=\"wp-block-column\">\n<figure class=\"wp-block-image is-resized\"><img src=\"https://s.w.org/images/core/5.0/features/Plugins1x.jpg\" alt=\"\" width=\"250\" height=\"250\" /><figcaption>Doe meer met minder plugins.</figcaption></figure>\n</div>\n\n\n\n<div class=\"wp-block-column\">\n<figure class=\"wp-block-image is-resized\"><img src=\"https://s.w.org/images/core/5.0/features/Layout1x.jpg\" alt=\"\" width=\"250\" height=\"250\" /><figcaption>Creëer moderne, multimedia rijke lay-outs.</figcaption></figure>\n</div>\n</div>\n\n\n\n<div class=\"wp-block-columns has-2-columns\">\n<div class=\"wp-block-column\">\n<figure class=\"wp-block-image is-resized\"><img src=\"https://s.w.org/images/core/5.0/features/Editor%20Styles1x.jpg\" alt=\"\" width=\"250\" height=\"250\" /><figcaption>Vertrouw dat de editor lijkt op je site.</figcaption></figure>\n</div>\n\n\n\n<div class=\"wp-block-column\">\n<figure class=\"wp-block-image is-resized\"><img src=\"https://s.w.org/images/core/5.0/features/Responsive1x.jpg\" alt=\"\" width=\"250\" height=\"250\" /><figcaption>Werkt op alle schermformaten en apparaten.</figcaption></figure>\n</div>\n</div>\n\n\n\n<h2>Bouwen met blokken</h2>\n\n\n\n<p>De nieuwe, op blokken gebaseerde editor, verandert niets aan de manier hoe je content er uit ziet voor bezoekers. Wat het wel doet, is het jou mogelijk maken elke multimedia soort super eenvoudig toe te voegen precies zoals jij dat voor ogen hebt. Elk stukje content is een afgebakend blok welke je eenvoudig kunt verplaatsen. Mocht je liever met HTML en CSS willen werken, dan staan de blokken je niet in de weg. WordPress is hier om het proces eenvoudiger te maken, niet het resultaat.</p>\n\n\n\n<figure class=\"wp-block-video\"><video controls src=\"https://s.w.org/images/core/5.0/videos/add-block.mp4\"></video></figure>\n\n\n\n<p>We hebben standaard al heel veel blokken beschikbaar, en elke dag worden er meer toegevoegd door de community. Begin een nieuw blok door de forward slash in te typen. Je krijgt dan direct te zien welke blokken er allemaal beschikbaar zijn.</p>\n\n\n\n<h3>Vrijheid om te bouwen, vrijheid om te schrijven</h3>\n\n\n\n<p>Deze nieuwe editor ervaring behandelt design en content op meer consistente manier. Wanneer je klantensites maakt kun je herbruikbare blokken maken. Hiermee kunnen je klanten steeds nieuwe content blijven toevoegen en blijft de uitstraling precies zoals je die bedoeld had.</p>\n\n\n\n<figure class=\"wp-block-video\"><video controls src=\"https://s.w.org/images/core/5.0/videos/build.mp4\"></video></figure>\n\n\n\n<hr class=\"wp-block-separator\" />\n\n\n\n<h2>Een verbluffend nieuw standaard thema</h2>\n\n\n\n<figure class=\"wp-block-image\"><img src=\"https://s.w.org/images/core/5.0/twenty%20nineteen/twenty-nineteen.webp\" alt=\"\" /><figcaption>De front-end van Twenty Nineteen aan de linkerkant en hoe het er uit ziet in de editor aan de rechterkant.</figcaption></figure>\n\n\n\n<p>De introductie van Twenty Nineteen, het nieuwe standaard thema welke de kracht van de nieuwe editor laat zien.</p>\n\n\n\n<h3>Ontworpen voor de Blok-editor</h3>\n\n\n\n<p>Twenty Nineteen bevat aangepaste styling voor de blokken die standaard beschikbaar zijn in 5.0. Het thema maakt uitgebreid gebruik van editor-styles. Op die manier is wat je in de editor aan het maken bent een reflectie van hoe het op de site zelf er uit ziet.</p>\n\n\n\n<h3>Eenvoudige, tekst gedreven lay-out</h3>\n\n\n\n<p>Twenty Nineteen bevat veel witruimte en moderne schreefloze koppen, gecombineerd met klassieke serif-tekst. Het thema gebruikt systeemlettertypes om de laadsnelheid te verhogen. Niet meer lang wachten op trage netwerken!</p>\n\n\n\n<figure class=\"wp-block-image\"><img src=\"https://s.w.org/images/core/5.0/twenty%20nineteen/twenty-nineteen-versatile.gif\" alt=\"\" /></figure>\n\n\n\n<h3>Veelzijdig ontwerp voor alle sites</h3>\n\n\n\n<p>Twenty Nineteen is ontworpen om te werken voor een breed scala aan toepassingen. Of je nu een foto-blog hebt, een nieuw bedrijf start of een non-profitorganisatie, Twenty Nineteen is flexibel genoeg om aan al je wensen te voldoen.</p>\n\n\n\n<hr class=\"wp-block-separator\" />\n\n\n\n<h2>Hier worden ontwikkelaars blij van</h2>\n\n\n\n<h3>Beschermen</h3>\n\n\n\n<p>Blokken bieden een fijne manier voor gebruikers om content direct te wijzigen en ze voorkomen dat de structuur van de content niet eenvoudig kan worden gestoord door onopzettelijke codebewerkingen. Dit staat de ontwikkelaar toe om controle te hebben op de output door gepolijste en semantische markup te leveren die bewaard blijft tijdens het bewerken.</p>\n\n\n\n<h3>Opstellen</h3>\n\n\n\n<p>Profiteer van een grote verzameling aan API&#8217;s en interface-componenten om eenvoudig blokken met een intuïtieve besturing voor je klanten te maken. Gebruik maken van deze componenten versnelt niet alleen het ontwikkelingswerk, maar biedt ook een consistentere, bruikbare en toegankelijke interface voor alle gebruikers.</p>\n\n\n\n<h3>Maak</h3>\n\n\n\n<p>Het nieuwe blokkenparadigma opent diverse mogelijkheden en verbeelding of hoe gebruikerswensen opgelost kunnen worden. Met eenduidige flow voor het toevoegen van een blok is eenvoudiger voor gebruikers om blokken te vinden voor alle soorten content. Ontwikkelaars kunnen zich richten op hun visie om verrijkte ervaringen te bieden, in plaats van werken met ingewikkelde API&#8217;s. <a href=\"https://wordpress.org/gutenberg/handbook/\">Leren hoe je moet starten</a></p>\n\n\n\n<hr class=\"wp-block-separator\" />\n\n\n\n<h2>Liever Klassiek?</h2>\n\n\n\n<figure class=\"wp-block-image\"><img src=\"https://s.w.org/images/core/5.0/classic/Classic.webp\" alt=\"\" /></figure>\n\n\n\n<p>Blijf je liever met de vertrouwde Klassieke editor werken? Dat is geen probleem! Ondersteuning voor de Klassieke editor plugin blijft aanwezig in WordPress tot en met 2021.</p>\n\n\n\n<p>De <a href=\"https://nl.wordpress.org/plugins/classic-editor/\">Klassieke editor</a> plugin herstelt de vorige WordPress editor en het &#8220;bericht bewerken&#8221; scherm. Hiermee kan je plugins blijven gebruiken die de klassieke editor uitbreiden, klassieke metaboxes toevoegen of op een andere manier afhankelijk zijn van de vorige editor. Bezoek de plugins pagina en klik de “Nu installeren” knop naast &#8220;Klassieke editor&#8221; om de plugin te installeren. Wanneer de plugin klaar is met installeren, klik op “Activeren”. Dat is alles!</p>\n\n\n\n<p>Opmerking voor gebruikers van computerhulpmiddelen: als je problemen met de bruikbaarheid ervaart met de Blok-editor, raden we je aan gebruik te maken van de Klassieke editor.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:70:\"https://nl.wordpress.org/2018/12/07/wordpress-5-0-is-vrijgegeven/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:4;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:30:\"WordCamps in Nederland in 2018\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:67:\"https://nl.wordpress.org/2018/01/10/wordcamps-in-nederland-in-2018/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:76:\"https://nl.wordpress.org/2018/01/10/wordcamps-in-nederland-in-2018/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Wed, 10 Jan 2018 10:11:02 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:8:\"WordCamp\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://nl.wordpress.org/?p=854\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:415:\"Beste WordCamp Nederland liefhebber, Zoals je mogelijk hebt kunnen zien moesten wij, als organisatie achter WordCamp Nederland, eerst 3 andere WordCamps in Nederland &#8220;laten gebeuren&#8221; voordat we weer een WordCamp Nederland mochten organiseren. Als WordPress Community hebben we hier werk van gemaakt. WordCamp Nijmegen en WordCamp Utrecht staan ondertussen al als twee succesvolle WordCamps die [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Remkus de Vries\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:2318:\"\n<p>Beste WordCamp Nederland liefhebber,</p>\n\n\n\n<p>Zoals je mogelijk hebt kunnen zien moesten wij, als organisatie achter WordCamp Nederland, eerst 3 andere WordCamps in Nederland &#8220;laten gebeuren&#8221; <a href=\"https://nl.wordpress.org/2017/06/15/toch-toekomst-voor-wordcamp-nederland/\" target=\"_blank\" rel=\"noopener noreferrer\">voordat we weer een WordCamp Nederland mochten organiseren</a>.</p>\n\n\n\n<p>Als WordPress Community hebben we hier werk van gemaakt. WordCamp Nijmegen en WordCamp Utrecht staan ondertussen al als twee succesvolle WordCamps die in de geschiedenisboeken.</p>\n\n\n\n<span id=\"more-854\"></span>\n\n\n\n<p>Maar daar stopt het niet. We hebben ook in 2018 al een aantal WordCamps op de planning staan. Dit zijn de twee die er op dit moment in de agenda staan:</p>\n\n\n\n<p><a href=\"https://2018.noordnederland.wordcamp.org\" target=\"_blank\" rel=\"noopener noreferrer\">WordCamp Noord-Nederland</a> in Drachten, 9 en 10 feb. <a href=\"https://2018.noordnederland.wordcamp.org/tickets/\" target=\"_blank\" rel=\"noopener noreferrer\">Tickets koop je hier</a>.</p>\n\n\n\n<p><a href=\"https://rotterdam.wordcamp.org\" target=\"_blank\" rel=\"noopener noreferrer\">WordCamp Rotterdam</a> in, je raadt het nooit, Rotterdam.&nbsp;23 &amp; 24 maart. <a href=\"https://2018.rotterdam.wordcamp.org/tickets/\" target=\"_blank\" rel=\"noopener noreferrer\">Tickets vind je hier</a>.</p>\n\n\n\n<p>Daarnaast zijn <a href=\"https://utrecht.wordcamp.org\" target=\"_blank\" rel=\"noopener noreferrer\">WordCamp Utrecht</a> en <a href=\"https://nijmegen.wordcamp.org\" target=\"_blank\" rel=\"noopener noreferrer\">WordCamp Nijmegen</a> alweer voorzichtig bezig met een planning. En uiteraard moeten we onze zuiderburen niet vergeten: WordCamp Antwerp gaat voor de tweede keer alweer los <a href=\"https://antwerp.wordcamp.org\" target=\"_blank\" rel=\"noopener noreferrer\">op 2 &amp; 3 maart 2018</a>.</p>\n\n\n\n<p>Al met al zul je de komende tijd steeds meer lokale WordCamps voorbij zien komen. Uiteraard komt er ook weer een WordCamp Nederland aan, maar we wachten nog even met het definitieve plannen hiervan.</p>\n\n\n\n<p>Zodra we meer weten laten we het jullie weten.</p>\n\n\n\n<p><strong>Update 6 juli 2018:</strong> <a href=\"https://nl.wordpress.org/team/2018/07/06/wordcamps-in-nederland-onze-ervaringen/\">WordCamps in Nederland &#8211; onze ervaringen</a></p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:72:\"https://nl.wordpress.org/2018/01/10/wordcamps-in-nederland-in-2018/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"1\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:5;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:47:\"Nederlanders krijgen commitrechten op WordPress\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:89:\"https://nl.wordpress.org/2017/12/12/nederlanders-krijgen-commitrechten-op-wordpress-core/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:98:\"https://nl.wordpress.org/2017/12/12/nederlanders-krijgen-commitrechten-op-wordpress-core/#comments\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Tue, 12 Dec 2017 08:42:14 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"WordPress Nieuws\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://nl.wordpress.org/?p=833\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:416:\"WordPress is een Open Source software-project. Dat betekent dat iedereen de broncode van WordPress kan bekijken en suggesties voor verbetering kan aandragen. Gebruikers maken WordPress daarom niet alleen voor elkaar, maar vooral ook met elkaar. Hoewel iedereen verbeteringen kan aandragen, is het daadwerkelijk toevoegen van code aan WordPress voorbehouden aan een vrij selecte groep ervaren WordPressers. [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"Taco Verdonschot\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:2973:\"<p>WordPress is een Open Source software-project. Dat betekent dat iedereen de broncode van WordPress kan bekijken en suggesties voor verbetering kan aandragen. Gebruikers maken WordPress daarom niet alleen voor elkaar, maar vooral ook met elkaar.</p>\n<p>Hoewel iedereen verbeteringen kan aandragen, is het daadwerkelijk toevoegen van code aan WordPress voorbehouden aan een vrij selecte groep ervaren WordPressers. Om dit te kunnen doen heb je zogenaamde commitrechten nodig. Je krijgt deze rechten niet zomaar, want het betekent dat je WordPress voor alle gebruikers kan aanpassen. Het gebeurt dus niet vaak dat er nieuwe committers (mensen met commitrechten) worden toegevoegd aan het project.<span id=\"more-833\"></span></p>\n<p>Extra bijzonder is het daarom dat maar liefst twee Nederlanders commitrechten hebben gekregen in de afgelopen 10 dagen. We zetten ze daarom graag even in het zonnetje.</p>\n<h3>Juliette Reinders Folmer</h3>\n<p><a href=\"https://profiles.wordpress.org/jrf\">Juliette</a> is geen onbekende in de WordPress community. Ze werkt al sinds 2004 met PHP en al sinds 2010 met WordPress, en heeft zeer actief bijgedragen aan diverse plugins en aan WordPress zelf. De afgelopen periode heeft Juliette zich vooral ingezet om te zorgen dat WordPress voldoet aan de WordPress codestandaard. Dit is een set afspraken die WordPress-ontwikkelaars hebben gemaakt over wat goede code is. Haar project zorgt ervoor dat meer dan 60.000 foutmeldingen en waarschuwingen in één klap worden opgelost. Indrukwekkend feit, deze ene patch (verbetering aan de software) heeft meer dan 25% van alle regels code in WordPress gewijzigd!<br />\nMeer weten? Op <a href=\"https://nijmegen.wordcamp.org\">WordCamp Nijmegen</a> heeft Juliette over deze patch gesproken. Je kan haar presentatie terugkijken op <a href=\"https://wordpress.tv/2017/10/14/juliette-reinders-folmer-the-biggest-wp-core-patch-ever/\">wordpress.tv</a>.</p>\n<h3>Anton Timmermans</h3>\n<p>Ook <a href=\"https://profiles.wordpress.org/atimmer\">Anton</a> loopt al lange tijd rond in het WordPress-wereldje. In 2012 ontdekte Anton WordPress, en nog geen jaar later werd zijn eerste verbetering (patch) toegevoegd aan WordPress. De afgelopen tijd heeft zich, samen met zijn collega&#8217;s van <a href=\"https://yoast.com\">Yoast</a>, ingezet voor het verbeteren van de JavaScript documentatie in WordPress. Hierdoor is het voor andere ontwikkelaars makkelijker om de JavaScript-code van WordPress te begrijpen en er op in te haken.</p>\n<h3>Zelf ook bijdragen?</h3>\n<p>Wil je zelf ook bijdragen aan het verbeteren van WordPress? Dat kan! Op <a href=\"https://make.wordpress.org\">make.wordpress.org</a> vind je een overzicht van de gebieden waarin je kan bijdragen. Elk team heeft een eigen &#8216;Getting started&#8217; sectie in hun handboek. Voor WordPress core kan je deze bijvoorbeeld vinden op <a href=\"https://make.wordpress.org/core/handbook/about/getting-started-at-a-contributor-day/\">Getting started</a>.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:94:\"https://nl.wordpress.org/2017/12/12/nederlanders-krijgen-commitrechten-op-wordpress-core/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"2\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:6;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:28:\"WordPress 4.9 is vrijgegeven\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:65:\"https://nl.wordpress.org/2017/11/16/wordpress-4-9-is-vrijgegeven/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:73:\"https://nl.wordpress.org/2017/11/16/wordpress-4-9-is-vrijgegeven/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 16 Nov 2017 22:23:59 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Release\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://nl.wordpress.org/?p=858\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:425:\"Met trots kunnen we melden dat WordPress 4.9 is vrijgegeven. WordPress 4.9 zal je ontwerp workflow vloeiend maken en je beschermen voor programmeerfouten. Belangrijke verbeteringen voor de Customizer, Foutcontrole en meer! 🎉 Welkom bij de verbeterde Customizer. Een verbeterde workflow met concept ontwerpen, ontwerpvergrendeling en voorbeeldlinks. En nog meer. Met syntaxmarkering en foutcontrole wordt het nog [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Remkus de Vries\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:8203:\"<p>Met trots kunnen we melden dat WordPress 4.9 is vrijgegeven. WordPress 4.9 zal je ontwerp workflow vloeiend maken en je beschermen voor programmeerfouten.</p>\n<h2>Belangrijke verbeteringen voor de Customizer, Foutcontrole en meer! <img src=\"https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f389.png\" alt=\"🎉\" class=\"wp-smiley\" style=\"height: 1em; max-height: 1em;\" /></h2>\n<p>Welkom bij de verbeterde Customizer. Een verbeterde workflow met concept ontwerpen, ontwerpvergrendeling en voorbeeldlinks. En nog meer. Met syntaxmarkering en foutcontrole wordt het nog makkelijk een site te ontwikkelen. En als klap op de vuurpijl is er een nieuwe galerij widget geïntroduceerd en zijn er diverse verbeteringen doorgevoerd aan themabeheer.<span id=\"more-858\"></span></p>\n<p><img class=\"alignnone size-full\" src=\"https://s.w.org/images/core/4.9/banner.svg\" width=\"1200\" height=\"600\" /></p>\n<h2>Verbeterde workflow Customizer</h2>\n<h3>Concept ontwerpaanpassingen inplannen</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/draft-and-schedule.svg\" alt=\"\" />Ja, je hebt het goed gelezen. Net zoals je concept berichten kunt opstellen, aanpassen en inplannen voor publicatie op een voor jou geschikt moment, kun je wijzigingen aan het ontwerp van je site nu zo plannen dat ze gepubliceerd worden wanneer jij dat wilt.</p>\n<h3>Samenwerken met conceptontwerp voorbeeldlinks</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/design-preview-links.svg\" alt=\"\" />Feedback nodig op voorgestelde site ontwerp wijzigingen? WordPress 4.9 geeft je een voorbeeld link die je naar je team en klanten kan sturen, zodat je feedback kan verzamelen en verwerken alvorens je je wijzigingen live zet. Dat is fijn samenwerken!</p>\n<h3>Ontwerpvergrendeling om wijzigingen te bewaken</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/locking.svg\" alt=\"\" />Heb je het ooit meegemaakt dat twee ontwikkelaars bij het project kwamen en elkaar verweten hun mooie aanpassingen te hebben overschreven? WordPress 4.9. ontwerpvergrendeling (vergelijkbaar met berichtvergrendeling) beschermd het concept ontwerp zodat niemand anders wijzigingen kan maken en alle het harde werk overschrijft.</p>\n<h3>Een melding om je werk te beschermen</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/prompt.svg\" alt=\"\" />Was je even niet bij je bureau voordat je het nieuwe concept ontwerp had opgeslagen? Geen zorgen, WordPress 4.9 zal je bij terugkeer vriendelijk vragen of je je wijzigingen wilt opslaan.</p>\n<h2>Codeverbeteringen</h2>\n<p>&nbsp;</p>\n<h3>Syntaxmarkering en foutcontrole? Ja, graag!</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/syntax-highlighting.svg\" alt=\"\" />Er is een weergaveprobleem, maar je kan maar niet vinden wat er fout is met de door jouw fantastisch geschreven CSS. Met syntaxmarkering en foutcontrole voor CSS en aangepaste HTML is het nog makkelijker geworden om fouten te vinden. Gegarandeerd eenvoudiger om fouten snel te verhelpen door eenvoudig de code te scannen.</p>\n<h3>Sandbox voor veiligheid</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/sandbox.svg\" alt=\"\" />Het gevreesde witte scherm. Je zal het kunnen voorkomen als je werkt met thema&#8217;s en plugin code, omdat WordPress 4.9 je zal waarschuwen alvorens een fout te bewaren. Je zal &#8217;s nachts beter slapen.</p>\n<h3>Waarschuwing: potentieel gevaar opkomst!</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/warning.svg\" alt=\"\" />Tijdens het direct bewerken van thema&#8217;s en plugins, zal WordPress 4.9 je vriendelijk waarschuwen dat dit een potentieel gevaar is. Een aanbeveling zal worden gegeven een backup te maken van je bestanden voordat je deze opslaat. Hierdoor worden wijzigingen niet overschreven bij een volgende update. Neem deze veilige route. Je toekomstige, je team en klanten zullen je dankbaar zijn voor deze beslissing.</p>\n<h2>Nog meer widget verbeteringen</h2>\n<h3>De nieuwe galerij widget</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/gallery-widget.svg\" alt=\"\" />Een extra verbetering aan de media aanpassingen die in WordPress 4.8 is uitgekomen. Nu is het mogelijk een galerij toe te voegen via widget. Fantastisch!</p>\n<h3>Druk op een knop, media toevoegen</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/media-button.svg\" alt=\"\" />Wil je media toevoegen aan je tekst-widget? Voeg afbeeldingen, video en audio direct in de widget met je tekst, met de simpele maar handige Voeg media toe knop.</p>\n<h2>Verbeteringen voor site-ontwikkeling</h2>\n<h3>Verbeterde themawisseling</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/theme-switching.svg\" alt=\"\" />Bij het wijzigen van een thema denken widgets soms dat ze zomaar van locatie kunnen verspringen. Verbeteringen in WordPress 4.9 zorgen er nu voor dat er een duidelijker menu en widget beheer wanneer je een nieuwe thema selecteert. Ook is mogelijk om eenvoudig een voorbeeld te tonen van geïnstalleerde thema&#8217;s of om een thema te downloaden, installeren en voorbeeld te tonen. Wel zo gemakkelijk om eerst een voorbeeld te bekijken voordat je deze activeert.</p>\n<h3>Betere menu instructies = Minder verwarring</h3>\n<p><img class=\"alignright\" src=\"https://s.w.org/images/core/4.9/menu-flow.svg\" alt=\"\" />Vond je de stappen om een nieuw menu te creëren verwarrend? Wellicht niet meer! We hebben de gebruikersinterface voor het maken van menu&#8217;s verbeterd zodat dit een stuk makkelijker gaat. Deze nieuwe update zal je hierin begeleiden.</p>\n<h2>Een handje meehelpen aan Gutenberg <img src=\"https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f91d.png\" alt=\"🤝\" class=\"wp-smiley\" style=\"height: 1em; max-height: 1em;\" /></h2>\n<p><img class=\"alignnone size-full\" src=\"https://s.w.org/images/core/4.9/gutenberg.svg\" width=\"1200\" height=\"600\" /><br />\nWordPress werkt aan een nieuwe manier om je inhoud te creëren en beheren en we stellen je hulp op prijs. Geïnteresseerd om een <a href=\"https://wordpress.org/plugins/gutenberg/\">vroege tester</a> te zijn of meehelpen met het Gutenberg project? <a href=\"https://github.com/WordPress/gutenberg\">Bijdragen op GitHub</a>.</p>\n<h2>Ontwikkelaarsgeluk <img src=\"https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f60a.png\" alt=\"😊\" class=\"wp-smiley\" style=\"height: 1em; max-height: 1em;\" /></h2>\n<h3><a href=\"https://make.wordpress.org/core/2017/11/01/improvements-to-the-customize-js-api-in-4-9/\">Customizer JS API verbeteringen</a></h3>\n<p>Er zijn diverse verbeteringen doorgevoerd aan de Customizer JS API in WordPress 4.9. Deze verbeteringen verwijderen diverse pijnpunten en maken het net zo makkelijk om mee te werken als de PHP API. Er zijn ook nieuwe standaarden, zoals: controle templates, een datum/tijd controle en een sectie/paneel notificatie toegevoegd. <a href=\"https://make.wordpress.org/core/2017/11/01/improvements-to-the-customize-js-api-in-4-9/\">Bekijk de hele lijst met verbeteringen.</a></p>\n<h3><a href=\"https://make.wordpress.org/core/2017/10/22/code-editing-improvements-in-wordpress-4-9/\">CodeMirror beschikbaar voor gebruik in je thema&#8217;s en plugins</a></h3>\n<p>We hebben een nieuwe codebewerkingsbibliotheek geïntroduceerd, CodeMirror, voor gebruik binnen WordPress. Gebruik het om iedere geschreven code, zoals CSS of JavaScript, binnen je plugins te verbeteren.</p>\n<h3><a href=\"https://make.wordpress.org/core/2017/10/30/mediaelement-upgrades-in-wordpress-4-9/\">MediaElement.js bijgewerkt naar 4.2.6</a></h3>\n<p>WordPress 4.9 bevat een bijgewerkte versie van MediaElement.js. Afhankelijkheden met jQuery zijn verwijderd, toegankelijkheid is verbeterd, gebruikersinterface is gemoderniseerd en veel bugs zijn gerepareerd.</p>\n<h3><a href=\"https://make.wordpress.org/core/2017/10/15/improvements-for-roles-and-capabilities-in-4-9/\">Verbeteringen aan rollen en rechten</a></h3>\n<p>Nieuwe eigenschappen zijn geïntroduceerd die het beheer van plugins en vertaalbestanden verfijnd mogelijk maken. Daarnaast is het siteschakelproces in multi-site nauwkeurig afgesteld om de beschikbare rollen en eigenschappen op een meer betrouwbare en coherente manier bij te werken.</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:70:\"https://nl.wordpress.org/2017/11/16/wordpress-4-9-is-vrijgegeven/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:7;a:6:{s:4:\"data\";s:76:\"\n		\n		\n					\n		\n		\n		\n				\n		\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:38:\"Toch toekomst voor WordCamp Nederland!\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:74:\"https://nl.wordpress.org/2017/06/15/toch-toekomst-voor-wordcamp-nederland/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:82:\"https://nl.wordpress.org/2017/06/15/toch-toekomst-voor-wordcamp-nederland/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 15 Jun 2017 14:33:13 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:2:{i:0;a:5:{s:4:\"data\";s:8:\"Algemeen\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}i:1;a:5:{s:4:\"data\";s:8:\"WordCamp\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://nl.wordpress.org/?p=754\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:349:\"Terugblik Op 30 maart dit jaar publiceerden we een bericht over het einde van WordCamp Nederland. Zoals velen van jullie hebben gemerkt heeft dat online geleid tot een stevige discussie. Vanuit de hele wereld ontvingen we berichten dat mensen het oneens waren met de beslissingen van het Global Community Team. Nu, een aantal maanden later [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:16:\"Taco Verdonschot\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:5484:\"<h2>Terugblik</h2>\n<p>Op 30 maart dit jaar publiceerden we een bericht over het einde van WordCamp Nederland. Zoals velen van jullie hebben gemerkt heeft dat online geleid tot een stevige discussie. Vanuit de hele wereld ontvingen we berichten dat mensen het oneens waren met de beslissingen van het Global Community Team. Nu, een aantal maanden later kijken we graag terug op wat er is gebeurd.<span id=\"more-754\"></span></p>\n<p>Als eerste willen we duidelijk maken dat er geen ‘wij’ en ‘zij’ is. We hebben altijd hetzelfde doel nagestreefd, namelijk de groei van de Nederlandse WordPress Community. Onze onenigheid ging slechts over juiste manier om dit te bereiken. Het Global Community Team was er van overtuigd dat het organiseren van meerdere lokale evenementen de juiste richting was, terwijl de WordCamp Nederland organisatie het laten groeien van de WordCamp Nederland zag als beste manier. Uiteindelijk besloot het Global Community Team dat WCNL 2017 niet door kon gaan.</p>\n<p>In de emotie van die beslissing hebben zowel de WCNL organisatie als het Global Community Team op het scherpst van de snede gecommuniceerd. Nu, een aantal maanden later, zijn we allemaal afgekoeld en hebben we een goed gesprek gehad over hoe we gezamenlijk verder kunnen.</p>\n<h2>Community Summit</h2>\n<p>Tijdens de Community Summit laaide het gesprek over regionale WordCamps opnieuw op. Ook hebben we hier gesproken over het verschil tussen regionale WordCamps en grote evenementen zoals WordCamp Europe. In een zeer open gesprek hebben we gesproken over de zorgen aan beide zijden, zoals geografie, het gevoel dat het niet laten doorgaan van WordCamp Nederland afbreuk deed aan de community, en ook dat de alternatieven niet duidelijk genoeg zijn gecommuniceerd.</p>\n<h2>De toekomst van WordCamps in Nederland</h2>\n<p>Het Global Community Team was erg duidelijk dat de huidige situatie (zonder WCNL) niet definitief is. Het nut van het verbieden van WCNL dit jaar is om een kans te creeren voor meer lokale teams om te proberen een evenement in hun eigen stad te organiseren. WordCamp Nederland kan absoluut weer opnieuw plaats gaan vinden. Om opnieuw toestemming te krijgen om WCNL te organiseren moeten we laten zien dat we gezonde lokale communities hebben. Dit wordt afgemeten aan het aantal Meetups en WordCamps.</p>\n<p>Kort gezegd, het Global Community Team heeft bevestigd dat we WordCamp Nederland opnieuw georganiseerd mag worden zodra we 3 verschillende, succesvolle WordCamps neerzetten in Nederland.</p>\n<p>Opmerking voor alle andere communities die dit bericht lezen: Omdat WordPress een wereldwijd project is worden deze richtlijnen opgesteld voor individuele situaties. Het aantal evenementen in een gebied kan verschillen, afhankelijk van de lokale situatie. Wil je ook een regionale WordCamp organiseren, neem dan contact op met het Global Community Team.</p>\n<p>&nbsp;</p>\n<hr />\n<p><em>[Below you&#8217;ll find the translation of the text above]</em></p>\n<h2>Looking back</h2>\n<p>On March 30th this year, we published a post about WordCamp The Netherlands not being organized anymore. As many of you know, it sparked quite a bit of discussion. From all over the world we received messages that people disagreed with the decisions made by Global Community Team. Now, a few months later we would like to look back on what happened.</p>\n<p>First of all, there’s no ‘us’ versus ‘them’. We’ve always had the same goals in mind, yet disagreed on the way to move the Dutch WordPress community forward. Global Community Team felt that we should move to more local WordCamps while the WCNL organization felt that WordCamp The Netherlands was the right way. In the end, Global Community Team decided WCNL 2017 wasn’t going to happen.</p>\n<p>In the emotion of that decision, both the WCNL organization and Global Community Team may have been a bit harsh in their communication. Now, a few months later things calmed down and we’ve been talking about a way forward together.</p>\n<h2>Community Summit</h2>\n<p>At the WordPress Community Summit, the conversation around regional WordCamps came up and we discussed the differences between regional events and large-scale events like WordCamp EU. We were able to communicate openly about the concerns on both sides including geography, the feeling that stopping WordCamp Netherlands was destructive, and also the fact that the alternatives communicated were not as clear as they could have been.</p>\n<h2>The future of WordCamps in The Netherlands</h2>\n<p>People from the Global Community Team were clear at the summit that this isn’t the final solution. The purpose of having no WordCamp Netherlands this year is to create the opportunity for more organizing teams to try having an event in their area. WordCamp The Netherlands can be organized again in the near future. To get permission again, we’ll have to show that we have healthy local communities. This is measured by the number of Meetups and WordCamps.</p>\n<p>Long story short, the Global Community Team confirmed that we have to have 3 different successful WordCamps in The Netherlands to qualify for a WordCamp The Netherlands again.</p>\n<p>Note for all other communities reading this post: Because WordPress is a global project, these guidelines are on a case by case basis. The number of events for any given area would depend on the local situation. Contact the Global Community Team if you’re interested in having your own regional WordCamp!</p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:79:\"https://nl.wordpress.org/2017/06/15/toch-toekomst-voor-wordcamp-nederland/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:8;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:28:\"WordPress 4.8 is vrijgegeven\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:65:\"https://nl.wordpress.org/2017/06/08/wordpress-4-8-is-vrijgegeven/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:73:\"https://nl.wordpress.org/2017/06/08/wordpress-4-8-is-vrijgegeven/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 08 Jun 2017 16:22:34 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:7:\"Release\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://nl.wordpress.org/?p=749\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:366:\"Een update met jou in de hoofdrol WordPress 4.8 voegt enkele fantastische nieuwe eigenschappen toe. Stap snel over voor een nog intuïtievere versie van WordPress. Al lijken sommige vernieuwingen klein, ze zijn gemaakt door honderden mensen met jou in gedachten. Maak je klaar voor nieuwe features die je zult verwelkomen als een oude vriend: verbeteringen [&#8230;]\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Remkus de Vries\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:7358:\"<h2>Een update met jou in de hoofdrol</h2>\n<p class=\"lead-description\">WordPress 4.8 voegt enkele fantastische nieuwe eigenschappen toe. Stap snel over voor een nog intuïtievere versie van WordPress.</p>\n<p>Al lijken sommige vernieuwingen klein, ze zijn gemaakt door honderden mensen met <em>jou</em> in gedachten. Maak je klaar voor nieuwe features die je zult verwelkomen als een oude vriend: verbeteringen aan links, <em>drie</em> nieuwe mediawigets voor afbeeldingen, audio en video, een verbeterde tekst widget die visueel bewerken ondersteunt, en een verbeterde nieuwssectie in je dashboard met informatie over WordPress evenementen die binnenkort bij jou in de buurt plaatsvinden.</p>\n<hr />\n<h2>Fantastische widget vernieuwingen</h2>\n<div class=\"headline-feature one-col\">\n<div class=\"col\"><img src=\"https://s.w.org/images/core/4.8/widgets-with-all-four-mobile_w_685.png\" alt=\"\" /></div>\n</div>\n<div class=\"feature-section two-col\">\n<div class=\"col\">\n<h3>Afbeeldingswidget</h3>\n<p>Een afbeelding toevoegen aan een widget is nu een eenvoudige taak die te doen is zonder kennis van code voor elke WordPress gebruiker. Je afbeelding eenvoudig toevoegen vanuit de widget instellingen. Probeer eens een foto van jezelf of van je nieuwste avonturen in het weekend — en zie hem als vanzelf verschijnen.</p>\n</div>\n<div class=\"col\">\n<h3>Videowidget</h3>\n<p>Een welkomstvideo is een geweldige manier om je website een menselijk gezicht te geven. Je kunt nu een video uit de mediabibliotheek toevoegen aan een sidebar op je site met de nieuwe video widget. Gebruik de videowidget om in een welkomstvideo de website bij je bezoekers te introduceren of promoot je nieuwste en beste inhoud.</p>\n</div>\n<div class=\"col\">\n<h3>Audiowidget</h3>\n<p>Ben jij een podcaster, muzikant of fanatieke blogger? Het toevoegen van een widget met je audiobestand is nog nooit zo eenvoudig geweest. Upload je audiobestand naar de mediabibliotheek, ga naar de widgetinstellingen, selecteer je bestand en je bent klaar voor luisteraars. Dit is ook een fantastische oplossing om welkomstbericht te tonen.</p>\n</div>\n<div class=\"col\">\n<h3>Richtekst widget</h3>\n<p>Deze nieuwe eigenschap is een echt paradepaardje. Rich-tekst bewerken is nu standaard voor tekstwidgets. Voeg ergens een widget toe en maak de tekst op zoals je wilt. Maak lijsten, maak tekst italic en voeg eenvoudig en snel links toe. Veel plezier met je nieuwe opmaakmogelijkheden en zie wat je snel voor elkaar kunt krijgen.</p>\n</div>\n</div>\n<hr />\n<div class=\"feature-section two-col\">\n<div class=\"col\">\n<h3>Link verbeteringen</h3>\n<p>Heb je ooit geprobeerd om een link bij te werken of de tekst naast een link te wijzigen? En het lukte niet om het goed te krijgen? Wanneer je tekst volgend op de link bewerkt, krijgt de nieuwe tekst ook de link. Of als je de tekst in de link bewerkt eindig je met tekst die niet gelinkt is. Dit kan heel frustrerend zijn! Maar met de nieuwe eigenschap link afbakening wordt het proces gestroomlijnd en zullen je links goed werken. Je zult er blij mee zijn. We beloven het</p>\n</div>\n<div class=\"col\">\n<div class=\"wp-video\">\n<div id=\"mep_0\" class=\"mejs-container svg wp-video-shortcode mejs-video\" role=\"application\">\n<div class=\"mejs-inner\">\n<div class=\"mejs-controls\">\n<div class=\"mejs-button mejs-fullscreen-button\"></div>\n</div>\n<div class=\"mejs-clear\"></div>\n</div>\n</div>\n</div>\n</div>\n</div>\n<hr />\n<div class=\"feature-section two-col\">\n<div class=\"col\">\n<h3>Dichtstbijzijnde WordPress evenementen</h3>\n<p>Wist je dat WordPress een bloeiende offline community heeft met groepen die regelmatig bijeenkomen in meer dan 400 steden rond de wereld? WordPress vestigt nu je aandacht op de events die je helpen om door te gaan met verbeteren van je WordPress vaardigheden, vrienden te ontmoeten en natuurlijk om te publiceren!</p>\n<p>Dit is snel een van onze favoriete eigenschappen aan het worden. Terwijl je bezig bent in het dashboard (omdat je updates draait en berichten schrijft, toch?) worden alle WordCamps en WordPress Meetups getoond — bij jou in de buurt — die binnenkort plaatsvinden.</p>\n<p>Meedoen aan de community helpt je om je WordPress vaardigheden te verbeteren en te netwerken met personen die je anders niet zou tegenkomen. Je kunt eenvoudig lokale evenementen vinden door in te loggen op het dashboard en daar de nieuwe evenementen en nieuws widget bekijken.</p>\n</div>\n<div class=\"col\"><img class=\"size-full wp-image-751 aligncenter\" src=\"https://nl.wordpress.org/files/2017/06/WordPress-evenementen-nieuws-widget.png\" alt=\"\" width=\"618\" height=\"378\" srcset=\"https://nl.wordpress.org/files/2017/06/WordPress-evenementen-nieuws-widget.png 618w, https://nl.wordpress.org/files/2017/06/WordPress-evenementen-nieuws-widget-300x183.png 300w\" sizes=\"(max-width: 618px) 100vw, 618px\" /></div>\n</div>\n<hr />\n<div class=\"changelog\">\n<h2>Zelfs nog meer ontwikkelaarsgeluk <img src=\"https://s.w.org/images/core/emoji/12.0.0-1/72x72/1f60a.png\" alt=\"😊\" class=\"wp-smiley\" style=\"height: 1em; max-height: 1em;\" /></h2>\n<div class=\"under-the-hood three-col\">\n<div class=\"col\">\n<h3><a href=\"https://make.wordpress.org/core/2017/05/17/cleaner-headings-in-the-admin-screens/\">Verbeterde toegankelijkheid admin paneel headings</a></h3>\n<p>Nieuwe CSS-regels betekent dat externe inhoud (zoals “Nieuwe toevoegen” links) niet meer toegevoegd moet worden in de admin headings. Deze panel-headings verbeteren de toegankelijkheid voor diegene die gebruik maken van hulpmiddelen.</p>\n</div>\n<div class=\"col\">\n<h3><a href=\"https://make.wordpress.org/core/2017/05/22/removal-of-core-embedding-support-for-wmv-and-wma-file-formats/\">Verwijderen van ondersteuning voor WMV en WMA bestanden</a></h3>\n<p>Omdat minder en minder browsers ondersteuning bieden voor Silverlight zijn bestandsformaten die de Silverlightplugin nodig hebben verwijderd. Bestanden worden nog steeds getoond als download link, maar worden niet meer automatisch ingevoegd.</p>\n</div>\n<div class=\"col\">\n<h3><a href=\"https://make.wordpress.org/core/2017/05/22/multisite-focused-changes-in-4-8/\">Multisite Updates</a></h3>\n<p>Nieuwe capabilities zijn geïntroduceerd in 4.8. met het oog op het verwijderen van aanroepen naar <code>is_super_admin()</code>. Verder zijn er nieuwe hooks en tweaks toegevoegd om precies het aantal sites en gebruikers per netwerk te controleren.</p>\n</div>\n<div class=\"col\">\n<h3><a href=\"https://make.wordpress.org/core/2017/05/23/addition-of-tinymce-to-the-text-widget/\">Text-Editor JavaScript API</a></h3>\n<p>Met het toevoegen van TinyMCE aan de tekstwidget in 4.8 komt een nieuwe JavaScript API die de editor activeert na het laden van de pagina. Dit kan worden gebruikt om de editor te activeren voor elk tekstvak waarbij je de knoppen en functies kan configureren. Geweldig voor pluginauteurs.</p>\n</div>\n<div class=\"col\">\n<h3><a href=\"https://make.wordpress.org/core/2017/05/26/media-widgets-for-images-video-and-audio/\">Mediawidgets API</a></h3>\n<p>De introductie van een nieuwe basis mediawidget REST API schema voor 4.8. biedt mogelijkheden voor meer mediawidgets (zoals galerijen of afspeellijsten) in de toekomst. De drie nieuwe mediawidgets worden ondersteund door een gedeelde class die zorg draagt voor de interactie met de mediamodel. Deze class maakt het ook eenvoudiger om nieuwe mediawidgets te maken and plaveit de weg voor nog meer.</p>\n</div>\n</div>\n</div>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:70:\"https://nl.wordpress.org/2017/06/08/wordpress-4-8-is-vrijgegeven/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}i:9;a:6:{s:4:\"data\";s:73:\"\n		\n		\n					\n		\n		\n		\n				\n		\n\n					\n										\n					\n					\n			\n		\n		\n			\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";s:5:\"child\";a:5:{s:0:\"\";a:7:{s:5:\"title\";a:1:{i:0;a:5:{s:4:\"data\";s:38:\"Het einde van WordCamp The Netherlands\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:75:\"https://nl.wordpress.org/2017/03/30/het-einde-van-wordcamp-the-netherlands/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:83:\"https://nl.wordpress.org/2017/03/30/het-einde-van-wordcamp-the-netherlands/#respond\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:7:\"pubDate\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"Thu, 30 Mar 2017 07:30:55 +0000\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:8:\"category\";a:1:{i:0;a:5:{s:4:\"data\";s:8:\"WordCamp\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:4:\"guid\";a:1:{i:0;a:5:{s:4:\"data\";s:31:\"https://nl.wordpress.org/?p=731\";s:7:\"attribs\";a:1:{s:0:\"\";a:1:{s:11:\"isPermaLink\";s:5:\"false\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:11:\"description\";a:1:{i:0;a:5:{s:4:\"data\";s:190:\"Op het WordPress NL teamblog wordt in een uitgebreide blog uitgelegd waarom er (voorlopig) geen WordCamp The Netherlands meer georganiseerd mag worden. Het einde van WordCamp The Netherlands\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:32:\"http://purl.org/dc/elements/1.1/\";a:1:{s:7:\"creator\";a:1:{i:0;a:5:{s:4:\"data\";s:15:\"Marcel Bootsman\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:40:\"http://purl.org/rss/1.0/modules/content/\";a:1:{s:7:\"encoded\";a:1:{i:0;a:5:{s:4:\"data\";s:780:\"<p>Op het WordPress NL teamblog wordt in een uitgebreide blog uitgelegd waarom er (voorlopig) geen WordCamp The Netherlands meer georganiseerd mag worden.</p>\n<blockquote class=\"wp-embedded-content\" data-secret=\"w0H3F05tfB\"><p><a href=\"https://nl.wordpress.org/team/2017/03/30/het-einde-van-wordcamp-the-netherlands/\">Het einde van WordCamp The Netherlands</a></p></blockquote>\n<p><iframe class=\"wp-embedded-content\" sandbox=\"allow-scripts\" security=\"restricted\" src=\"https://nl.wordpress.org/team/2017/03/30/het-einde-van-wordcamp-the-netherlands/embed/#?secret=w0H3F05tfB\" data-secret=\"w0H3F05tfB\" width=\"600\" height=\"338\" title=\"&#8220;Het einde van WordCamp The Netherlands&#8221; &#8212; Team NL\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\"></iframe></p>\n\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:36:\"http://wellformedweb.org/CommentAPI/\";a:1:{s:10:\"commentRss\";a:1:{i:0;a:5:{s:4:\"data\";s:80:\"https://nl.wordpress.org/2017/03/30/het-einde-van-wordcamp-the-netherlands/feed/\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:38:\"http://purl.org/rss/1.0/modules/slash/\";a:1:{s:8:\"comments\";a:1:{i:0;a:5:{s:4:\"data\";s:1:\"0\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}}}s:27:\"http://www.w3.org/2005/Atom\";a:1:{s:4:\"link\";a:1:{i:0;a:5:{s:4:\"data\";s:0:\"\";s:7:\"attribs\";a:1:{s:0:\"\";a:3:{s:4:\"href\";s:30:\"https://nl.wordpress.org/feed/\";s:3:\"rel\";s:4:\"self\";s:4:\"type\";s:19:\"application/rss+xml\";}}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}s:44:\"http://purl.org/rss/1.0/modules/syndication/\";a:2:{s:12:\"updatePeriod\";a:1:{i:0;a:5:{s:4:\"data\";s:9:\"\n	hourly	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}s:15:\"updateFrequency\";a:1:{i:0;a:5:{s:4:\"data\";s:4:\"\n	1	\";s:7:\"attribs\";a:0:{}s:8:\"xml_base\";s:0:\"\";s:17:\"xml_base_explicit\";b:0;s:8:\"xml_lang\";s:0:\"\";}}}}}}}}}}}}s:4:\"type\";i:128;s:7:\"headers\";O:42:\"Requests_Utility_CaseInsensitiveDictionary\":1:{s:7:\"\0*\0data\";a:8:{s:6:\"server\";s:5:\"nginx\";s:4:\"date\";s:29:\"Wed, 25 Mar 2020 14:35:17 GMT\";s:12:\"content-type\";s:34:\"application/rss+xml; charset=UTF-8\";s:6:\"x-olaf\";s:3:\"⛄\";s:13:\"last-modified\";s:29:\"Mon, 16 Dec 2019 16:18:14 GMT\";s:4:\"link\";s:61:\"<https://nl.wordpress.org/wp-json/>; rel=\"https://api.w.org/\"\";s:15:\"x-frame-options\";s:10:\"SAMEORIGIN\";s:4:\"x-nc\";s:9:\"HIT ord 2\";}}s:5:\"build\";s:14:\"20190927172710\";}', 'no');
INSERT INTO `cmm_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(465, '_transient_timeout_feed_mod_26b0d8e18ed25a5313e8c7eb9c687d1b', '1585190118', 'no'),
(466, '_transient_feed_mod_26b0d8e18ed25a5313e8c7eb9c687d1b', '1585146918', 'no'),
(467, '_transient_timeout_dash_v2_12fb51b99c5dfec05835445e04f970a4', '1585190118', 'no'),
(468, '_transient_dash_v2_12fb51b99c5dfec05835445e04f970a4', '<div class=\"rss-widget\"><ul><li>Er is een fout opgetreden, wat waarschijnlijk betekent dat de feed uit de lucht is. Probeer later opnieuw.</li></ul></div><div class=\"rss-widget\"><ul><li><a class=\'rsswidget\' href=\'https://nl.wordpress.org/2019/11/13/wordpress-5-3-is-vrijgegeven/\'>WordPress 5.3 is vrijgegeven</a></li><li><a class=\'rsswidget\' href=\'https://nl.wordpress.org/2019/05/08/wordpress-5-2-is-vrijgegeven/\'>WordPress 5.2 is vrijgegeven</a></li><li><a class=\'rsswidget\' href=\'https://nl.wordpress.org/2019/02/22/wordpress-5-1-is-vrijgegeven/\'>WordPress 5.1 is vrijgegeven</a></li></ul></div>', 'no'),
(477, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"Z7c77mQ7ypR6YvyDeXBnu50QU7ZbDwTJ\";}', 'yes'),
(478, 'action_scheduler_hybrid_store_demarkation', '71', 'yes'),
(479, 'schema-ActionScheduler_StoreSchema', '3.0.1585147592', 'yes'),
(480, 'schema-ActionScheduler_LoggerSchema', '2.0.1585147592', 'yes'),
(483, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(484, 'woocommerce_version', '4.0.1', 'yes'),
(486, '_transient_timeout_as-post-store-dependencies-met', '1585233993', 'no'),
(487, '_transient_as-post-store-dependencies-met', 'yes', 'no'),
(488, 'action_scheduler_lock_async-request-runner', '1585148466', 'yes'),
(489, '_transient_wc_attribute_taxonomies', 'a:2:{i:0;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"1\";s:14:\"attribute_name\";s:5:\"color\";s:15:\"attribute_label\";s:5:\"Color\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"0\";}i:1;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"2\";s:14:\"attribute_name\";s:4:\"size\";s:15:\"attribute_label\";s:4:\"Size\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"0\";}}', 'yes'),
(492, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(497, '_transient_timeout__woocommerce_helper_subscriptions', '1585148852', 'no'),
(498, '_transient__woocommerce_helper_subscriptions', 'a:0:{}', 'no'),
(499, 'rewrite_rules', 'a:158:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:9:\"winkel/?$\";s:27:\"index.php?post_type=product\";s:39:\"winkel/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:34:\"winkel/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:26:\"winkel/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:56:\"product-categorie/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:51:\"product-categorie/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:32:\"product-categorie/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:44:\"product-categorie/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:26:\"product-categorie/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=62&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:25:\"([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?name=$matches[1]&wc-api=$matches[3]\";s:31:\"[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(512, 'admin_email_lifespan', '1600700398', 'yes'),
(518, 'can_compress_scripts', '0', 'no'),
(525, 'woocommerce_onboarding_opt_in', 'no', 'yes'),
(526, 'woocommerce_admin_version', '0.19.0', 'yes'),
(529, '_transient_woocommerce_reports-transient-version', '1585148434', 'yes'),
(530, 'woocommerce_admin_install_timestamp', '1585148460', 'yes'),
(549, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/nl_NL/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"nl_NL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/nl_NL/wordpress-5.3.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1585148455;s:15:\"version_checked\";s:5:\"5.3.2\";s:12:\"translations\";a:0:{}}', 'no'),
(550, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1585148457;s:7:\"checked\";a:4:{s:41:\"better-wp-security/better-wp-security.php\";s:5:\"7.6.1\";s:27:\"woocommerce/woocommerce.php\";s:5:\"4.0.1\";s:39:\"woocommerce-admin/woocommerce-admin.php\";s:5:\"1.0.3\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"13.3\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:4:{s:41:\"better-wp-security/better-wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/better-wp-security\";s:4:\"slug\";s:18:\"better-wp-security\";s:6:\"plugin\";s:41:\"better-wp-security/better-wp-security.php\";s:11:\"new_version\";s:5:\"7.6.1\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/better-wp-security/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/better-wp-security.7.6.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:70:\"https://ps.w.org/better-wp-security/assets/icon-256x256.jpg?rev=969999\";s:2:\"1x\";s:62:\"https://ps.w.org/better-wp-security/assets/icon.svg?rev=970042\";s:3:\"svg\";s:62:\"https://ps.w.org/better-wp-security/assets/icon.svg?rev=970042\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/better-wp-security/assets/banner-772x250.png?rev=881897\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"4.0.1\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.4.0.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}}s:39:\"woocommerce-admin/woocommerce-admin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/woocommerce-admin\";s:4:\"slug\";s:17:\"woocommerce-admin\";s:6:\"plugin\";s:39:\"woocommerce-admin/woocommerce-admin.php\";s:11:\"new_version\";s:5:\"1.0.3\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/woocommerce-admin/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce-admin.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/woocommerce-admin/assets/icon-256x256.jpg?rev=2057866\";s:2:\"1x\";s:70:\"https://ps.w.org/woocommerce-admin/assets/icon-128x128.jpg?rev=2057866\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/woocommerce-admin/assets/banner-1544x500.jpg?rev=2057866\";s:2:\"1x\";s:72:\"https://ps.w.org/woocommerce-admin/assets/banner-772x250.jpg?rev=2057866\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"13.3\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.13.3.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no'),
(551, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1585148457;s:7:\"checked\";a:2:{s:10:\"storefront\";s:5:\"2.5.5\";s:12:\"twentytwenty\";s:3:\"1.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_postmeta`
--

CREATE TABLE `cmm_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_postmeta`
--

INSERT INTO `cmm_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', 'woocommerce-placeholder.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1200x1200.png\";s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:9:\"mime-type\";s:9:\"image/png\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(6, 11, '_wp_attached_file', '2019/10/sample_data.csv'),
(7, 11, '_wp_attachment_context', 'import'),
(8, 12, '_sku', 'woo-vneck-tee'),
(9, 12, 'total_sales', '0'),
(10, 12, '_tax_status', 'taxable'),
(11, 12, '_tax_class', ''),
(12, 12, '_manage_stock', 'no'),
(13, 12, '_backorders', 'no'),
(14, 12, '_sold_individually', 'no'),
(15, 12, '_virtual', 'no'),
(16, 12, '_downloadable', 'no'),
(17, 12, '_download_limit', '0'),
(18, 12, '_download_expiry', '0'),
(19, 12, '_stock', NULL),
(20, 12, '_stock_status', 'instock'),
(21, 12, '_wc_average_rating', '0'),
(22, 12, '_wc_review_count', '0'),
(23, 12, '_product_version', '3.7.0'),
(25, 13, '_sku', 'woo-hoodie'),
(26, 13, 'total_sales', '0'),
(27, 13, '_tax_status', 'taxable'),
(28, 13, '_tax_class', ''),
(29, 13, '_manage_stock', 'no'),
(30, 13, '_backorders', 'no'),
(31, 13, '_sold_individually', 'no'),
(32, 13, '_virtual', 'no'),
(33, 13, '_downloadable', 'no'),
(34, 13, '_download_limit', '0'),
(35, 13, '_download_expiry', '0'),
(36, 13, '_stock', NULL),
(37, 13, '_stock_status', 'instock'),
(38, 13, '_wc_average_rating', '0'),
(39, 13, '_wc_review_count', '0'),
(40, 13, '_product_version', '3.7.0'),
(42, 14, '_sku', 'woo-hoodie-with-logo'),
(43, 14, 'total_sales', '0'),
(44, 14, '_tax_status', 'taxable'),
(45, 14, '_tax_class', ''),
(46, 14, '_manage_stock', 'no'),
(47, 14, '_backorders', 'no'),
(48, 14, '_sold_individually', 'no'),
(49, 14, '_virtual', 'no'),
(50, 14, '_downloadable', 'no'),
(51, 14, '_download_limit', '0'),
(52, 14, '_download_expiry', '0'),
(53, 14, '_stock', NULL),
(54, 14, '_stock_status', 'instock'),
(55, 14, '_wc_average_rating', '0'),
(56, 14, '_wc_review_count', '0'),
(57, 14, '_product_version', '3.7.0'),
(59, 15, '_sku', 'woo-paasei'),
(60, 15, 'total_sales', '0'),
(61, 15, '_tax_status', 'taxable'),
(62, 15, '_tax_class', ''),
(63, 15, '_manage_stock', 'no'),
(64, 15, '_backorders', 'no'),
(65, 15, '_sold_individually', 'no'),
(66, 15, '_virtual', 'no'),
(67, 15, '_downloadable', 'no'),
(68, 15, '_download_limit', '0'),
(69, 15, '_download_expiry', '0'),
(70, 15, '_stock', NULL),
(71, 15, '_stock_status', 'instock'),
(72, 15, '_wc_average_rating', '0'),
(73, 15, '_wc_review_count', '0'),
(74, 15, '_product_version', '3.7.0'),
(76, 16, '_sku', 'woo-beanie'),
(77, 16, 'total_sales', '0'),
(78, 16, '_tax_status', 'taxable'),
(79, 16, '_tax_class', ''),
(80, 16, '_manage_stock', 'no'),
(81, 16, '_backorders', 'no'),
(82, 16, '_sold_individually', 'no'),
(83, 16, '_virtual', 'no'),
(84, 16, '_downloadable', 'no'),
(85, 16, '_download_limit', '0'),
(86, 16, '_download_expiry', '0'),
(87, 16, '_stock', NULL),
(88, 16, '_stock_status', 'instock'),
(89, 16, '_wc_average_rating', '0'),
(90, 16, '_wc_review_count', '0'),
(91, 16, '_product_version', '3.7.0'),
(93, 17, '_sku', 'woo-belt'),
(94, 17, 'total_sales', '0'),
(95, 17, '_tax_status', 'taxable'),
(96, 17, '_tax_class', ''),
(97, 17, '_manage_stock', 'no'),
(98, 17, '_backorders', 'no'),
(99, 17, '_sold_individually', 'no'),
(100, 17, '_virtual', 'no'),
(101, 17, '_downloadable', 'no'),
(102, 17, '_download_limit', '0'),
(103, 17, '_download_expiry', '0'),
(104, 17, '_stock', NULL),
(105, 17, '_stock_status', 'instock'),
(106, 17, '_wc_average_rating', '0'),
(107, 17, '_wc_review_count', '0'),
(108, 17, '_product_version', '3.7.0'),
(110, 18, '_sku', 'woo-cap'),
(111, 18, 'total_sales', '0'),
(112, 18, '_tax_status', 'taxable'),
(113, 18, '_tax_class', ''),
(114, 18, '_manage_stock', 'no'),
(115, 18, '_backorders', 'no'),
(116, 18, '_sold_individually', 'no'),
(117, 18, '_virtual', 'no'),
(118, 18, '_downloadable', 'no'),
(119, 18, '_download_limit', '0'),
(120, 18, '_download_expiry', '0'),
(121, 18, '_stock', NULL),
(122, 18, '_stock_status', 'instock'),
(123, 18, '_wc_average_rating', '0'),
(124, 18, '_wc_review_count', '0'),
(125, 18, '_product_version', '3.7.0'),
(127, 19, '_sku', 'woo-sunglasses'),
(128, 19, 'total_sales', '0'),
(129, 19, '_tax_status', 'taxable'),
(130, 19, '_tax_class', ''),
(131, 19, '_manage_stock', 'no'),
(132, 19, '_backorders', 'no'),
(133, 19, '_sold_individually', 'no'),
(134, 19, '_virtual', 'no'),
(135, 19, '_downloadable', 'no'),
(136, 19, '_download_limit', '0'),
(137, 19, '_download_expiry', '0'),
(138, 19, '_stock', NULL),
(139, 19, '_stock_status', 'instock'),
(140, 19, '_wc_average_rating', '0'),
(141, 19, '_wc_review_count', '0'),
(142, 19, '_product_version', '3.7.0'),
(144, 20, '_sku', 'woo-hoodie-with-pocket'),
(145, 20, 'total_sales', '0'),
(146, 20, '_tax_status', 'taxable'),
(147, 20, '_tax_class', ''),
(148, 20, '_manage_stock', 'no'),
(149, 20, '_backorders', 'no'),
(150, 20, '_sold_individually', 'no'),
(151, 20, '_virtual', 'no'),
(152, 20, '_downloadable', 'no'),
(153, 20, '_download_limit', '0'),
(154, 20, '_download_expiry', '0'),
(155, 20, '_stock', NULL),
(156, 20, '_stock_status', 'instock'),
(157, 20, '_wc_average_rating', '0'),
(158, 20, '_wc_review_count', '0'),
(159, 20, '_product_version', '3.7.0'),
(161, 21, '_sku', 'woo-hoodie-with-zipper'),
(162, 21, 'total_sales', '0'),
(163, 21, '_tax_status', 'taxable'),
(164, 21, '_tax_class', ''),
(165, 21, '_manage_stock', 'no'),
(166, 21, '_backorders', 'no'),
(167, 21, '_sold_individually', 'no'),
(168, 21, '_virtual', 'no'),
(169, 21, '_downloadable', 'no'),
(170, 21, '_download_limit', '0'),
(171, 21, '_download_expiry', '0'),
(172, 21, '_stock', NULL),
(173, 21, '_stock_status', 'instock'),
(174, 21, '_wc_average_rating', '0'),
(175, 21, '_wc_review_count', '0'),
(176, 21, '_product_version', '3.7.0'),
(178, 22, '_sku', 'woo-long-sleeve-tee'),
(179, 22, 'total_sales', '0'),
(180, 22, '_tax_status', 'taxable'),
(181, 22, '_tax_class', ''),
(182, 22, '_manage_stock', 'no'),
(183, 22, '_backorders', 'no'),
(184, 22, '_sold_individually', 'no'),
(185, 22, '_virtual', 'no'),
(186, 22, '_downloadable', 'no'),
(187, 22, '_download_limit', '0'),
(188, 22, '_download_expiry', '0'),
(189, 22, '_stock', NULL),
(190, 22, '_stock_status', 'instock'),
(191, 22, '_wc_average_rating', '0'),
(192, 22, '_wc_review_count', '0'),
(193, 22, '_product_version', '3.7.0'),
(195, 23, '_sku', 'woo-polo'),
(196, 23, 'total_sales', '0'),
(197, 23, '_tax_status', 'taxable'),
(198, 23, '_tax_class', ''),
(199, 23, '_manage_stock', 'no'),
(200, 23, '_backorders', 'no'),
(201, 23, '_sold_individually', 'no'),
(202, 23, '_virtual', 'no'),
(203, 23, '_downloadable', 'no'),
(204, 23, '_download_limit', '0'),
(205, 23, '_download_expiry', '0'),
(206, 23, '_stock', NULL),
(207, 23, '_stock_status', 'instock'),
(208, 23, '_wc_average_rating', '0'),
(209, 23, '_wc_review_count', '0'),
(210, 23, '_product_version', '3.7.0'),
(212, 24, '_sku', 'woo-album'),
(213, 24, 'total_sales', '0'),
(214, 24, '_tax_status', 'taxable'),
(215, 24, '_tax_class', ''),
(216, 24, '_manage_stock', 'no'),
(217, 24, '_backorders', 'no'),
(218, 24, '_sold_individually', 'no'),
(219, 24, '_virtual', 'yes'),
(220, 24, '_downloadable', 'yes'),
(221, 24, '_download_limit', '1'),
(222, 24, '_download_expiry', '1'),
(223, 24, '_stock', NULL),
(224, 24, '_stock_status', 'instock'),
(225, 24, '_wc_average_rating', '0'),
(226, 24, '_wc_review_count', '0'),
(227, 24, '_product_version', '3.7.0'),
(229, 25, '_sku', 'woo-single'),
(230, 25, 'total_sales', '0'),
(231, 25, '_tax_status', 'taxable'),
(232, 25, '_tax_class', ''),
(233, 25, '_manage_stock', 'no'),
(234, 25, '_backorders', 'no'),
(235, 25, '_sold_individually', 'no'),
(236, 25, '_virtual', 'yes'),
(237, 25, '_downloadable', 'yes'),
(238, 25, '_download_limit', '1'),
(239, 25, '_download_expiry', '1'),
(240, 25, '_stock', NULL),
(241, 25, '_stock_status', 'instock'),
(242, 25, '_wc_average_rating', '0'),
(243, 25, '_wc_review_count', '0'),
(244, 25, '_product_version', '3.7.0'),
(246, 26, '_sku', 'woo-vneck-tee-red'),
(247, 26, 'total_sales', '0'),
(248, 26, '_tax_status', 'taxable'),
(249, 26, '_tax_class', ''),
(250, 26, '_manage_stock', 'no'),
(251, 26, '_backorders', 'no'),
(252, 26, '_sold_individually', 'no'),
(253, 26, '_virtual', 'no'),
(254, 26, '_downloadable', 'no'),
(255, 26, '_download_limit', '0'),
(256, 26, '_download_expiry', '0'),
(257, 26, '_stock', NULL),
(258, 26, '_stock_status', 'instock'),
(259, 26, '_wc_average_rating', '0'),
(260, 26, '_wc_review_count', '0'),
(261, 26, '_product_version', '3.7.0'),
(263, 27, '_sku', 'woo-vneck-tee-green'),
(264, 27, 'total_sales', '0'),
(265, 27, '_tax_status', 'taxable'),
(266, 27, '_tax_class', ''),
(267, 27, '_manage_stock', 'no'),
(268, 27, '_backorders', 'no'),
(269, 27, '_sold_individually', 'no'),
(270, 27, '_virtual', 'no'),
(271, 27, '_downloadable', 'no'),
(272, 27, '_download_limit', '0'),
(273, 27, '_download_expiry', '0'),
(274, 27, '_stock', NULL),
(275, 27, '_stock_status', 'instock'),
(276, 27, '_wc_average_rating', '0'),
(277, 27, '_wc_review_count', '0'),
(278, 27, '_product_version', '3.7.0'),
(280, 28, '_sku', 'woo-vneck-tee-blue'),
(281, 28, 'total_sales', '0'),
(282, 28, '_tax_status', 'taxable'),
(283, 28, '_tax_class', ''),
(284, 28, '_manage_stock', 'no'),
(285, 28, '_backorders', 'no'),
(286, 28, '_sold_individually', 'no'),
(287, 28, '_virtual', 'no'),
(288, 28, '_downloadable', 'no'),
(289, 28, '_download_limit', '0'),
(290, 28, '_download_expiry', '0'),
(291, 28, '_stock', NULL),
(292, 28, '_stock_status', 'instock'),
(293, 28, '_wc_average_rating', '0'),
(294, 28, '_wc_review_count', '0'),
(295, 28, '_product_version', '3.7.0'),
(297, 29, '_sku', 'woo-hoodie-red'),
(298, 29, 'total_sales', '0'),
(299, 29, '_tax_status', 'taxable'),
(300, 29, '_tax_class', ''),
(301, 29, '_manage_stock', 'no'),
(302, 29, '_backorders', 'no'),
(303, 29, '_sold_individually', 'no'),
(304, 29, '_virtual', 'no'),
(305, 29, '_downloadable', 'no'),
(306, 29, '_download_limit', '0'),
(307, 29, '_download_expiry', '0'),
(308, 29, '_stock', NULL),
(309, 29, '_stock_status', 'instock'),
(310, 29, '_wc_average_rating', '0'),
(311, 29, '_wc_review_count', '0'),
(312, 29, '_product_version', '3.7.0'),
(314, 30, '_sku', 'woo-hoodie-green'),
(315, 30, 'total_sales', '0'),
(316, 30, '_tax_status', 'taxable'),
(317, 30, '_tax_class', ''),
(318, 30, '_manage_stock', 'no'),
(319, 30, '_backorders', 'no'),
(320, 30, '_sold_individually', 'no'),
(321, 30, '_virtual', 'no'),
(322, 30, '_downloadable', 'no'),
(323, 30, '_download_limit', '0'),
(324, 30, '_download_expiry', '0'),
(325, 30, '_stock', NULL),
(326, 30, '_stock_status', 'instock'),
(327, 30, '_wc_average_rating', '0'),
(328, 30, '_wc_review_count', '0'),
(329, 30, '_product_version', '3.7.0'),
(331, 31, '_sku', 'woo-hoodie-blue'),
(332, 31, 'total_sales', '0'),
(333, 31, '_tax_status', 'taxable'),
(334, 31, '_tax_class', ''),
(335, 31, '_manage_stock', 'no'),
(336, 31, '_backorders', 'no'),
(337, 31, '_sold_individually', 'no'),
(338, 31, '_virtual', 'no'),
(339, 31, '_downloadable', 'no'),
(340, 31, '_download_limit', '0'),
(341, 31, '_download_expiry', '0'),
(342, 31, '_stock', NULL),
(343, 31, '_stock_status', 'instock'),
(344, 31, '_wc_average_rating', '0'),
(345, 31, '_wc_review_count', '0'),
(346, 31, '_product_version', '3.7.0'),
(348, 32, '_sku', 'Woo-tshirt-logo'),
(349, 32, 'total_sales', '0'),
(350, 32, '_tax_status', 'taxable'),
(351, 32, '_tax_class', ''),
(352, 32, '_manage_stock', 'no'),
(353, 32, '_backorders', 'no'),
(354, 32, '_sold_individually', 'no'),
(355, 32, '_virtual', 'no'),
(356, 32, '_downloadable', 'no'),
(357, 32, '_download_limit', '0'),
(358, 32, '_download_expiry', '0'),
(359, 32, '_stock', NULL),
(360, 32, '_stock_status', 'instock'),
(361, 32, '_wc_average_rating', '0'),
(362, 32, '_wc_review_count', '0'),
(363, 32, '_product_version', '3.7.0'),
(365, 33, '_sku', 'Woo-beanie-logo'),
(366, 33, 'total_sales', '0'),
(367, 33, '_tax_status', 'taxable'),
(368, 33, '_tax_class', ''),
(369, 33, '_manage_stock', 'no'),
(370, 33, '_backorders', 'no'),
(371, 33, '_sold_individually', 'no'),
(372, 33, '_virtual', 'no'),
(373, 33, '_downloadable', 'no'),
(374, 33, '_download_limit', '0'),
(375, 33, '_download_expiry', '0'),
(376, 33, '_stock', NULL),
(377, 33, '_stock_status', 'instock'),
(378, 33, '_wc_average_rating', '0'),
(379, 33, '_wc_review_count', '0'),
(380, 33, '_product_version', '3.7.0'),
(382, 34, '_sku', 'logo-collection'),
(383, 34, 'total_sales', '0'),
(384, 34, '_tax_status', 'taxable'),
(385, 34, '_tax_class', ''),
(386, 34, '_manage_stock', 'no'),
(387, 34, '_backorders', 'no'),
(388, 34, '_sold_individually', 'no'),
(389, 34, '_virtual', 'no'),
(390, 34, '_downloadable', 'no'),
(391, 34, '_download_limit', '0'),
(392, 34, '_download_expiry', '0'),
(393, 34, '_stock', NULL),
(394, 34, '_stock_status', 'instock'),
(395, 34, '_wc_average_rating', '0'),
(396, 34, '_wc_review_count', '0'),
(397, 34, '_product_version', '3.7.0'),
(415, 36, '_sku', 'wp-pennant'),
(416, 36, 'total_sales', '0'),
(417, 36, '_tax_status', 'taxable'),
(418, 36, '_tax_class', ''),
(419, 36, '_manage_stock', 'no'),
(420, 36, '_backorders', 'no'),
(421, 36, '_sold_individually', 'no'),
(422, 36, '_virtual', 'no'),
(423, 36, '_downloadable', 'no'),
(424, 36, '_download_limit', '0'),
(425, 36, '_download_expiry', '0'),
(426, 36, '_stock', NULL),
(427, 36, '_stock_status', 'instock'),
(428, 36, '_wc_average_rating', '0'),
(429, 36, '_wc_review_count', '0'),
(430, 36, '_product_version', '3.7.0'),
(432, 37, '_sku', 'woo-hoodie-blue-logo'),
(433, 37, 'total_sales', '0'),
(434, 37, '_tax_status', 'taxable'),
(435, 37, '_tax_class', ''),
(436, 37, '_manage_stock', 'no'),
(437, 37, '_backorders', 'no'),
(438, 37, '_sold_individually', 'no'),
(439, 37, '_virtual', 'no'),
(440, 37, '_downloadable', 'no'),
(441, 37, '_download_limit', '0'),
(442, 37, '_download_expiry', '0'),
(443, 37, '_stock', NULL),
(444, 37, '_stock_status', 'instock'),
(445, 37, '_wc_average_rating', '0'),
(446, 37, '_wc_review_count', '0'),
(447, 37, '_product_version', '3.7.0'),
(449, 38, '_wp_attached_file', '2019/10/vneck-tee-2.jpg'),
(450, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:800;s:4:\"file\";s:23:\"2019/10/vneck-tee-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"vneck-tee-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"vneck-tee-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"vneck-tee-2-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"vneck-tee-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"vneck-tee-2-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"vneck-tee-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"vneck-tee-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:23:\"vneck-tee-2-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"vneck-tee-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(451, 38, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vneck-tee-2.jpg'),
(452, 39, '_wp_attached_file', '2019/10/vnech-tee-green-1.jpg'),
(453, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:29:\"2019/10/vnech-tee-green-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"vnech-tee-green-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"vnech-tee-green-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"vnech-tee-green-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:29:\"vnech-tee-green-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:29:\"vnech-tee-green-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:29:\"vnech-tee-green-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:29:\"vnech-tee-green-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:29:\"vnech-tee-green-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:29:\"vnech-tee-green-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(454, 39, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vnech-tee-green-1.jpg'),
(455, 40, '_wp_attached_file', '2019/10/vnech-tee-blue-1.jpg'),
(456, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:28:\"2019/10/vnech-tee-blue-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"vnech-tee-blue-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"vnech-tee-blue-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"vnech-tee-blue-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:28:\"vnech-tee-blue-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:28:\"vnech-tee-blue-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:28:\"vnech-tee-blue-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:28:\"vnech-tee-blue-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:28:\"vnech-tee-blue-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:28:\"vnech-tee-blue-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(457, 40, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/vnech-tee-blue-1.jpg'),
(458, 12, '_wpcom_is_markdown', '1'),
(459, 12, '_wp_old_slug', 'import-placeholder-for-44'),
(460, 12, '_product_image_gallery', '39,40'),
(461, 12, '_thumbnail_id', '38'),
(462, 12, '_product_attributes', 'a:2:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"1\";s:11:\"is_taxonomy\";s:1:\"1\";}s:7:\"pa_size\";a:6:{s:4:\"name\";s:7:\"pa_size\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"1\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(463, 41, '_wp_attached_file', '2019/10/hoodie-2.jpg'),
(464, 41, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:20:\"2019/10/hoodie-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"hoodie-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"hoodie-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"hoodie-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"hoodie-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"hoodie-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"hoodie-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"hoodie-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"hoodie-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"hoodie-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(465, 41, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-2.jpg'),
(466, 42, '_wp_attached_file', '2019/10/hoodie-blue-1.jpg'),
(467, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:25:\"2019/10/hoodie-blue-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"hoodie-blue-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"hoodie-blue-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"hoodie-blue-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"hoodie-blue-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"hoodie-blue-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"hoodie-blue-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"hoodie-blue-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"hoodie-blue-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"hoodie-blue-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(468, 42, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-blue-1.jpg'),
(469, 43, '_wp_attached_file', '2019/10/hoodie-green-1.jpg'),
(470, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:26:\"2019/10/hoodie-green-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"hoodie-green-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"hoodie-green-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"hoodie-green-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"hoodie-green-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:26:\"hoodie-green-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"hoodie-green-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"hoodie-green-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:26:\"hoodie-green-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"hoodie-green-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(471, 43, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-green-1.jpg'),
(472, 44, '_wp_attached_file', '2019/10/hoodie-with-logo-2.jpg'),
(473, 44, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:30:\"2019/10/hoodie-with-logo-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-logo-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"hoodie-with-logo-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"hoodie-with-logo-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"hoodie-with-logo-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"hoodie-with-logo-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-logo-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"hoodie-with-logo-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"hoodie-with-logo-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-logo-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(474, 44, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-logo-2.jpg'),
(475, 13, '_wpcom_is_markdown', '1'),
(476, 13, '_wp_old_slug', 'import-placeholder-for-45'),
(477, 13, '_product_image_gallery', '42,43,44'),
(478, 13, '_thumbnail_id', '41'),
(479, 13, '_product_attributes', 'a:2:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"1\";s:11:\"is_taxonomy\";s:1:\"1\";}s:4:\"logo\";a:6:{s:4:\"name\";s:4:\"Logo\";s:5:\"value\";s:8:\"Yes | No\";s:8:\"position\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"1\";s:11:\"is_taxonomy\";s:1:\"0\";}}'),
(480, 14, '_wpcom_is_markdown', '1'),
(481, 14, '_wp_old_slug', 'import-placeholder-for-46'),
(482, 14, '_regular_price', '45'),
(483, 14, '_thumbnail_id', '44'),
(484, 14, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(485, 14, '_price', '45'),
(486, 45, '_wp_attached_file', '2019/10/image.jpg'),
(487, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1053;s:6:\"height\";i:1126;s:4:\"file\";s:17:\"2019/10/image.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"image-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"image-281x300.jpg\";s:5:\"width\";i:281;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"image-768x821.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:821;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"image-958x1024.jpg\";s:5:\"width\";i:958;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"image-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"image-416x445.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:445;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"image-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"image-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"image-416x445.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:445;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"image-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:33:\"(c) Beaniebeagle | Dreamstime.com\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(488, 45, '_wc_attachment_source', 'http://jasper.cmmstudent.nl/downloads/image.jpg'),
(489, 15, '_wpcom_is_markdown', '1'),
(490, 15, '_wp_old_slug', 'import-placeholder-for-47'),
(491, 15, '_regular_price', '18'),
(492, 15, '_thumbnail_id', '45'),
(493, 15, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(494, 15, '_price', '18'),
(495, 46, '_wp_attached_file', '2019/10/beanie-2.jpg'),
(496, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:20:\"2019/10/beanie-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"beanie-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"beanie-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"beanie-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"beanie-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"beanie-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"beanie-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"beanie-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"beanie-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"beanie-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(497, 46, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/beanie-2.jpg'),
(498, 16, '_wpcom_is_markdown', '1'),
(499, 16, '_wp_old_slug', 'import-placeholder-for-48'),
(500, 16, '_regular_price', '20'),
(501, 16, '_sale_price', '18'),
(502, 16, '_thumbnail_id', '46'),
(503, 16, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(504, 16, '_price', '18'),
(505, 47, '_wp_attached_file', '2019/10/belt-2.jpg'),
(506, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:18:\"2019/10/belt-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"belt-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"belt-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"belt-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"belt-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"belt-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"belt-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"belt-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:18:\"belt-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"belt-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(507, 47, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/belt-2.jpg'),
(508, 17, '_wpcom_is_markdown', '1'),
(509, 17, '_wp_old_slug', 'import-placeholder-for-58'),
(510, 17, '_regular_price', '65'),
(511, 17, '_sale_price', '55'),
(512, 17, '_thumbnail_id', '47'),
(513, 17, '_price', '55'),
(514, 48, '_wp_attached_file', '2019/10/cap-2.jpg'),
(515, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:17:\"2019/10/cap-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cap-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cap-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"cap-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cap-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"cap-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cap-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cap-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"cap-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cap-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(516, 48, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/cap-2.jpg'),
(517, 18, '_wpcom_is_markdown', '1'),
(518, 18, '_wp_old_slug', 'import-placeholder-for-60'),
(519, 18, '_regular_price', '18'),
(520, 18, '_sale_price', '16'),
(521, 18, '_thumbnail_id', '48'),
(522, 18, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(523, 18, '_price', '16'),
(524, 49, '_wp_attached_file', '2019/10/sunglasses-2.jpg'),
(525, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:24:\"2019/10/sunglasses-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"sunglasses-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"sunglasses-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"sunglasses-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"sunglasses-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:24:\"sunglasses-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"sunglasses-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:24:\"sunglasses-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:24:\"sunglasses-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"sunglasses-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(526, 49, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/sunglasses-2.jpg'),
(527, 19, '_wpcom_is_markdown', '1'),
(528, 19, '_wp_old_slug', 'import-placeholder-for-62'),
(529, 19, '_regular_price', '90'),
(530, 19, '_thumbnail_id', '49'),
(531, 19, '_price', '90'),
(532, 50, '_wp_attached_file', '2019/10/hoodie-with-pocket-2.jpg'),
(533, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:32:\"2019/10/hoodie-with-pocket-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-pocket-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(534, 50, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-pocket-2.jpg'),
(535, 20, '_wpcom_is_markdown', '1'),
(536, 20, '_wp_old_slug', 'import-placeholder-for-64'),
(537, 20, '_regular_price', '45'),
(538, 20, '_sale_price', '35'),
(539, 20, '_thumbnail_id', '50'),
(540, 20, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(541, 20, '_price', '35'),
(542, 51, '_wp_attached_file', '2019/10/hoodie-with-zipper-2.jpg'),
(543, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:32:\"2019/10/hoodie-with-zipper-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"hoodie-with-zipper-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(544, 51, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/hoodie-with-zipper-2.jpg'),
(545, 21, '_wpcom_is_markdown', '1'),
(546, 21, '_wp_old_slug', 'import-placeholder-for-66'),
(547, 21, '_regular_price', '45'),
(548, 21, '_thumbnail_id', '51'),
(549, 21, '_price', '45'),
(550, 52, '_wp_attached_file', '2019/10/long-sleeve-tee-2.jpg');
INSERT INTO `cmm_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(551, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:29:\"2019/10/long-sleeve-tee-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"long-sleeve-tee-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"long-sleeve-tee-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"long-sleeve-tee-2-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:29:\"long-sleeve-tee-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:29:\"long-sleeve-tee-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:29:\"long-sleeve-tee-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:29:\"long-sleeve-tee-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:29:\"long-sleeve-tee-2-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:29:\"long-sleeve-tee-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(552, 52, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/long-sleeve-tee-2.jpg'),
(553, 22, '_wpcom_is_markdown', '1'),
(554, 22, '_wp_old_slug', 'import-placeholder-for-68'),
(555, 22, '_regular_price', '25'),
(556, 22, '_thumbnail_id', '52'),
(557, 22, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(558, 22, '_price', '25'),
(559, 53, '_wp_attached_file', '2019/10/polo-2.jpg'),
(560, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:800;s:4:\"file\";s:18:\"2019/10/polo-2.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"polo-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"polo-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"polo-2-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"polo-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"polo-2-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"polo-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"polo-2-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:18:\"polo-2-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"polo-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(561, 53, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/polo-2.jpg'),
(562, 23, '_wpcom_is_markdown', '1'),
(563, 23, '_wp_old_slug', 'import-placeholder-for-70'),
(564, 23, '_regular_price', '20'),
(565, 23, '_thumbnail_id', '53'),
(566, 23, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(567, 23, '_price', '20'),
(568, 54, '_wp_attached_file', '2019/10/album-1.jpg'),
(569, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:19:\"2019/10/album-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"album-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"album-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"album-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"album-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:19:\"album-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"album-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"album-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:19:\"album-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"album-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(570, 54, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/album-1.jpg'),
(571, 24, '_wpcom_is_markdown', '1'),
(572, 24, '_wp_old_slug', 'import-placeholder-for-73'),
(573, 24, '_regular_price', '15'),
(574, 24, '_thumbnail_id', '54'),
(575, 24, '_downloadable_files', 'a:2:{s:36:\"259f3041-94dc-4c24-b8f6-3de00c1b0ca8\";a:3:{s:2:\"id\";s:36:\"259f3041-94dc-4c24-b8f6-3de00c1b0ca8\";s:4:\"name\";s:8:\"Single 1\";s:4:\"file\";s:85:\"https://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2017/08/single.jpg\";}s:36:\"5058a594-fa89-4da4-9234-3570ea024285\";a:3:{s:2:\"id\";s:36:\"5058a594-fa89-4da4-9234-3570ea024285\";s:4:\"name\";s:8:\"Single 2\";s:4:\"file\";s:84:\"https://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2017/08/album.jpg\";}}'),
(576, 24, '_price', '15'),
(577, 55, '_wp_attached_file', '2019/10/single-1.jpg'),
(578, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:20:\"2019/10/single-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"single-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"single-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"single-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"single-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"single-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"single-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"single-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"single-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"single-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(579, 55, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/single-1.jpg'),
(580, 25, '_wpcom_is_markdown', '1'),
(581, 25, '_wp_old_slug', 'import-placeholder-for-75'),
(582, 25, '_regular_price', '3'),
(583, 25, '_sale_price', '2'),
(584, 25, '_thumbnail_id', '55'),
(585, 25, '_downloadable_files', 'a:1:{s:36:\"502d43bc-3a20-4403-bf32-da23d4a6acf5\";a:3:{s:2:\"id\";s:36:\"502d43bc-3a20-4403-bf32-da23d4a6acf5\";s:4:\"name\";s:6:\"Single\";s:4:\"file\";s:85:\"https://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2017/08/single.jpg\";}}'),
(586, 25, '_price', '2'),
(587, 26, '_wpcom_is_markdown', ''),
(588, 26, '_wp_old_slug', 'import-placeholder-for-76'),
(589, 26, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.'),
(590, 26, '_regular_price', '20'),
(591, 26, '_thumbnail_id', '38'),
(592, 26, 'attribute_pa_color', 'red'),
(593, 26, 'attribute_pa_size', ''),
(594, 26, '_price', '20'),
(595, 27, '_wpcom_is_markdown', ''),
(596, 27, '_wp_old_slug', 'import-placeholder-for-77'),
(597, 27, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.'),
(598, 27, '_regular_price', '20'),
(599, 27, '_thumbnail_id', '39'),
(600, 27, 'attribute_pa_color', 'green'),
(601, 27, 'attribute_pa_size', ''),
(602, 27, '_price', '20'),
(603, 28, '_wpcom_is_markdown', ''),
(604, 28, '_wp_old_slug', 'import-placeholder-for-78'),
(605, 28, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.'),
(606, 28, '_regular_price', '15'),
(607, 28, '_thumbnail_id', '40'),
(608, 28, 'attribute_pa_color', 'blue'),
(609, 28, 'attribute_pa_size', ''),
(610, 28, '_price', '15'),
(611, 29, '_wpcom_is_markdown', ''),
(612, 29, '_wp_old_slug', 'import-placeholder-for-79'),
(613, 29, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.'),
(614, 29, '_regular_price', '45'),
(615, 29, '_sale_price', '42'),
(616, 29, '_thumbnail_id', '41'),
(617, 29, 'attribute_pa_color', 'red'),
(618, 29, 'attribute_logo', 'No'),
(619, 29, '_price', '42'),
(620, 30, '_wpcom_is_markdown', ''),
(621, 30, '_wp_old_slug', 'import-placeholder-for-80'),
(622, 30, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.'),
(623, 30, '_regular_price', '45'),
(624, 30, '_thumbnail_id', '43'),
(625, 30, 'attribute_pa_color', 'green'),
(626, 30, 'attribute_logo', 'No'),
(627, 30, '_price', '45'),
(628, 31, '_wpcom_is_markdown', ''),
(629, 31, '_wp_old_slug', 'import-placeholder-for-81'),
(630, 31, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.'),
(631, 31, '_regular_price', '45'),
(632, 31, '_thumbnail_id', '42'),
(633, 31, 'attribute_pa_color', 'blue'),
(634, 31, 'attribute_logo', 'No'),
(635, 31, '_price', '45'),
(636, 56, '_wp_attached_file', '2019/10/t-shirt-with-logo-1.jpg'),
(637, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:31:\"2019/10/t-shirt-with-logo-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"t-shirt-with-logo-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(638, 56, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/t-shirt-with-logo-1.jpg'),
(639, 32, '_wpcom_is_markdown', '1'),
(640, 32, '_wp_old_slug', 'import-placeholder-for-83'),
(641, 32, '_regular_price', '18'),
(642, 32, '_thumbnail_id', '56'),
(643, 32, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(644, 32, '_price', '18'),
(645, 12, '_price', '15'),
(646, 12, '_price', '20'),
(649, 57, '_wp_attached_file', '2019/10/beanie-with-logo-1.jpg'),
(650, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:30:\"2019/10/beanie-with-logo-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"beanie-with-logo-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"beanie-with-logo-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"beanie-with-logo-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"beanie-with-logo-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"beanie-with-logo-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"beanie-with-logo-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"beanie-with-logo-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"beanie-with-logo-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"beanie-with-logo-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(651, 57, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/beanie-with-logo-1.jpg'),
(652, 33, '_wpcom_is_markdown', '1'),
(653, 33, '_wp_old_slug', 'import-placeholder-for-85'),
(654, 33, '_regular_price', '20'),
(655, 33, '_sale_price', '18'),
(656, 33, '_thumbnail_id', '57'),
(657, 33, '_product_attributes', 'a:1:{s:8:\"pa_color\";a:6:{s:4:\"name\";s:8:\"pa_color\";s:5:\"value\";s:0:\"\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"0\";s:11:\"is_taxonomy\";s:1:\"1\";}}'),
(658, 33, '_price', '18'),
(659, 58, '_wp_attached_file', '2019/10/logo-1.jpg'),
(660, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:799;s:4:\"file\";s:18:\"2019/10/logo-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"logo-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"logo-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"logo-1-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"logo-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"logo-1-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"logo-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"logo-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:18:\"logo-1-416x415.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:415;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"logo-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(661, 58, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/logo-1.jpg'),
(662, 34, '_wpcom_is_markdown', '1'),
(663, 34, '_wp_old_slug', 'import-placeholder-for-87'),
(664, 34, '_children', 'a:3:{i:0;i:14;i:1;i:35;i:2;i:16;}'),
(665, 34, '_product_image_gallery', '57,56,44'),
(666, 34, '_thumbnail_id', '58'),
(667, 34, '_price', '18'),
(668, 34, '_price', '45'),
(669, 59, '_wp_attached_file', '2019/10/pennant-1.jpg'),
(670, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:21:\"2019/10/pennant-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"pennant-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"pennant-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"pennant-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"pennant-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"pennant-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"pennant-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"pennant-1-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"pennant-1-416x416.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:416;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"pennant-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(671, 59, '_wc_attachment_source', 'https://woocommercecore.mystagingwebsite.com/wp-content/uploads/2017/12/pennant-1.jpg'),
(672, 36, '_wpcom_is_markdown', '1'),
(673, 36, '_wp_old_slug', 'import-placeholder-for-89'),
(674, 36, '_regular_price', '11.05'),
(675, 36, '_thumbnail_id', '59'),
(676, 36, '_product_url', 'https://mercantile.wordpress.org/product/wordpress-pennant/'),
(677, 36, '_button_text', 'Buy on the WordPress swag store!'),
(678, 36, '_price', '11.05'),
(679, 37, '_wpcom_is_markdown', ''),
(680, 37, '_wp_old_slug', 'import-placeholder-for-90'),
(681, 37, '_variation_description', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.'),
(682, 37, '_regular_price', '45'),
(683, 37, '_thumbnail_id', '44'),
(684, 37, 'attribute_pa_color', 'blue'),
(685, 37, 'attribute_logo', 'Yes'),
(686, 37, '_price', '45'),
(687, 13, '_price', '42'),
(688, 13, '_price', '45'),
(689, 60, '_wp_attached_file', '2019/10/hero.jpg'),
(690, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:3795;s:6:\"height\";i:2355;s:4:\"file\";s:16:\"2019/10/hero.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"hero-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"hero-300x186.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:186;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"hero-768x477.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:477;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"hero-1024x635.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:635;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(691, 60, '_starter_content_theme', 'storefront'),
(694, 61, '_customize_changeset_uuid', '176a7229-28b6-4a48-a6f1-4dbb075cb093'),
(695, 62, '_wp_page_template', 'template-fullwidth.php'),
(697, 62, '_customize_changeset_uuid', '176a7229-28b6-4a48-a6f1-4dbb075cb093'),
(698, 63, '_edit_lock', '1570196522:1'),
(699, 64, '_wp_attached_file', '2019/10/image.png'),
(700, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:446;s:6:\"height\";i:200;s:4:\"file\";s:17:\"2019/10/image.png\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"image-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"image-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"image-324x200.png\";s:5:\"width\";i:324;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"image-416x187.png\";s:5:\"width\";i:416;s:6:\"height\";i:187;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"image-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"image-324x200.png\";s:5:\"width\";i:324;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"image-416x187.png\";s:5:\"width\";i:416;s:6:\"height\";i:187;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"image-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(701, 65, '_wp_attached_file', '2019/10/cropped-image.png'),
(702, 65, '_wp_attachment_context', 'custom-logo'),
(703, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:446;s:6:\"height\";i:200;s:4:\"file\";s:25:\"2019/10/cropped-image.png\";s:5:\"sizes\";a:8:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"cropped-image-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"cropped-image-300x135.png\";s:5:\"width\";i:300;s:6:\"height\";i:135;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"cropped-image-324x200.png\";s:5:\"width\";i:324;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"cropped-image-416x187.png\";s:5:\"width\";i:416;s:6:\"height\";i:187;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"cropped-image-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"cropped-image-324x200.png\";s:5:\"width\";i:324;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"cropped-image-416x187.png\";s:5:\"width\";i:416;s:6:\"height\";i:187;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"cropped-image-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(704, 63, '_wp_trash_meta_status', 'publish'),
(705, 63, '_wp_trash_meta_time', '1570196522'),
(706, 68, '_wp_attached_file', '2019/10/cropped-pennant-1.jpg'),
(707, 68, '_wp_attachment_context', 'custom-header'),
(708, 68, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:800;s:6:\"height\";i:205;s:4:\"file\";s:29:\"2019/10/cropped-pennant-1.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"cropped-pennant-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"cropped-pennant-1-300x77.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:77;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"cropped-pennant-1-768x197.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:29:\"cropped-pennant-1-324x205.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:205;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:29:\"cropped-pennant-1-416x107.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:107;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:29:\"cropped-pennant-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:29:\"cropped-pennant-1-324x205.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:205;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:29:\"cropped-pennant-1-416x107.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:107;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:29:\"cropped-pennant-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}s:17:\"attachment_parent\";i:59;}'),
(709, 68, '_wp_attachment_custom_header_last_used_storefront', '1570196650'),
(710, 68, '_wp_attachment_is_custom_header', 'storefront'),
(711, 69, '_edit_lock', '1570196646:1'),
(712, 69, '_wp_trash_meta_status', 'publish'),
(713, 69, '_wp_trash_meta_time', '1570196650'),
(714, 70, '_wp_trash_meta_status', 'publish'),
(715, 70, '_wp_trash_meta_time', '1570196695');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_posts`
--

CREATE TABLE `cmm_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_posts`
--

INSERT INTO `cmm_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-10-04 13:29:44', '2019-10-04 13:29:44', 'Welkom bij WordPress. Dit is je eerste bericht. Pas het aan of verwijder het en start met bloggen.', 'Hallo wereld.', '', 'publish', 'open', 'open', '', 'hallo-wereld', '', '', '2019-10-04 13:29:44', '2019-10-04 13:29:44', '', 0, 'https://webshop.test/?p=1', 0, 'post', '', 1),
(2, 1, '2019-10-04 13:29:44', '2019-10-04 13:29:44', 'Dit is een voorbeeld pagina. Het verschilt van een bericht omdat het zich op een vaste plaats bevindt en getoond wordt in je sitenavigatie (in de meeste thema\'s). Veel mensen starten met een Over pagina waarmee ze zich introduceren bij de site bezoekers. Het kan iets vertellen als dit: \n\n<blockquote>Hallo allemaal! Ik ben overdag postbezorger en \'s avonds houd ik me bezig met acteren. Ik woon in Amsterdam en heb een tekkel met de naam Hecktor en houd van sterke koffie</blockquote>\n\n...of iets als dit:\n\n<blockquote>De XYZ vereniging is opgericht in 1971 en levert kwaliteit aan de leden. De vereniging kent sinds de oprichting meer dan 1000 leden, waarvan vele vrijwilligers taken op zich nemen om XYZ te laten bestaan.</blockquote>\n\nAls nieuwe WordPress gebruiker, ga naar <a href=\"https://webshop.test/wp-admin/\">je dashboard</a> om deze pagina te verwijderen en maak nieuwe pagina\'s voor je site. Veel plezier!', 'Voorbeeld pagina', '', 'publish', 'closed', 'open', '', 'voorbeeld-pagina', '', '', '2019-10-04 13:29:44', '2019-10-04 13:29:44', '', 0, 'https://webshop.test/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-10-04 13:29:44', '2019-10-04 13:29:44', '<h2>Wie zijn we</h2><p>Ons website-adres is: https://webshop.test.</p><h2>Welke persoonlijke gegevens we verzamelen en waarom we die verzamelen</h2><h3>Reacties</h3><p>Als bezoekers reacties achterlaten op de site, verzamelen we de gegevens getoond in het reactieformulier, het IP-adres van de bezoeker en de browser user agent om te helpen spam te detecteren.</p><p>Een geanonimiseerde string, gemaakt op basis van je e-mailadres (dit wordt ook een hash genoemd) kan worden gestuurd naar de Gravatar service indien je dit gebruikt. De privacybeleidspagina kun je hier vinden: https://automattic.com/privacy/. Nadat je reactie is goedgekeurd, is je profielfoto publiekelijk zichtbaar in de context van je reactie.</p><h3>Media</h3><p>Als je een geregistreerde gebruiker bent en afbeeldingen naar de site upload, moet je voorkomen dat je afbeeldingen uploadt met daarin EXIF GPS locatie gegevens. Bezoekers van de website kunnen de afbeeldingen van de website downloaden en de locatiegegevens inzien.</p><h3>Contactformulieren</h3><h3>Cookies</h3><p>Wanneer je een reactie achterlaat op onze site, kun je aangeven of je naam, je e-mailadres en website in een cookie opgeslagen mogen worden. Dit doen we voor jouw gemak zodat je deze gegevens niet opnieuw hoeft in te vullen voor een nieuwe reactie. Deze cookies zijn een jaar lang geldig. </p><p>Indien je een account hebt en je logt in op deze site, slaan we een tijdelijke cookie op om te bepalen of jouw browser cookies accepteert. Deze cookie bevat geen persoonlijke gegevens en wordt verwijderd zodra je je browser sluit.</p><p>Zodra je inlogt, zullen we enkele cookies bewaren in verband met jouw login informatie en schermweergave opties. Login cookies zijn 2 dagen geldig en cookies voor schermweergave opties 1 jaar. Als je &quot;Herinner mij&quot; selecteert, wordt je login 2 weken bewaard. Zodra je uitlogt van jouw account, worden login cookies verwijderd.</p><p>Wanneer je een bericht wijzigt of publiceert wordt een aanvullende cookie door je browser opgeslagen. Deze cookie bevat geen persoonlijke data en heeft enkel het post ID van het artikel wat je hebt bewerkt in zich. Deze cookie is na een dag verlopen.</p><h3>Ingesloten inhoud van andere websites</h3><p>Berichten op deze site kunnen ingesloten (embedded) inhoud bevatten (bijvoorbeeld video\'s, afbeeldingen, berichten, etc.). Ingesloten inhoud van andere websites gedraagt zich exact hetzelfde alsof de bezoeker deze andere website heeft bezocht.</p><p>Deze websites kunnen data over jou verzamelen, cookies gebruiken, tracking van derde partijen insluiten en je interactie met deze ingesloten inhoud monitoren, inclusief de interactie met ingesloten inhoud als je een account hebt en ingelogd bent op die website.</p><h3>Analytics</h3><h2>Met wie we jouw data delen</h2><h2>Hoe lang we jouw data bewaren</h2><p>Wanneer je een reactie achterlaat dan wordt die reactie en de metadata van die reactie voor altijd bewaard. Op deze manier kunnen we vervolgreacties automatisch herkennen en goedkeuren in plaats van dat we ze moeten modereren.</p><p>Voor gebruikers die geregistreerd op onze website (indien van toepassing), bewaren we ook persoonlijke informatie in hun gebruikersprofiel. Alle gebruikers kunnen hun persoonlijke informatie bekijken, wijzigen of verwijderen op ieder moment (de gebruikersnaam kan niet gewijzigd worden). Website beheerders kunnen deze informatie ook bekijken en wijzigen.</p><h2>Welke rechten je hebt over je data</h2><p>Als je een account hebt op deze site of je hebt reacties achter gelaten, kan je verzoeken om een exportbestand van je persoonlijke gegevens die we van je hebben, inclusief alle gegevens die je ons opgegeven hebt. Je kan ook verzoeken dat we alle persoonlijke gegevens die we van je hebben verwijderen. Dit bevat geen gegevens die we verplicht moeten bewaren in verband met administratieve, wettelijke of beveiligings doeleinden.</p><h2>Waar we jouw data naartoe sturen</h2><p>Reacties van bezoekers kunnen door een geautomatiseerde spamdetectie service geleid worden.</p><h2>Jouw contactinformatie</h2><h2>Aanvullende informatie</h2><h3>Hoe we jouw data beveiligen</h3><h3>Welke datalek procedures we geïmplementeerd hebben</h3><h3>Van welke derde partijen we data ontvangen</h3><h3>Wat voor geautomatiseerde besluiten we nemen en profilering we doen met gebruikersgegevens</h3><h3>Openbaarmakingsverplichtingen van de industrie</h3>', 'Privacybeleid', '', 'draft', 'closed', 'open', '', 'privacybeleid', '', '', '2019-10-04 13:29:44', '2019-10-04 13:29:44', '', 0, 'https://webshop.test/?page_id=3', 0, 'page', '', 0),
(4, 1, '2019-10-04 13:30:01', '0000-00-00 00:00:00', '', 'Automatische concepten', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-10-04 13:30:01', '0000-00-00 00:00:00', '', 0, 'https://webshop.test/?p=4', 0, 'post', '', 0),
(5, 1, '2019-10-04 13:33:37', '2019-10-04 13:33:37', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2019-10-04 13:33:37', '2019-10-04 13:33:37', '', 0, '/content/uploads/2019/10/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2019-10-04 13:36:35', '2019-10-04 13:36:35', '', 'Winkel', '', 'publish', 'closed', 'closed', '', 'winkel', '', '', '2019-10-04 13:36:35', '2019-10-04 13:36:35', '', 0, 'https://webshop.test/?page_id=7', 0, 'page', '', 0),
(8, 1, '2019-10-04 13:36:35', '2019-10-04 13:36:35', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Winkelmand', '', 'publish', 'closed', 'closed', '', 'winkelmand', '', '', '2019-10-04 13:36:35', '2019-10-04 13:36:35', '', 0, 'https://webshop.test/?page_id=8', 0, 'page', '', 0),
(9, 1, '2019-10-04 13:36:35', '2019-10-04 13:36:35', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Afrekenen', '', 'publish', 'closed', 'closed', '', 'afrekenen', '', '', '2019-10-04 13:36:35', '2019-10-04 13:36:35', '', 0, 'https://webshop.test/?page_id=9', 0, 'page', '', 0),
(10, 1, '2019-10-04 13:36:35', '2019-10-04 13:36:35', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'Mijn account', '', 'publish', 'closed', 'closed', '', 'mijn-account', '', '', '2019-10-04 13:36:35', '2019-10-04 13:36:35', '', 0, 'https://webshop.test/?page_id=10', 0, 'page', '', 0),
(11, 1, '2019-10-04 13:38:23', '2019-10-04 13:38:23', '/content/uploads/2019/10/sample_data.csv', 'sample_data.csv', '', 'private', 'open', 'closed', '', 'sample_data-csv', '', '', '2019-10-04 13:38:23', '2019-10-04 13:38:23', '', 0, '/content/uploads/2019/10/sample_data.csv', 0, 'attachment', 'text/csv', 0),
(12, 1, '2019-10-04 13:38:30', '2019-10-04 13:38:30', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'V-Neck T-Shirt', 'This is a variable product.', 'publish', 'open', 'closed', '', 'v-neck-t-shirt', '', '', '2019-10-04 13:38:53', '2019-10-04 13:38:53', '', 0, 'https://webshop.test/?product=import-placeholder-for-44', 0, 'product', '', 0),
(13, 1, '2019-10-04 13:38:30', '2019-10-04 13:38:30', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie', 'This is a variable product.', 'publish', 'open', 'closed', '', 'hoodie', '', '', '2019-10-04 13:38:58', '2019-10-04 13:38:58', '', 0, 'https://webshop.test/?product=import-placeholder-for-45', 0, 'product', '', 0),
(14, 1, '2019-10-04 13:38:31', '2019-10-04 13:38:31', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Logo', 'This is a simple product.', 'publish', 'open', 'closed', '', 'hoodie-with-logo', '', '', '2019-10-04 13:38:40', '2019-10-04 13:38:40', '', 0, 'https://webshop.test/?product=import-placeholder-for-46', 0, 'product', '', 0),
(15, 1, '2019-10-04 13:38:31', '2019-10-04 13:38:31', 'Paaseieren kunnen op vele manieren worden versierd. Bijvoorbeeld door het ei te beschilderen of beplakken. Maar ook het perforeren van de schaal komt voor. Het versieren van eieren kan gedaan worden met behulp van een eierverfmolen.In de folklore van Overijssel worden madeliefjes tegen de eierschaal gedrukt en daarna omwikkeld met uienschillen, kranten en touw. De eieren worden gekookt, waarna de kleurstof uit de uienschillen in de eierschaal trekt. Op de plaatsen van de madeliefjes ontstaan afdrukken op de eierschaal. Na het koken wordt het ei uitgepakt en met boter ingesmeerd om het mooi te laten glanzen.', 'Paasei', 'Paasei van de week', 'publish', 'open', 'closed', '', 'paasei', '', '', '2019-10-04 13:38:41', '2019-10-04 13:38:41', '', 0, 'https://webshop.test/?product=import-placeholder-for-47', 0, 'product', '', 0),
(16, 1, '2019-10-04 13:38:31', '2019-10-04 13:38:31', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Beanie', 'This is a simple product.', 'publish', 'open', 'closed', '', 'beanie', '', '', '2019-10-04 13:38:42', '2019-10-04 13:38:42', '', 0, 'https://webshop.test/?product=import-placeholder-for-48', 0, 'product', '', 0),
(17, 1, '2019-10-04 13:38:31', '2019-10-04 13:38:31', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Belt', 'This is a simple product.', 'publish', 'open', 'closed', '', 'belt', '', '', '2019-10-04 13:38:43', '2019-10-04 13:38:43', '', 0, 'https://webshop.test/?product=import-placeholder-for-58', 0, 'product', '', 0),
(18, 1, '2019-10-04 13:38:31', '2019-10-04 13:38:31', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Cap', 'This is a simple product.', 'publish', 'open', 'closed', '', 'cap', '', '', '2019-10-04 13:38:44', '2019-10-04 13:38:44', '', 0, 'https://webshop.test/?product=import-placeholder-for-60', 0, 'product', '', 0),
(19, 1, '2019-10-04 13:38:31', '2019-10-04 13:38:31', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Sunglasses', 'This is a simple product.', 'publish', 'open', 'closed', '', 'sunglasses', '', '', '2019-10-04 13:38:45', '2019-10-04 13:38:45', '', 0, 'https://webshop.test/?product=import-placeholder-for-62', 0, 'product', '', 0),
(20, 1, '2019-10-04 13:38:31', '2019-10-04 13:38:31', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Pocket', 'This is a simple product.', 'publish', 'open', 'closed', '', 'hoodie-with-pocket', '', '', '2019-10-04 13:38:46', '2019-10-04 13:38:46', '', 0, 'https://webshop.test/?product=import-placeholder-for-64', 0, 'product', '', 0),
(21, 1, '2019-10-04 13:38:31', '2019-10-04 13:38:31', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Zipper', 'This is a simple product.', 'publish', 'open', 'closed', '', 'hoodie-with-zipper', '', '', '2019-10-04 13:38:47', '2019-10-04 13:38:47', '', 0, 'https://webshop.test/?product=import-placeholder-for-66', 0, 'product', '', 0),
(22, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Long Sleeve Tee', 'This is a simple product.', 'publish', 'open', 'closed', '', 'long-sleeve-tee', '', '', '2019-10-04 13:38:48', '2019-10-04 13:38:48', '', 0, 'https://webshop.test/?product=import-placeholder-for-68', 0, 'product', '', 0),
(23, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Polo', 'This is a simple product.', 'publish', 'open', 'closed', '', 'polo', '', '', '2019-10-04 13:38:49', '2019-10-04 13:38:49', '', 0, 'https://webshop.test/?product=import-placeholder-for-70', 0, 'product', '', 0),
(24, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.', 'Album', 'This is a simple, virtual product.', 'publish', 'open', 'closed', '', 'album', '', '', '2019-10-04 13:38:50', '2019-10-04 13:38:50', '', 0, 'https://webshop.test/?product=import-placeholder-for-73', 0, 'product', '', 0),
(25, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sagittis orci ac odio dictum tincidunt. Donec ut metus leo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed luctus, dui eu sagittis sodales, nulla nibh sagittis augue, vel porttitor diam enim non metus. Vestibulum aliquam augue neque. Phasellus tincidunt odio eget ullamcorper efficitur. Cras placerat ut turpis pellentesque vulputate. Nam sed consequat tortor. Curabitur finibus sapien dolor. Ut eleifend tellus nec erat pulvinar dignissim. Nam non arcu purus. Vivamus et massa massa.', 'Single', 'This is a simple, virtual product.', 'publish', 'open', 'closed', '', 'single', '', '', '2019-10-04 13:38:51', '2019-10-04 13:38:51', '', 0, 'https://webshop.test/?product=import-placeholder-for-75', 0, 'product', '', 0),
(26, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', '', 'V-Neck T-Shirt - Red', 'Color: Red', 'publish', 'closed', 'closed', '', 'v-neck-t-shirt-red', '', '', '2019-10-04 13:38:51', '2019-10-04 13:38:51', '', 12, 'https://webshop.test/?product=import-placeholder-for-76', 0, 'product_variation', '', 0),
(27, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', '', 'V-Neck T-Shirt - Green', 'Color: Green', 'publish', 'closed', 'closed', '', 'v-neck-t-shirt-green', '', '', '2019-10-04 13:38:51', '2019-10-04 13:38:51', '', 12, 'https://webshop.test/?product=import-placeholder-for-77', 0, 'product_variation', '', 0),
(28, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', '', 'V-Neck T-Shirt - Blue', 'Color: Blue', 'publish', 'closed', 'closed', '', 'v-neck-t-shirt-blue', '', '', '2019-10-04 13:38:52', '2019-10-04 13:38:52', '', 12, 'https://webshop.test/?product=import-placeholder-for-78', 0, 'product_variation', '', 0),
(29, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', '', 'Hoodie - Red, No', 'Color: Red, Logo: No', 'publish', 'closed', 'closed', '', 'hoodie-red-no', '', '', '2019-10-04 13:38:52', '2019-10-04 13:38:52', '', 13, 'https://webshop.test/?product=import-placeholder-for-79', 1, 'product_variation', '', 0),
(30, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', '', 'Hoodie - Green, No', 'Color: Green, Logo: No', 'publish', 'closed', 'closed', '', 'hoodie-green-no', '', '', '2019-10-04 13:38:52', '2019-10-04 13:38:52', '', 13, 'https://webshop.test/?product=import-placeholder-for-80', 2, 'product_variation', '', 0),
(31, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', '', 'Hoodie - Blue, No', 'Color: Blue, Logo: No', 'publish', 'closed', 'closed', '', 'hoodie-blue-no', '', '', '2019-10-04 13:38:52', '2019-10-04 13:38:52', '', 13, 'https://webshop.test/?product=import-placeholder-for-81', 3, 'product_variation', '', 0),
(32, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'T-Shirt with Logo', 'This is a simple product.', 'publish', 'open', 'closed', '', 't-shirt-with-logo', '', '', '2019-10-04 13:38:53', '2019-10-04 13:38:53', '', 0, 'https://webshop.test/?product=import-placeholder-for-83', 0, 'product', '', 0),
(33, 1, '2019-10-04 13:38:32', '2019-10-04 13:38:32', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Beanie with Logo', 'This is a simple product.', 'publish', 'open', 'closed', '', 'beanie-with-logo', '', '', '2019-10-04 13:38:55', '2019-10-04 13:38:55', '', 0, 'https://webshop.test/?product=import-placeholder-for-85', 0, 'product', '', 0),
(34, 1, '2019-10-04 13:38:33', '2019-10-04 13:38:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Logo Collection', 'This is a grouped product.', 'publish', 'open', 'closed', '', 'logo-collection', '', '', '2019-10-04 13:38:57', '2019-10-04 13:38:57', '', 0, 'https://webshop.test/?product=import-placeholder-for-87', 0, 'product', '', 0),
(36, 1, '2019-10-04 13:38:33', '2019-10-04 13:38:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'WordPress Pennant', 'This is an external product.', 'publish', 'open', 'closed', '', 'wordpress-pennant', '', '', '2019-10-04 13:38:58', '2019-10-04 13:38:58', '', 0, 'https://webshop.test/?product=import-placeholder-for-89', 0, 'product', '', 0),
(37, 1, '2019-10-04 13:38:33', '2019-10-04 13:38:33', '', 'Hoodie - Blue, Yes', 'Color: Blue, Logo: Yes', 'publish', 'closed', 'closed', '', 'hoodie-blue-yes', '', '', '2019-10-04 13:38:58', '2019-10-04 13:38:58', '', 13, 'https://webshop.test/?product=import-placeholder-for-90', 0, 'product_variation', '', 0),
(38, 1, '2019-10-04 13:38:34', '2019-10-04 13:38:34', '', 'vneck-tee-2.jpg', '', 'inherit', 'open', 'closed', '', 'vneck-tee-2-jpg', '', '', '2019-10-04 13:38:34', '2019-10-04 13:38:34', '', 12, '/content/uploads/2019/10/vneck-tee-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2019-10-04 13:38:35', '2019-10-04 13:38:35', '', 'vnech-tee-green-1.jpg', '', 'inherit', 'open', 'closed', '', 'vnech-tee-green-1-jpg', '', '', '2019-10-04 13:38:35', '2019-10-04 13:38:35', '', 12, '/content/uploads/2019/10/vnech-tee-green-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(40, 1, '2019-10-04 13:38:35', '2019-10-04 13:38:35', '', 'vnech-tee-blue-1.jpg', '', 'inherit', 'open', 'closed', '', 'vnech-tee-blue-1-jpg', '', '', '2019-10-04 13:38:35', '2019-10-04 13:38:35', '', 12, '/content/uploads/2019/10/vnech-tee-blue-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2019-10-04 13:38:37', '2019-10-04 13:38:37', '', 'hoodie-2.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-2-jpg', '', '', '2019-10-04 13:38:37', '2019-10-04 13:38:37', '', 13, '/content/uploads/2019/10/hoodie-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(42, 1, '2019-10-04 13:38:38', '2019-10-04 13:38:38', '', 'hoodie-blue-1.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-blue-1-jpg', '', '', '2019-10-04 13:38:38', '2019-10-04 13:38:38', '', 13, '/content/uploads/2019/10/hoodie-blue-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(43, 1, '2019-10-04 13:38:39', '2019-10-04 13:38:39', '', 'hoodie-green-1.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-green-1-jpg', '', '', '2019-10-04 13:38:39', '2019-10-04 13:38:39', '', 13, '/content/uploads/2019/10/hoodie-green-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(44, 1, '2019-10-04 13:38:39', '2019-10-04 13:38:39', '', 'hoodie-with-logo-2.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-with-logo-2-jpg', '', '', '2019-10-04 13:38:39', '2019-10-04 13:38:39', '', 13, '/content/uploads/2019/10/hoodie-with-logo-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(45, 1, '2019-10-04 13:38:41', '2019-10-04 13:38:41', '', 'image.jpg', '', 'inherit', 'open', 'closed', '', 'image-jpg', '', '', '2019-10-04 13:38:41', '2019-10-04 13:38:41', '', 15, '/content/uploads/2019/10/image.jpg', 0, 'attachment', 'image/jpeg', 0),
(46, 1, '2019-10-04 13:38:42', '2019-10-04 13:38:42', '', 'beanie-2.jpg', '', 'inherit', 'open', 'closed', '', 'beanie-2-jpg', '', '', '2019-10-04 13:38:42', '2019-10-04 13:38:42', '', 16, '/content/uploads/2019/10/beanie-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(47, 1, '2019-10-04 13:38:43', '2019-10-04 13:38:43', '', 'belt-2.jpg', '', 'inherit', 'open', 'closed', '', 'belt-2-jpg', '', '', '2019-10-04 13:38:43', '2019-10-04 13:38:43', '', 17, '/content/uploads/2019/10/belt-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(48, 1, '2019-10-04 13:38:44', '2019-10-04 13:38:44', '', 'cap-2.jpg', '', 'inherit', 'open', 'closed', '', 'cap-2-jpg', '', '', '2019-10-04 13:38:44', '2019-10-04 13:38:44', '', 18, '/content/uploads/2019/10/cap-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(49, 1, '2019-10-04 13:38:45', '2019-10-04 13:38:45', '', 'sunglasses-2.jpg', '', 'inherit', 'open', 'closed', '', 'sunglasses-2-jpg', '', '', '2019-10-04 13:38:45', '2019-10-04 13:38:45', '', 19, '/content/uploads/2019/10/sunglasses-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(50, 1, '2019-10-04 13:38:46', '2019-10-04 13:38:46', '', 'hoodie-with-pocket-2.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-with-pocket-2-jpg', '', '', '2019-10-04 13:38:46', '2019-10-04 13:38:46', '', 20, '/content/uploads/2019/10/hoodie-with-pocket-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(51, 1, '2019-10-04 13:38:47', '2019-10-04 13:38:47', '', 'hoodie-with-zipper-2.jpg', '', 'inherit', 'open', 'closed', '', 'hoodie-with-zipper-2-jpg', '', '', '2019-10-04 13:38:47', '2019-10-04 13:38:47', '', 21, '/content/uploads/2019/10/hoodie-with-zipper-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(52, 1, '2019-10-04 13:38:48', '2019-10-04 13:38:48', '', 'long-sleeve-tee-2.jpg', '', 'inherit', 'open', 'closed', '', 'long-sleeve-tee-2-jpg', '', '', '2019-10-04 13:38:48', '2019-10-04 13:38:48', '', 22, '/content/uploads/2019/10/long-sleeve-tee-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(53, 1, '2019-10-04 13:38:49', '2019-10-04 13:38:49', '', 'polo-2.jpg', '', 'inherit', 'open', 'closed', '', 'polo-2-jpg', '', '', '2019-10-04 13:38:49', '2019-10-04 13:38:49', '', 23, '/content/uploads/2019/10/polo-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(54, 1, '2019-10-04 13:38:50', '2019-10-04 13:38:50', '', 'album-1.jpg', '', 'inherit', 'open', 'closed', '', 'album-1-jpg', '', '', '2019-10-04 13:38:50', '2019-10-04 13:38:50', '', 24, '/content/uploads/2019/10/album-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(55, 1, '2019-10-04 13:38:51', '2019-10-04 13:38:51', '', 'single-1.jpg', '', 'inherit', 'open', 'closed', '', 'single-1-jpg', '', '', '2019-10-04 13:38:51', '2019-10-04 13:38:51', '', 25, '/content/uploads/2019/10/single-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(56, 1, '2019-10-04 13:38:53', '2019-10-04 13:38:53', '', 't-shirt-with-logo-1.jpg', '', 'inherit', 'open', 'closed', '', 't-shirt-with-logo-1-jpg', '', '', '2019-10-04 13:38:53', '2019-10-04 13:38:53', '', 32, '/content/uploads/2019/10/t-shirt-with-logo-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(57, 1, '2019-10-04 13:38:55', '2019-10-04 13:38:55', '', 'beanie-with-logo-1.jpg', '', 'inherit', 'open', 'closed', '', 'beanie-with-logo-1-jpg', '', '', '2019-10-04 13:38:55', '2019-10-04 13:38:55', '', 33, '/content/uploads/2019/10/beanie-with-logo-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2019-10-04 13:38:56', '2019-10-04 13:38:56', '', 'logo-1.jpg', '', 'inherit', 'open', 'closed', '', 'logo-1-jpg', '', '', '2019-10-04 13:38:56', '2019-10-04 13:38:56', '', 34, '/content/uploads/2019/10/logo-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(59, 1, '2019-10-04 13:38:58', '2019-10-04 13:38:58', '', 'pennant-1.jpg', '', 'inherit', 'open', 'closed', '', 'pennant-1-jpg', '', '', '2019-10-04 13:38:58', '2019-10-04 13:38:58', '', 36, '/content/uploads/2019/10/pennant-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(60, 1, '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 'Hero', '', 'inherit', 'open', 'closed', '', 'hero-image', '', '', '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 0, '/content/uploads/2019/10/hero.jpg', 0, 'attachment', 'image/jpeg', 0),
(61, 1, '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 0, 'https://webshop.test/?page_id=61', 0, 'page', '', 0),
(62, 1, '2019-10-04 13:42:02', '2019-10-04 13:42:02', 'Welkom op je site! Dit is je homepage, dat is wat de meeste bezoekers zullen zien als ze voor het eerst op je site komen.', 'Homepagina', '', 'publish', 'closed', 'closed', '', 'homepagina', '', '', '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 0, 'https://webshop.test/?page_id=62', 0, 'page', '', 0),
(63, 1, '2019-10-04 13:42:02', '2019-10-04 13:42:02', '{\n    \"nav_menus_created_posts\": {\n        \"value\": [\n            60,\n            61,\n            62\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:42:02\"\n    },\n    \"show_on_front\": {\n        \"starter_content\": true,\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:41:00\"\n    },\n    \"page_on_front\": {\n        \"starter_content\": true,\n        \"value\": 62,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:41:00\"\n    },\n    \"page_for_posts\": {\n        \"starter_content\": true,\n        \"value\": 61,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:41:00\"\n    },\n    \"blogname\": {\n        \"value\": \"CMM test weshop\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:41:32\"\n    },\n    \"blogdescription\": {\n        \"value\": \"Dit is mijn webshop\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:41:32\"\n    },\n    \"storefront::custom_logo\": {\n        \"value\": 65,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:42:02\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '176a7229-28b6-4a48-a6f1-4dbb075cb093', '', '', '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 0, 'https://webshop.test/?p=63', 0, 'customize_changeset', '', 0),
(64, 1, '2019-10-04 13:41:51', '2019-10-04 13:41:51', '', 'image', '', 'inherit', 'open', 'closed', '', 'image', '', '', '2019-10-04 13:41:51', '2019-10-04 13:41:51', '', 0, '/content/uploads/2019/10/image.png', 0, 'attachment', 'image/png', 0),
(65, 1, '2019-10-04 13:41:59', '2019-10-04 13:41:59', '/content/uploads/2019/10/cropped-image.png', 'cropped-image.png', '', 'inherit', 'open', 'closed', '', 'cropped-image-png', '', '', '2019-10-04 13:41:59', '2019-10-04 13:41:59', '', 0, '/content/uploads/2019/10/cropped-image.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 61, 'https://webshop.test/61-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2019-10-04 13:42:02', '2019-10-04 13:42:02', 'Welkom op je site! Dit is je homepage, dat is wat de meeste bezoekers zullen zien als ze voor het eerst op je site komen.', 'Homepagina', '', 'inherit', 'closed', 'closed', '', '62-revision-v1', '', '', '2019-10-04 13:42:02', '2019-10-04 13:42:02', '', 62, 'https://webshop.test/62-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2019-10-04 13:43:56', '2019-10-04 13:43:56', '', 'cropped-pennant-1.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-pennant-1-jpg', '', '', '2019-10-04 13:43:56', '2019-10-04 13:43:56', '', 0, '/content/uploads/2019/10/cropped-pennant-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(69, 1, '2019-10-04 13:44:09', '2019-10-04 13:44:09', '{\n    \"storefront::header_image\": {\n        \"value\": \"/content/uploads/2019/10/cropped-pennant-1.jpg\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:44:06\"\n    },\n    \"storefront::header_image_data\": {\n        \"value\": {\n            \"url\": \"/content/uploads/2019/10/cropped-pennant-1.jpg\",\n            \"thumbnail_url\": \"/content/uploads/2019/10/cropped-pennant-1.jpg\",\n            \"timestamp\": 1570196636257,\n            \"attachment_id\": 68,\n            \"width\": 800,\n            \"height\": 205\n        },\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:44:06\"\n    },\n    \"storefront::storefront_header_background_color\": {\n        \"value\": \"#e8e8e8\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:44:06\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '5ce0b176-7181-4baa-a946-a326771b4601', '', '', '2019-10-04 13:44:09', '2019-10-04 13:44:09', '', 0, 'https://webshop.test/?p=69', 0, 'customize_changeset', '', 0),
(70, 1, '2019-10-04 13:44:55', '2019-10-04 13:44:55', '{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-10-04 13:44:55\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '54882ae1-d553-4e41-9640-8164080d3e43', '', '', '2019-10-04 13:44:55', '2019-10-04 13:44:55', '', 0, 'https://webshop.test/54882ae1-d553-4e41-9640-8164080d3e43/', 0, 'customize_changeset', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_termmeta`
--

CREATE TABLE `cmm_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_termmeta`
--

INSERT INTO `cmm_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 15, 'product_count_product_cat', '0'),
(2, 17, 'order', '0'),
(3, 18, 'order', '0'),
(4, 19, 'order', '0'),
(5, 20, 'order', '0'),
(6, 21, 'order', '0'),
(7, 22, 'order', '0'),
(8, 23, 'order', '0'),
(9, 18, 'product_count_product_cat', '4'),
(10, 17, 'product_count_product_cat', '14'),
(11, 24, 'order_pa_color', '0'),
(12, 25, 'order_pa_color', '0'),
(13, 26, 'order_pa_color', '0'),
(14, 27, 'order_pa_size', '0'),
(15, 28, 'order_pa_size', '0'),
(16, 29, 'order_pa_size', '0'),
(17, 19, 'product_count_product_cat', '3'),
(18, 20, 'product_count_product_cat', '1'),
(19, 30, 'order_pa_color', '0'),
(20, 21, 'product_count_product_cat', '5'),
(21, 31, 'order_pa_color', '0'),
(22, 32, 'order_pa_color', '0'),
(23, 22, 'product_count_product_cat', '2'),
(24, 23, 'product_count_product_cat', '1');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_terms`
--

CREATE TABLE `cmm_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_terms`
--

INSERT INTO `cmm_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Geen categorie', 'geen-categorie', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Geen categorie', 'geen-categorie', 0),
(16, 'wc-admin-notes', 'wc-admin-notes', 0),
(17, 'Clothing', 'clothing', 0),
(18, 'Tshirts', 'tshirts', 0),
(19, 'Hoodies', 'hoodies', 0),
(20, 'Paasei', 'paasei', 0),
(21, 'Accessories', 'accessories', 0),
(22, 'Music', 'music', 0),
(23, 'Decor', 'decor', 0),
(24, 'Blue', 'blue', 0),
(25, 'Green', 'green', 0),
(26, 'Red', 'red', 0),
(27, 'Large', 'large', 0),
(28, 'Medium', 'medium', 0),
(29, 'Small', 'small', 0),
(30, 'Brown', 'brown', 0),
(31, 'Yellow', 'yellow', 0),
(32, 'Gray', 'gray', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_term_relationships`
--

CREATE TABLE `cmm_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_term_relationships`
--

INSERT INTO `cmm_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(12, 4, 0),
(12, 8, 0),
(12, 18, 0),
(12, 24, 0),
(12, 25, 0),
(12, 26, 0),
(12, 27, 0),
(12, 28, 0),
(12, 29, 0),
(13, 4, 0),
(13, 19, 0),
(13, 24, 0),
(13, 25, 0),
(13, 26, 0),
(14, 2, 0),
(14, 19, 0),
(14, 24, 0),
(15, 2, 0),
(15, 20, 0),
(15, 30, 0),
(16, 2, 0),
(16, 21, 0),
(16, 26, 0),
(17, 2, 0),
(17, 21, 0),
(18, 2, 0),
(18, 8, 0),
(18, 21, 0),
(18, 31, 0),
(19, 2, 0),
(19, 8, 0),
(19, 21, 0),
(20, 2, 0),
(20, 6, 0),
(20, 7, 0),
(20, 8, 0),
(20, 19, 0),
(20, 32, 0),
(21, 2, 0),
(21, 8, 0),
(21, 19, 0),
(22, 2, 0),
(22, 18, 0),
(22, 25, 0),
(23, 2, 0),
(23, 18, 0),
(23, 24, 0),
(24, 2, 0),
(24, 22, 0),
(25, 2, 0),
(25, 22, 0),
(26, 15, 0),
(27, 15, 0),
(28, 15, 0),
(29, 15, 0),
(30, 15, 0),
(31, 15, 0),
(32, 2, 0),
(32, 18, 0),
(32, 32, 0),
(33, 2, 0),
(33, 21, 0),
(33, 26, 0),
(34, 3, 0),
(34, 17, 0),
(36, 5, 0),
(36, 23, 0),
(37, 15, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_term_taxonomy`
--

CREATE TABLE `cmm_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_term_taxonomy`
--

INSERT INTO `cmm_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 14),
(3, 3, 'product_type', '', 0, 1),
(4, 4, 'product_type', '', 0, 2),
(5, 5, 'product_type', '', 0, 1),
(6, 6, 'product_visibility', '', 0, 1),
(7, 7, 'product_visibility', '', 0, 1),
(8, 8, 'product_visibility', '', 0, 5),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'action-group', '', 0, 0),
(17, 17, 'product_cat', '', 0, 1),
(18, 18, 'product_cat', '', 17, 4),
(19, 19, 'product_cat', '', 17, 4),
(20, 20, 'product_cat', '', 17, 1),
(21, 21, 'product_cat', '', 17, 5),
(22, 22, 'product_cat', '', 0, 2),
(23, 23, 'product_cat', '', 0, 1),
(24, 24, 'pa_color', '', 0, 4),
(25, 25, 'pa_color', '', 0, 3),
(26, 26, 'pa_color', '', 0, 4),
(27, 27, 'pa_size', '', 0, 1),
(28, 28, 'pa_size', '', 0, 1),
(29, 29, 'pa_size', '', 0, 1),
(30, 30, 'pa_color', '', 0, 1),
(31, 31, 'pa_color', '', 0, 1),
(32, 32, 'pa_color', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_usermeta`
--

CREATE TABLE `cmm_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_usermeta`
--

INSERT INTO `cmm_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'jasper'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'cmm_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'cmm_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'cmm_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'itsec_user_activity_last_seen', '1585146913'),
(20, 1, '_woocommerce_tracks_anon_id', 'woo:Mv/9N9oq+qp7QHQy2zsmcC7V'),
(21, 1, 'cmm_yoast_notifications', 'a:1:{i:0;a:2:{s:7:\"message\";s:456:\"Yoast SEO en WooCommerce werken veel beter samen als er een extra, ondersteunende plugin wordt gebruikt. Installeer Yoast WooCommerce SEO, daar wordt je leven makkelijker van. <a href=\"https://yoa.st/1o0?php_version=7.4&platform=wordpress&platform_version=5.3.2&software=free&software_version=13.3&days_active=30plus&user_language=nl_NL\" aria-label=\"Meer informatie over Yoast WooCommerce SEO\" target=\"_blank\" rel=\"noopener noreferrer\">Meer informatie</a>.\";s:7:\"options\";a:10:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:44:\"wpseo-suggested-plugin-yoast-woocommerce-seo\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"1\";s:10:\"user_login\";s:6:\"jasper\";s:9:\"user_pass\";s:34:\"$P$BgOwEeJ0an.LIKFExLUVSkGDERXE8i.\";s:13:\"user_nicename\";s:6:\"jasper\";s:10:\"user_email\";s:13:\"jasper@cmm.nl\";s:8:\"user_url\";s:0:\"\";s:15:\"user_registered\";s:19:\"2019-10-04 13:29:44\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:6:\"jasper\";}s:2:\"ID\";i:1;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:16:\"cmm_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:116:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:20:\"wpseo_manage_options\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";a:1:{i:0;s:15:\"install_plugins\";}s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}'),
(22, 1, '_yoast_wpseo_profile_updated', '1570196236'),
(23, 1, 'cmm_woocommerce_product_import_mapping', 'a:51:{i:0;s:2:\"id\";i:1;s:4:\"type\";i:2;s:3:\"sku\";i:3;s:4:\"name\";i:4;s:9:\"published\";i:5;s:8:\"featured\";i:6;s:18:\"catalog_visibility\";i:7;s:17:\"short_description\";i:8;s:11:\"description\";i:9;s:17:\"date_on_sale_from\";i:10;s:15:\"date_on_sale_to\";i:11;s:10:\"tax_status\";i:12;s:9:\"tax_class\";i:13;s:12:\"stock_status\";i:14;s:14:\"stock_quantity\";i:15;s:10:\"backorders\";i:16;s:17:\"sold_individually\";i:17;s:0:\"\";i:18;s:0:\"\";i:19;s:0:\"\";i:20;s:0:\"\";i:21;s:15:\"reviews_allowed\";i:22;s:13:\"purchase_note\";i:23;s:10:\"sale_price\";i:24;s:13:\"regular_price\";i:25;s:12:\"category_ids\";i:26;s:7:\"tag_ids\";i:27;s:17:\"shipping_class_id\";i:28;s:6:\"images\";i:29;s:14:\"download_limit\";i:30;s:15:\"download_expiry\";i:31;s:9:\"parent_id\";i:32;s:16:\"grouped_products\";i:33;s:10:\"upsell_ids\";i:34;s:14:\"cross_sell_ids\";i:35;s:11:\"product_url\";i:36;s:11:\"button_text\";i:37;s:10:\"menu_order\";i:38;s:16:\"attributes:name1\";i:39;s:17:\"attributes:value1\";i:40;s:19:\"attributes:visible1\";i:41;s:20:\"attributes:taxonomy1\";i:42;s:16:\"attributes:name2\";i:43;s:17:\"attributes:value2\";i:44;s:19:\"attributes:visible2\";i:45;s:20:\"attributes:taxonomy2\";i:46;s:23:\"meta:_wpcom_is_markdown\";i:47;s:15:\"downloads:name1\";i:48;s:14:\"downloads:url1\";i:49;s:15:\"downloads:name2\";i:50;s:14:\"downloads:url2\";}'),
(24, 1, 'cmm_product_import_error_log', 'a:0:{}'),
(25, 1, 'wc_last_active', '1585094400'),
(26, 1, 'itsec-settings-view', 'grid'),
(27, 1, 'cmm_wpseo-suggested-plugin-yoast-woocommerce-seo', 'seen'),
(28, 1, 'itsec-password-strength', '1'),
(30, 1, '_itsec_password_requirements', 'a:1:{s:16:\"evaluation_times\";a:1:{s:8:\"strength\";i:1585146868;}}'),
(32, 1, 'last_update', '1585146909'),
(33, 1, 'itsec_last_password_change', '1585146908'),
(34, 1, 'session_tokens', 'a:1:{s:64:\"98dbb81dacfb6d8f5196520fd31e24b25a10a14e7516a09cdb4a49b27c59c4f0\";a:4:{s:10:\"expiration\";i:1585319709;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:119:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.5 Safari/605.1.15\";s:5:\"login\";i:1585146909;}}'),
(35, 1, '_order_count', '0'),
(37, 1, '_itsec_has_logged_in', '1585146908');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_users`
--

CREATE TABLE `cmm_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_users`
--

INSERT INTO `cmm_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'jasper', '$P$BgOwEeJ0an.LIKFExLUVSkGDERXE8i.', 'jasper', 'jasper@cmm.nl', '', '2019-10-04 13:29:44', '', 0, 'jasper');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_admin_notes`
--

CREATE TABLE `cmm_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `icon` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_wc_admin_notes`
--

INSERT INTO `cmm_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `icon`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`) VALUES
(1, 'wc-admin-welcome-note', 'info', 'en_US', 'New feature(s)', 'Welcome to the new WooCommerce experience! In this new release you\'ll be able to have a glimpse of how your store is doing in the Dashboard, manage important aspects of your business (such as managing orders, stock, reviews) from anywhere in the interface, dive into your store data with a completely new Analytics section and more!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2019-10-04 13:34:05', NULL, 0),
(2, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2019-10-04 13:34:05', NULL, 0),
(4, 'wc-admin-store-notice-setting-moved', 'info', 'en_US', 'Looking for the Store Notice setting?', 'It can now be found in the Customizer.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-03-25 15:00:34', NULL, 0),
(5, 'wc-update-db-reminder', 'update', 'en_US', 'WooCommerce database update in progress', 'WooCommerce werkt de database op de achtergrond bij. Het updateproces van de database kan even duren. Even geduld.', 'info', '{}', 'unactioned', 'woocommerce-core', '2020-03-25 15:00:34', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_admin_note_actions`
--

CREATE TABLE `cmm_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_wc_admin_note_actions`
--

INSERT INTO `cmm_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`) VALUES
(1, 1, 'learn-more', 'Learn more', 'https://woocommerce.wordpress.com/', 'actioned', 0),
(2, 2, 'connect', 'Connect', '?page=wc-addons&section=helper', 'actioned', 0),
(4, 4, 'open-customizer', 'Open Customizer', 'customize.php', 'actioned', 0),
(9, 5, 'update-db_see-progress', 'View progress →', 'https://webshop.test/wp-admin/admin.php?page=wc-status&tab=action-scheduler&s=woocommerce_run_update&status=pending', 'unactioned', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_category_lookup`
--

CREATE TABLE `cmm_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_customer_lookup`
--

CREATE TABLE `cmm_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_download_log`
--

CREATE TABLE `cmm_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_order_coupon_lookup`
--

CREATE TABLE `cmm_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_order_product_lookup`
--

CREATE TABLE `cmm_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_order_stats`
--

CREATE TABLE `cmm_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `gross_total` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `total_sales` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_order_tax_lookup`
--

CREATE TABLE `cmm_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_product_meta_lookup`
--

CREATE TABLE `cmm_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_wc_product_meta_lookup`
--

INSERT INTO `cmm_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`, `tax_status`, `tax_class`) VALUES
(12, 'woo-vneck-tee', 0, 0, '15.0000', '20.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(13, 'woo-hoodie', 0, 0, '42.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(14, 'woo-hoodie-with-logo', 0, 0, '45.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(15, 'woo-paasei', 0, 0, '18.0000', '18.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(16, 'woo-beanie', 0, 0, '18.0000', '18.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(17, 'woo-belt', 0, 0, '55.0000', '55.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(18, 'woo-cap', 0, 0, '16.0000', '16.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(19, 'woo-sunglasses', 0, 0, '90.0000', '90.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(20, 'woo-hoodie-with-pocket', 0, 0, '35.0000', '35.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(21, 'woo-hoodie-with-zipper', 0, 0, '45.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(22, 'woo-long-sleeve-tee', 0, 0, '25.0000', '25.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(23, 'woo-polo', 0, 0, '20.0000', '20.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(24, 'woo-album', 1, 1, '15.0000', '15.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(25, 'woo-single', 1, 1, '2.0000', '2.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(26, 'woo-vneck-tee-red', 0, 0, '20.0000', '20.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(27, 'woo-vneck-tee-green', 0, 0, '20.0000', '20.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(28, 'woo-vneck-tee-blue', 0, 0, '15.0000', '15.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(29, 'woo-hoodie-red', 0, 0, '42.0000', '42.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(30, 'woo-hoodie-green', 0, 0, '45.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(31, 'woo-hoodie-blue', 0, 0, '45.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(32, 'Woo-tshirt-logo', 0, 0, '18.0000', '18.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(33, 'Woo-beanie-logo', 0, 0, '18.0000', '18.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(34, 'logo-collection', 0, 0, '18.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(35, 'woo-tshirt', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(36, 'wp-pennant', 0, 0, '11.0500', '11.0500', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(37, 'woo-hoodie-blue-logo', 0, 0, '45.0000', '45.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', '');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_tax_rate_classes`
--

CREATE TABLE `cmm_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_wc_tax_rate_classes`
--

INSERT INTO `cmm_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Gereduceerd tarief', 'gereduceerd-tarief'),
(2, 'Nultarief', 'nultarief');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_wc_webhooks`
--

CREATE TABLE `cmm_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_api_keys`
--

CREATE TABLE `cmm_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_attribute_taxonomies`
--

CREATE TABLE `cmm_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_woocommerce_attribute_taxonomies`
--

INSERT INTO `cmm_woocommerce_attribute_taxonomies` (`attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'color', 'Color', 'select', 'menu_order', 0),
(2, 'size', 'Size', 'select', 'menu_order', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `cmm_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_log`
--

CREATE TABLE `cmm_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_order_itemmeta`
--

CREATE TABLE `cmm_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_order_items`
--

CREATE TABLE `cmm_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_payment_tokenmeta`
--

CREATE TABLE `cmm_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_payment_tokens`
--

CREATE TABLE `cmm_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_sessions`
--

CREATE TABLE `cmm_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_woocommerce_sessions`
--

INSERT INTO `cmm_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(1, '1', 'a:7:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:729:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2020-03-25T14:35:09+00:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:1:\"*\";s:7:\"country\";s:2:\"NL\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:1:\"*\";s:16:\"shipping_country\";s:2:\"NL\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:13:\"jasper@cmm.nl\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1585319716);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_shipping_zones`
--

CREATE TABLE `cmm_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_woocommerce_shipping_zones`
--

INSERT INTO `cmm_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(1, 'Nederland', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_shipping_zone_locations`
--

CREATE TABLE `cmm_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_woocommerce_shipping_zone_locations`
--

INSERT INTO `cmm_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(1, 1, 'NL', 'country');

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_shipping_zone_methods`
--

CREATE TABLE `cmm_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_woocommerce_shipping_zone_methods`
--

INSERT INTO `cmm_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(1, 1, 'flat_rate', 1, 1),
(0, 2, 'flat_rate', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_tax_rates`
--

CREATE TABLE `cmm_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_woocommerce_tax_rate_locations`
--

CREATE TABLE `cmm_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_yoast_seo_links`
--

CREATE TABLE `cmm_yoast_seo_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `target_post_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cmm_yoast_seo_meta`
--

CREATE TABLE `cmm_yoast_seo_meta` (
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `internal_link_count` int(10) UNSIGNED DEFAULT NULL,
  `incoming_link_count` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `cmm_yoast_seo_meta`
--

INSERT INTO `cmm_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(6, 0, 0),
(7, 0, 0),
(8, 0, 0),
(9, 0, 0),
(10, 0, 0),
(12, 0, 0),
(13, 0, 0),
(14, 0, 0),
(15, 0, 0),
(16, 0, 0),
(17, 0, 0),
(18, 0, 0),
(19, 0, 0),
(20, 0, 0),
(21, 0, 0),
(22, 0, 0),
(23, 0, 0),
(24, 0, 0),
(25, 0, 0),
(26, 0, 0),
(27, 0, 0),
(28, 0, 0),
(29, 0, 0),
(30, 0, 0),
(31, 0, 0),
(32, 0, 0),
(33, 0, 0),
(34, 0, 0),
(35, 0, 0),
(36, 0, 0),
(37, 0, 0),
(61, 0, 0),
(62, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cmm_actionscheduler_actions`
--
ALTER TABLE `cmm_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `cmm_actionscheduler_claims`
--
ALTER TABLE `cmm_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `cmm_actionscheduler_groups`
--
ALTER TABLE `cmm_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `cmm_actionscheduler_logs`
--
ALTER TABLE `cmm_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `cmm_commentmeta`
--
ALTER TABLE `cmm_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cmm_comments`
--
ALTER TABLE `cmm_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `cmm_itsec_distributed_storage`
--
ALTER TABLE `cmm_itsec_distributed_storage`
  ADD PRIMARY KEY (`storage_id`),
  ADD UNIQUE KEY `storage_group__key__chunk` (`storage_group`,`storage_key`,`storage_chunk`);

--
-- Indexes for table `cmm_itsec_fingerprints`
--
ALTER TABLE `cmm_itsec_fingerprints`
  ADD PRIMARY KEY (`fingerprint_id`),
  ADD UNIQUE KEY `fingerprint_user__hash` (`fingerprint_user`,`fingerprint_hash`),
  ADD UNIQUE KEY `fingerprint_uuid` (`fingerprint_uuid`);

--
-- Indexes for table `cmm_itsec_geolocation_cache`
--
ALTER TABLE `cmm_itsec_geolocation_cache`
  ADD PRIMARY KEY (`location_id`),
  ADD UNIQUE KEY `location_host` (`location_host`),
  ADD KEY `location_time` (`location_time`);

--
-- Indexes for table `cmm_itsec_lockouts`
--
ALTER TABLE `cmm_itsec_lockouts`
  ADD PRIMARY KEY (`lockout_id`),
  ADD KEY `lockout_expire_gmt` (`lockout_expire_gmt`),
  ADD KEY `lockout_host` (`lockout_host`),
  ADD KEY `lockout_user` (`lockout_user`),
  ADD KEY `lockout_username` (`lockout_username`),
  ADD KEY `lockout_active` (`lockout_active`);

--
-- Indexes for table `cmm_itsec_logs`
--
ALTER TABLE `cmm_itsec_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module` (`module`),
  ADD KEY `code` (`code`),
  ADD KEY `type` (`type`),
  ADD KEY `timestamp` (`timestamp`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `blog_id` (`blog_id`);

--
-- Indexes for table `cmm_itsec_opaque_tokens`
--
ALTER TABLE `cmm_itsec_opaque_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD UNIQUE KEY `token_hashed` (`token_hashed`);

--
-- Indexes for table `cmm_itsec_temp`
--
ALTER TABLE `cmm_itsec_temp`
  ADD PRIMARY KEY (`temp_id`),
  ADD KEY `temp_date_gmt` (`temp_date_gmt`),
  ADD KEY `temp_host` (`temp_host`),
  ADD KEY `temp_user` (`temp_user`),
  ADD KEY `temp_username` (`temp_username`);

--
-- Indexes for table `cmm_links`
--
ALTER TABLE `cmm_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `cmm_options`
--
ALTER TABLE `cmm_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `cmm_postmeta`
--
ALTER TABLE `cmm_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cmm_posts`
--
ALTER TABLE `cmm_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `cmm_termmeta`
--
ALTER TABLE `cmm_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cmm_terms`
--
ALTER TABLE `cmm_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `cmm_term_relationships`
--
ALTER TABLE `cmm_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `cmm_term_taxonomy`
--
ALTER TABLE `cmm_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `cmm_usermeta`
--
ALTER TABLE `cmm_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `cmm_users`
--
ALTER TABLE `cmm_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `cmm_wc_admin_notes`
--
ALTER TABLE `cmm_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `cmm_wc_admin_note_actions`
--
ALTER TABLE `cmm_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `cmm_wc_category_lookup`
--
ALTER TABLE `cmm_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `cmm_wc_customer_lookup`
--
ALTER TABLE `cmm_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `cmm_wc_download_log`
--
ALTER TABLE `cmm_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `cmm_wc_order_coupon_lookup`
--
ALTER TABLE `cmm_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `cmm_wc_order_product_lookup`
--
ALTER TABLE `cmm_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `cmm_wc_order_stats`
--
ALTER TABLE `cmm_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `cmm_wc_order_tax_lookup`
--
ALTER TABLE `cmm_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `cmm_wc_product_meta_lookup`
--
ALTER TABLE `cmm_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `cmm_wc_tax_rate_classes`
--
ALTER TABLE `cmm_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `cmm_wc_webhooks`
--
ALTER TABLE `cmm_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cmm_woocommerce_api_keys`
--
ALTER TABLE `cmm_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `cmm_woocommerce_attribute_taxonomies`
--
ALTER TABLE `cmm_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `cmm_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `cmm_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `cmm_woocommerce_log`
--
ALTER TABLE `cmm_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `cmm_woocommerce_order_itemmeta`
--
ALTER TABLE `cmm_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `cmm_woocommerce_order_items`
--
ALTER TABLE `cmm_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `cmm_woocommerce_payment_tokenmeta`
--
ALTER TABLE `cmm_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `cmm_woocommerce_payment_tokens`
--
ALTER TABLE `cmm_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cmm_woocommerce_sessions`
--
ALTER TABLE `cmm_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `cmm_woocommerce_shipping_zones`
--
ALTER TABLE `cmm_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `cmm_woocommerce_shipping_zone_locations`
--
ALTER TABLE `cmm_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `cmm_woocommerce_shipping_zone_methods`
--
ALTER TABLE `cmm_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `cmm_woocommerce_tax_rates`
--
ALTER TABLE `cmm_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `cmm_woocommerce_tax_rate_locations`
--
ALTER TABLE `cmm_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `cmm_yoast_seo_links`
--
ALTER TABLE `cmm_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `cmm_yoast_seo_meta`
--
ALTER TABLE `cmm_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cmm_actionscheduler_actions`
--
ALTER TABLE `cmm_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `cmm_actionscheduler_claims`
--
ALTER TABLE `cmm_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_actionscheduler_groups`
--
ALTER TABLE `cmm_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cmm_actionscheduler_logs`
--
ALTER TABLE `cmm_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cmm_commentmeta`
--
ALTER TABLE `cmm_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_comments`
--
ALTER TABLE `cmm_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cmm_itsec_distributed_storage`
--
ALTER TABLE `cmm_itsec_distributed_storage`
  MODIFY `storage_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_itsec_fingerprints`
--
ALTER TABLE `cmm_itsec_fingerprints`
  MODIFY `fingerprint_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_itsec_geolocation_cache`
--
ALTER TABLE `cmm_itsec_geolocation_cache`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_itsec_lockouts`
--
ALTER TABLE `cmm_itsec_lockouts`
  MODIFY `lockout_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_itsec_logs`
--
ALTER TABLE `cmm_itsec_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cmm_itsec_temp`
--
ALTER TABLE `cmm_itsec_temp`
  MODIFY `temp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cmm_links`
--
ALTER TABLE `cmm_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_options`
--
ALTER TABLE `cmm_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=556;

--
-- AUTO_INCREMENT for table `cmm_postmeta`
--
ALTER TABLE `cmm_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=716;

--
-- AUTO_INCREMENT for table `cmm_posts`
--
ALTER TABLE `cmm_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `cmm_termmeta`
--
ALTER TABLE `cmm_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cmm_terms`
--
ALTER TABLE `cmm_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `cmm_term_taxonomy`
--
ALTER TABLE `cmm_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `cmm_usermeta`
--
ALTER TABLE `cmm_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `cmm_users`
--
ALTER TABLE `cmm_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cmm_wc_admin_notes`
--
ALTER TABLE `cmm_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cmm_wc_admin_note_actions`
--
ALTER TABLE `cmm_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cmm_wc_customer_lookup`
--
ALTER TABLE `cmm_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_wc_download_log`
--
ALTER TABLE `cmm_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_wc_tax_rate_classes`
--
ALTER TABLE `cmm_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cmm_wc_webhooks`
--
ALTER TABLE `cmm_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_api_keys`
--
ALTER TABLE `cmm_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_attribute_taxonomies`
--
ALTER TABLE `cmm_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `cmm_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_log`
--
ALTER TABLE `cmm_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_order_itemmeta`
--
ALTER TABLE `cmm_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_order_items`
--
ALTER TABLE `cmm_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_payment_tokenmeta`
--
ALTER TABLE `cmm_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_payment_tokens`
--
ALTER TABLE `cmm_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_sessions`
--
ALTER TABLE `cmm_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_shipping_zones`
--
ALTER TABLE `cmm_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_shipping_zone_locations`
--
ALTER TABLE `cmm_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_shipping_zone_methods`
--
ALTER TABLE `cmm_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_tax_rates`
--
ALTER TABLE `cmm_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_woocommerce_tax_rate_locations`
--
ALTER TABLE `cmm_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cmm_yoast_seo_links`
--
ALTER TABLE `cmm_yoast_seo_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cmm_wc_download_log`
--
ALTER TABLE `cmm_wc_download_log`
  ADD CONSTRAINT `fk_cmm_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `cmm_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
