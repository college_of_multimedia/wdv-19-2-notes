<h1>Loops.</h1>
<h2>Opdracht 1</h2>
Geef met maximaal 3 regels php de getallen 1 tot en met 99 weer<br>

<h2>Opdracht 2</h2>
Geef $counter weer zolang $counter < 87<br>
Tel tijdens elke loop 2 bij $counter op<br>
Het eerste getal dat je weergeeft is 0<br>
Elke counter op een losse regel<br>

<h2>Opdracht 3</h2>
Geef twee getallen weer in dezelfde loop<br>
Getal A begint bij 0 en gaat elke keer 1 omhoog<br>
Getal B begint bij 33 en gaat elke keer omlaag<br>
Laat dit zien zolang getal A kleiner is dan getal B<br>

<h2>Opdracht 4</h2>
Geef de getallen 1 tot en met 99 weer<br>
Sla de volgende getallen over: 99, 2, 5<br>
Zodra het getal deelbaar is door 2 moet je de kleur aanpassen van de tekst<br>
Als het getal groter is dan 78 mag je het script afbreken.

<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);
// error_reporting(E_ALL & ~E_NOTICE); // alles behalve de notices
$nl = "\n";
$br = '<br>' . $nl;
$hr = '<hr>' . $nl;

echo $hr;
echo '<h2>Opdracht 1:</h2>' . $nl;

for ($counter = 1; $counter <= 99; $counter++ ){
	echo $counter . $nl;
}

echo $hr;
echo '<h2>Opdracht 2a:</h2>' . $nl;
for ($counter = 0; $counter <= 99; $counter += 2 ){
	if ($counter < 87){
		echo $counter . $br;
	}
}

echo '<h2>Opdracht 2b:</h2>' . $nl;
for ($counter = 0; $counter <= 99; $counter++ ){
	if ($counter >= 87) break;
	echo $counter++ . $br;
}


echo $hr;
echo '<h2>Opdracht 3a:</h2>' . $nl;

$getalA = 0;
$getalB = 33;
for ($getalA; $getalA <= 99; $getalA++ ){
	if ($getalA < $getalB){
		echo $getalA . ':' . $getalB-- . $br;
	} else {
		break;
	}
}
echo '<h2>Opdracht 3b:</h2>' . $nl;

$getalA = 0;
$getalB = 33;
for ($getalA; $getalA <= 99; $getalA++ ){
	if ($getalA >= $getalB) break;
	echo $getalA . ':' . $getalB-- . $br;
}

echo $hr;
echo '<h2>Opdracht 4:</h2>' . $nl;

for ($counter = 1; $counter <= 99; $counter++){
	if ($counter == 99 || $counter == 2 || $counter == 5) continue;
	if ($counter > 78) break; // or die();
	$style = ($counter % 2 == 0) ? 'red': 'black';
	echo '<div style="color:' . $style . '">' . $counter . '</div>' . $nl;
}



