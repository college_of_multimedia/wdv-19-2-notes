<h1>Arrays.</h1>
<h2>Opdracht 0</h2>
Maak een associatieve array aan met de volgende elementen: <br>
- type<br>
- merk<br>
- kleur<br>
<br>
Geef de volgende zin weer op basis van de informatie uit de array<br>
<br>
De <b>Volkswagen</b> van het type <b>Polo</b> is <b>rood</b><hr>
<?php
$auto = [];
$auto['type'] 	= 'A3';
$auto['merk'] 	= 'Audi';
$auto['kleur'] 	= 'paars';

echo 'De <b>' . $auto['merk'] . '</b> ' . $auto['type'] . ' heeft de kleur: ' . $auto['kleur'];
?>
<hr>

<h2>Opdracht 1</h2>
Een indexed array aan met minimaal 5 auto merken<br>
Geef de complete inhoud van deze array weer<br>
<hr>
<h3>resultaat opdracht 1:</h3>
<?php
$autos = [	
			[	 'A3',
				 'Audi',
				 'paars'
			],[	 'Polo',
				 'Volkswagen',
				 'rood'
			],[	 'B-class',
				 'Mercedes',
				 'zwart'
			],[	 'SLK',
				 'Mercedes',
				 'zilver'
			],[	 'D4',
				 'Citroen',
				 'blauw'
			]
];
echo "<p>\n";
// var_dump ($autos);
/* 	ik dump de autos door voor elke auto een echo regel te maken, 
	maar $autos[$count][1] kan ook 
	bijv $count in een for loop
*/
echo "De {$autos[0][1]} van het type {$autos[0][0]} is {$autos[0][2]}<br>\n";
echo "De {$autos[1][1]} van het type {$autos[1][0]} is {$autos[1][2]}<br>\n";
echo "De {$autos[2][1]} van het type {$autos[2][0]} is {$autos[2][2]}<br>\n";
echo "De {$autos[3][1]} van het type {$autos[3][0]} is {$autos[3][2]}<br>\n";
echo "De {$autos[4][1]} van het type {$autos[4][0]} is {$autos[4][2]}<br>\n";
echo '</p>';

?>
<hr>
<h2>Opdracht 2</h2>
Maak nu een associatieve array met 5 automerken<br>
Geef de complete inhoud van deze array weer<br>
<hr>
<h3>resultaat opdracht 2:</h3>
<?php
$autos = [	'A3' => [
				'type'	=> 'A3',
				'merk'	=> 'Audi',
				'kleur'	=> 'paars'
			], 'Polo' => [
				'type'	=> 'Polo',
				'merk'	=> 'Volkswagen',
				'kleur'	=> 'rood'
			], 'B-class' => [
				'merk'	=> 'Mercedes',
				'type'	=> 'B-class',
				'kleur'	=> 'zwart'
			], 'SLK' => [	
				'type'	=> 'SLK',
				'merk'	=> 'Mercedes',
				'kleur'	=> 'zilver'
			], 'D4' => [	
				'type'	=> 'D4',
				'merk'	=> 'Citroen',
				'kleur'	=> 'blauw'
			]
];
echo "<p>\n";
// var_dump ($autos);
/* 	ik dump elke auto die ik uit de array haal in een foreach loop
	en hoef dan maar een uitvoer te maken
*/
foreach ($autos as $auto) {
	echo "De {$auto['merk']} van het type {$auto['type']} is {$auto['kleur']}<br>\n";
}
echo '</p>';
?>
<hr>

<h2>Opdracht 3</h2>
Gebruik de bovenstaande array<br>
Voeg per merk 3 types toe<br>
Geef de complete inhoud van deze array weer<br>
<hr>
<h3>resultaat opdracht 3:</h3>
<?php
$autos = [	'Golf' => [
				'type'	=> 'Golf',
				'merk'	=> 'Volkswagen',
				'kleur'	=> 'paars'
			], 'Polo' => [
				'type'	=> 'Polo',
				'merk'	=> 'Volkswagen',
				'kleur'	=> 'rood'
			], 'Up' => [
				'type'	=> 'Up',
				'merk'	=> 'Volkswagen',
				'kleur'	=> 'zwart'
			], 'SLK' => [	
				'type'	=> 'SLK',
				'merk'	=> 'Mercedes',
				'kleur'	=> 'zilver'
			], 'B-class' => [	
				'type'	=> 'B-class',
				'merk'	=> 'Mercedes',
				'kleur'	=> 'zwart'
			], 'C-class' => [	
				'type'	=> 'C-class',
				'merk'	=> 'Mercedes',
				'kleur'	=> 'blauw'
			]
];
echo "<p>\n";
// var_dump ($autos);
foreach ($autos as $auto) {
	echo "De {$auto['merk']} van het type {$auto['type']} is {$auto['kleur']}<br>\n";
}
echo '</p>';
?>

<hr>

<h2>Opdracht 4</h2>
Gebruik de bovenstaande array<br>
Geef per type aan wat de prijs is<br>
Geef de complete inhoud van deze array weer<br>
<hr>
<h3>resultaat opdracht 4:</h3>
<?php
/* 	ik heb alle autos al, dus ik voeg alleen de prijs toe
	Doordat het type ook de associatieve index is, 
	kan ik gewoon op alphabetische volgorde de prijzen toe kennen
*/

$autos['B-class']['prijs'] = 33323.00;
$autos['C-class']['prijs'] = 44423.45;
$autos['Golf']['prijs'] = 12345.67;
$autos['Polo']['prijs'] =  10000.40;
$autos['SLK']['prijs'] = 62223.45;
$autos['Up']['prijs'] = 12121.12;

echo "<p>\n";
// var_dump ($autos);
foreach ($autos as $auto) {
	echo "De {$auto['merk']} van het type {$auto['type']} is {$auto['kleur']} en kost {$auto['prijs']}<br>\n";
}
// prijs is lelijk!! bah
echo '</p>';
?>

<hr>

<h2>Opdracht 5</h2>
Pas de weergave van de laaste array zo aan dat het volgende zichtbaar is<br>
De <b>Volkswagen</b> van het type <b>Polo</b> kost <b>&euro; 13.333,00</b><br>
De <b>Volkswagen</b> van het type <b>Up</b> kost <b>&euro; 9.999,00</b><br>
De <b>Volkswagen</b> van het type <b>Golf</b> kost <b>&euro; 39.999,00</b><br>
De <b>BMW</b> van het type <b>3</b> kost <b> &euro; 13.33300</b><br>
etc.
<hr>
<h3>resultaat opdracht 5:</h3>
<p>
<?php
// <p> hoeft niet perse in de echo kan ook in de jump-in-out style

// var_dump ($autos);
foreach ($autos as $auto) {
	echo "De <b>{$auto['merk']}</b> van het type <b>{$auto['type']}</b> is {$auto['kleur']} en kost <b>&euro; " . number_format($auto['prijs'],2,',','.') . "</b><br>\n";
}
// nu prijs netjes met euroteken en 2 cijfers achter de "komma" en elk 1000tal een punt
// https://www.php.net/manual/en/function.number-format.php
// $getal 12345.678 met number_format($getal,2,',','.') wordt 12.345,68 
// $getal 12345.678 met number_format($getal,0,'.',',') wordt 12.346 
?>
</p>

<hr>



