<h1>Arrays - Statements.</h1>
<h2>Opdracht 1</h2>
Laat alle items uit de onderstaande array zien<br>

<h2>Opdracht 2</h2>
Laat alleen Karel zien<br>

<h2>Opdracht 3</h2>
Laat iedereen zien behalve Kees<br>

<h2>Opdracht 4</h2>
hoeveel regels zitten er in de array?<br>

<h2>Opdracht 5</h2>
Geef alle namen netjes onder elkaar weer.<br>
Als iemand geen tussenvoegsel heeft dan mag er maat 1 spatie zitten tussen de voor en achternaam<br>

<br>
<hr>

<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);
$br = "<br>\n";

$myArr = array();
$myArr[] = array( 'voornaam'=>'kees', 	'tussen'=>'de',	'achternaam'=>'vries');
$myArr[] = array( 'voornaam'=>'Jan', 	'tussen'=>'', 	'achternaam'=>'bakker');
$myArr[] = array( 'voornaam'=>'Karel', 	'tussen'=>'', 	'achternaam'=>'groenteman');
$myArr[] = array( 'voornaam'=>'Jan', 	'tussen'=>'de', 'achternaam'=>'vries');
$myArr[] = array( 'voornaam'=>'Joop', 	'tussen'=>'de', 'achternaam'=>'bakker');
$myArr[] = array( 'voornaam'=>'piet', 	'tussen'=>'', 	'achternaam'=>'bakker');

// opdracht 1
echo "<h3>Oplossing 1</h3><pre>";
foreach ($myArr as $persoon) {
    echo 'Voornaam   : ' . $persoon['voornaam'] . "\n";
    echo 'Tussen     : ' . $persoon['tussen'] . "\n";
    echo 'Achternaam : ' . $persoon['achternaam'] . "\n";
    echo "<hr>";
}
echo "</pre>";

// opdracht 2
echo "<h3>Oplossing 2</h3><pre>";
foreach ($myArr as $persoon) {
	if ('Karel' !== $persoon['voornaam']) continue;
    echo 'Voornaam   : ' . $persoon['voornaam'] . "\n";
    echo 'Tussen     : ' . $persoon['tussen'] . "\n";
    echo 'Achternaam : ' . $persoon['achternaam'] . "\n";
    echo "<hr>";
}
echo "</pre>";

// opdracht 4
echo "<h3>Oplossing 4</h3>";
echo 'Er zijn ' . count($myArr) . ' personen' . $br;
echo "<hr>";

// opdracht 3 en 5
// Ik loop nu door myArr heen en zeg dat elk item $persoon is
echo "<h3>Oplossing 3 en 5</h3>";

foreach ($myArr as $persoon) {
    if ('kees' === $persoon['voornaam']) continue; // onderdeel opdracht 3

    echo $persoon['voornaam'] . ' ';

    if ( ! empty($persoon['tussen'])) {
        echo $persoon['tussen'] . ' ';
    }

    echo $persoon['achternaam'] . $br;
}
