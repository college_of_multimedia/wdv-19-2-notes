<?php
$bierArr = array();	// reset de array en define de variabele.

$bierArr["ij"]					= array("KEY"=>"VALUE", "schuim"=>"ja", "kleur"=>"blond");
$bierArr["jupiler"]				= array();	// zeg dat de inhoud van jupiler een array is
$bierArr["jupiler"]["KEY"]		= "VALUE";	// jupiler heeft een onderdeel KEY
$bierArr["jupiler"]["kleur"]	= "blond";

$bierArr["guinnes"]	= array();
$bierArr["guinnes"]["KEY"]		= "VALUE";
$bierArr["guinnes"]["schuim"]	= "nee";
$bierArr["guinnes"]["kleur"]	= "donker";

$bierArr["amstel"]	= array();
$bierArr["amstel"]["KEY"]		= "VALUE";
$bierArr["amstel"]["schuim"]	= "ja";
$bierArr["amstel"]["kleur"]		= "licht";

$bierArr["amstel2"]	= array();
$bierArr["amstel2"]["KEY"]		= "VALUE";
$bierArr["amstel2"]["schuim"]	= "ja";
$bierArr["amstel2"]["kleur"]	= "donker";

/*
ij, shuim=ja
jupiler, shuim=niet bekend
guinness, shuim=nee
amstel, shuim=ja
amstel2, shuim=ja
*/


// Geef alle items uit de array weer in de vorm van een list, bv:
?>
<ul>
	<li>ij</li>
	<li>jupiler</li>
	<li>cola</li>
</ul>
<?php
// hoeveel items zitten er in de array?
// alleen het item 'spa' zien
// vervang amstel voor palm
// voeg een nieuw item toe
// voeg nog een nieuwe drank toe
// sorteer op naam en laat het resultaat zien
// Laat alles zien behalve de appelsap
// geef alle items weer die schuim hebben



    ?>
Opdracht dag 2

$itemsArray = array();
$itemsArray['appel'] 	= array( "aantal_items" => 12,
								"naam" 			=> "appel",
								"type" 			=> "fruit",
								"stuk_prijs" 	=> 0.10,
								"kleur" 		=> "groen" );
$itemsArray[''] 		= array( "aantal_items" => 32,
								"naam" 			=> "peer",
								"type" 			=> "fruit",
								"stuk_prijs" 	=> 0.12,
								"kleur" 		=> "groen" );
$itemsArray['aardbei']	= array( "aantal_items" => 92,
								"naam" 			=> "aardbei",
								"type" 			=> "fruit",
								"stuk_prijs" 	=> 0.06,
								"kleur" 		=> "rood" );
$itemsArray[]			= array( "aantal_items" => 32,
								"naam" 			=> "banaan",
								"type" 			=> "fruit",
								"stuk_prijs" 	=> 0.13,
								"kleur" 		=> "geel" );
$itemsArray['sla']		= array( "aantal_items" => 10,
								"naam" 			=> "rucola",
								"type" 			=> "groente",
								"stuk_prijs" 	=> 0.30,
								"kleur" 		=> "groen" );

Er is een winkelmandje met diverse producten.
Er moet een weergave van een groenten / fruit mandje gemaakt worden.
Alle items uit de array moeten weergegeven worden ( alle appels, peren, bananen, etc )
Opmaak is niet belangrijk, gebruik simpele HTML. Bijvoorbeeld een tabel.
Maak het in php op de voor jou beste manier, zorg er wel voor dat de code leesbaar blijft.

- Er zijn minimaal 5 soorten groente of fruit, je mag er natuurlijk ook meer bij plaatsen.
- Prijs per stuk moet zichtbaar zijn (in euro's, 2 getallen achter komma)

extra:
- Bereken het totaal bedrag per kistje en maak dit zichtbaar op elke regel.
- Onder de tabel moet de totale som van 'aantal_items' weergegeven worden


bv
Aarbei, aantal items: 92 stuks, kleur: rood, prijs 92 * € 0,06 =  € 5,52
