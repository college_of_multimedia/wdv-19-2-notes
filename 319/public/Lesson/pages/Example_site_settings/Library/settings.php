<?php

$debug = true;

ini_set( 'display_errors', (int) $debug );
// ini_set('error_reporting', E_ALL);
error_reporting(E_ALL & ~E_NOTICE);
// error_reporting(E_ALL);

$br = '<br>' ."\n";

$path = __DIR__;

define('LIB_ROOT'       , realpath($path)                           );
define('INC'            , realpath(LIB_ROOT . '/includes')          );
define('SITE_ROOT'      , realpath($path . '/..')                   );

// heet de root folder public of public-html of htdocs oid deze staat nu netjes in $rootfolder
$rootfolder = explode( '/', substr( $_SERVER['SCRIPT_FILENAME'], strlen( SITE_ROOT ) + 1 ) ) [0];

define('DOCUMENT_ROOT'  , realpath(SITE_ROOT . '/' . $rootfolder)   );   
define('IMG_ROOT'       , realpath(DOCUMENT_ROOT . '/images')       );

// als de $_SERVER['DOCUMENT_ROOT'] niet de echte DOCUMENT_ROOT is plak de diepere folder er achter
$WEB_PATH = '';
if (DOCUMENT_ROOT !== $_SERVER['DOCUMENT_ROOT']) 
    $WEB_PATH = substr(DOCUMENT_ROOT, strlen($_SERVER['DOCUMENT_ROOT']));

define('WEB_URL'        , '//' . $_SERVER['HTTP_HOST'] . $WEB_PATH  );
define('IMG_URL'        , WEB_URL . '/images'                       );
define('CSS_URL'        , WEB_URL . '/css'                          );
define('JS_URL'         , WEB_URL . '/js'                           );

// #### Zoek alle overige defines op door de server te identificeren ####

switch ($_SERVER['SERVER_NAME']) {
    case 'cmm-students.localhost':        // #### Nakijk Server (Homestead) ####
        define('DB_HOST'        , 'localhost'                       );
        define('DB_NAME'        , 'cmmdatabase'                     );
        define('DB_USER'        , 'homestead'                       );
        define('DB_PASS'        , 'secret'                          );
        break; 
            
    case 'student.cmmstudents.test':        // #### test Server (student thuis) ####
        define('DB_HOST'        , 'localhost'                        );
        define('DB_NAME'        , 'wd_cmmnews'                       );
        define('DB_USER'        , 'gdeyyxunhyugwe'                   );
        define('DB_PASS'        , 'jd4r73r83'                        );
        break;

    default:                                // #### Test Server (deslager) ####
        define('DB_HOST'        , 'localhost'                         );
        define('DB_NAME'        , 'cmmnews_2019'                      );
        define('DB_USER'        , 'root'                              );
        define('DB_PASS'        , 'root'                              );
}

// laad de algemene scripts
require_once( LIB_ROOT . '/functions.php' );