<html>
<head>
    <title>WD 319</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style>
        #holder {
            margin      : 50px auto;
            width       : 800px;
            padding     : 0 5px;
            color       : #4F3E25;
            font-family : "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        .ui-icon {
            margin : 0 10px 0 0;
        }

        .ui-accordion .ui-accordion-content {
            padding : 1.2rem 1.5rem 4rem;
        }
    </style>

    <script>
		var icons = {
			header       : "ui-icon-circle-arrow-e",
			activeHeader : "ui-icon-circle-arrow-s"
		};
		$( function () {
			$( "#accordion" ).accordion( {
				icons       : icons,
				active      : 100,
				heightStyle : "content"
			} );
		} );
    </script>
</head>

<body>
<div id="holder">
    <h1>Deze les:</h1>
    <div id="accordion">

        <h3>Hou je aan de opdracht, controleer in de browser, daarna pas css</h3>
        <div>
            <b>Beoordeling op :</b><br>
            - code<br>
            - techniek <br>
            - leesbaarheid<br>
            - functionel resultaat<br>
            <br>
            <b>Geen beoordeling op:</b><br>
            - design<br>
        </div>

        <h3>Echo / Print</h3>
        <div>
            <b>Doel:</b><br>
            - Snelheid<br>
            - Alleen echo gebruiken<br>
            <br>
            <a href="https://www.w3schools.com/php/php_echo_print.asp">https://www.w3schools.com/php/php_echo_print.asp</a>
        </div>

        <h3>Geen lege regel aan begin of eind</h3>
        <div>
            <b>Doel:</b><br>
            - Headers<br>
            - Sessies<br>
            - Output naar browser<br>
            - vb footer.php in index.php
        </div>

        <h3>Browser cache</h3>
        <div>
            <pre>
                &smt;img src="mijn_foto.png?t=<?= time(); ?>" alt="profielfoto"&gt;
            </pre>
            <b>Doel:</b><br>
            - Uniek bestand<br>
            - Geld voor browser files, js, img, css<br>
            - Get variabele geeft versie<br>
        </div>

        <h3>Yoda style</h3>
        <div>
    <pre>
    if ( $post_type == 'post' ) {
        // do stuff
    }
    </pre>
            <pre>
    if ( 'post' == $post_type ) {
        // do stuff
    }
    </pre>
            <br>
            <br>
            <pre>
    if ( $number_of_posts >= 5  ) {
        // do stuff
    }
    </pre>
            <pre>
    if ( 5 <= $number_of_posts ) {
        // do stuff
    }
    </pre>
            <b>Doel:</b><br>
            - Niet sneller<br>
            - Minder fout gevoelig<br>
            - Net zo leesbaar als vroeger<br>
            <br>
            <a href="https://knowthecode.io/yoda-conditions-yoda-not-yoda">https://knowthecode.io/yoda-conditions-yoda-not-yoda</a>
        </div>

        <h3>Early return</h3>
        <div>
        <pre>
            function ( $value = true ) {
                if ( is_string( $value ) ) {
                    echo 'String';
                }
            }
        </pre>
            <b>Doel:</b><br>
            - leesbaarheid<br>
            - overzicht<br>
            - snelheid<br>
            <br>
            <a href="pages/functie-progressie.php">functie-progressie</a>
        </div>


        <h3>Duplicate code</h3>
        <div>
            Gewoon niet doen. <br>
            Gebruik een functie<br>
            <br>
            <b>Doel</b><br>
            - Snelheid<br>
            - Overzicht<br>
            - Herbruikbaar<br>
        </div>

        <h3>Einde loop</h3>
        <div>
            <b>Code moet overzichtelijk zijn</b><br>
            - Extra comments niet nodig<br>
            - Geen end while comment<br>
        </div>

        <h3>Locatie van comments</h3>
        <div>
<pre>
    function showMyValue_1($value = '')
    {
        # Dit is een mooie functie die iets speciaals doet.
        if ( ! is_string($value)) { # Is het echt een string
            return 'Geen string';   # het is misschien handig dat hier een code staat
        }   # einde van de if

        if (strlen($value) > 5) { // Is de string lang genoeg
            return 'String groter dan 5 karakters'; // dan wat tekst
        }   # einde van de if

        return 'Dit is mijn String: ' . $value; // Hier laat ik de tekst zien
    }
</pre>
            <b>In een commentaar vertel je wat je gaat doen</b><br>
            - plaats comments bove de code die je gaat uitvoeren<br>
            <br>
            
        </div>

        <h3>DocBlock</h3>
        <div>
<pre>/**
 * Dit is een mooie functie die iets speciaals doet.
 *
 * @param string $value
 *
 * @return string
 */
function showMyValue_2($value = '')
{
        </pre>
            <b>Code boven een functie</b><br>
            - editor gebruikt dit voor code hinting<br>
            - documentatie software<br>
            - Belangrijkste: jij zelf en andere dev<br>
            - Code valideren en duplicate code voorkomen<br>
            - Code hinting phpStorm<br>
        </div>

        <h3>Enkele vs Dubbele quotes</h3>
        <div>
            <b>Doel:</b><br>
            - Snelheid<br>
            - Overzicht<br>
            - HTML<br>
        </div>

        <h3>Relatief pad naar config</h3>
        <div>
            <pre>
switch ($_SERVER['SERVER_NAME']) {
    case 'cmmproject.cmm':
        define('DB_HOST', 'localhost');
        define('DB_USER', 'root');
        break;

            </pre>
            <b>Doel:</b><br>
            - Code moet onafhankelijk van omgeving zijn<br>
            - Alle code blind naar prod uploaden<br>
            <br>
            <a href="pages/Example_site_settings/public_html/index.php">Example_site_settings</a>
        </div>

        <h3>Sanatize POST</h3>
        <div>
            <b>Doel:</b><br>
            - Zeker weten dat de variabele veilig is.<br>
            - Gebruik van variabele binnen je code, niet $_POST['xxxx']<br>
            - Sanatize voorbeeld<br>
            <br>
           <a href="pages/injection.php">Injection</a><br>
           <a href="pages/sanit.php">Sanit</a><br>
           <a href="pages/hack_project/index.php">TEACHER ONLY HACK EXAMPLE!</a>
        </div>

        <h3>Inspringen van code, wanneer wel / niet</h3>
        <div>
        <pre>
function showMyValue($value = '')
{
    if ( ! is_string($value)) {
return 'Geen string';
    } else echo 'anders'; }
    echo 'mooi';
        </pre>
            <b>Doel:</b><br>
            - Na een accolade openen inspringen {<br>
            - Na een accolade sluiten terug }<br>
        </div>


        <h3>Ook correcte HTML Source</h3>
        <div>
            <b>Doel:</b><br>
            - Vanaf volgende week moet de HTML source correct zijn<br>
            - Inspringen in HTML is minder belangrijk<br>
            - Onderdeel openen betekend ook sluiten<br>
        </div>

        <h3>Error handling</h3>
        <div>
        <pre>
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        </pre>
            <b>Doel:</b><br>
            - Error vrijde code schrijven<br>
            - Straks ook Try - Catch - Exception<br>

            <a href="pages/functie-met-exception.php">functie-met-exception</a>
        </div>

        <h3>Wachtwoord</h3>
        <div>
            <a href="https://ithemes.com/2018/05/03/hows-your-wordpress-password-strength-take-this-quiz/">https://ithemes.com/2018/05/03/hows-your-wordpress-password-strength-take-this-quiz/</a>
        </div>
        <h3>Captcha</h3>
        <div>
            <a href="pages/captcha/index.php">Captcha</a>
        </div>
    </div>

</div>
</body>
</html>