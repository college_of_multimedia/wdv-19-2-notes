<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="favicon.ico">

    <title>College of MultiMedia | <?= $page_title; ?></title>

    <link href="<?=CSS_URL?>/bootstrap.css" rel="stylesheet">
    <link href="<?=CSS_URL?>/main.css" rel="stylesheet">
</head>

<body>
