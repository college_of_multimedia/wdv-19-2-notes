<?php
define('DB_NAME', 'root');
define('DB_PASS', 'root');

define('WEB_URL', 'http://www.deslager.test/');
define('DOCUMENT_ROOT', '/Users/wdv/Sites/deslager/public/');

$enkeleString = 'het is \'savonds koud\\koel';


// echo "Hoi";
// het is ongewenst om uitvoer (echo) te hebben 
// VOOR dat de doctype is geladen

// <?= is hetzelfde als <?php echo
// 'string' met enkele quotes is een LITERAL string
// "string" met dubbelle quotes is een EVALUATED string

$br = "<br>\n";
$page_title = 'Mijn eerste PHP pagina!'; 
$naam = 'Harald';
// $par = '<p class="welkom">' . $boodschap . '</p>';

$uurloon = 12.34;
$leeftijd = 23 + 5;

$kind1 = 'Davey';
$kind2 = 'Robin';

$kids[] = 'Davey'; // das index 0
$kids[] = 'Robin'; // das index 1 (automatisch)
$kids[5] = 'Sammie'; // das index 5 (hard)

$kids = []; // reset de kids Array

$kids['primary'] = 'Davey'; // das index primary
$kids['secondary'] = 'Robin'; // das index secondary
$kids[] = 'Sammie'; // das index 0 (automatisch)

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $page_title }}</title>
</head>
<body>
	<h1><?=$page_title?></h1>

	<p>Hallo <?=$naam;?>, hoe gaat het</p>

<?php	
	echo $par;
	echo "\tDit is een tekst".$br;
	echo "\tDit is een tekst$br";
	echo '\tDit is een tekst'.$br;
	echo '\tDit is een tekst$br';
	echo $br;
	echo "\t" . 'Dit is een tekst' . $br;
	echo "Welkom op $page_title, ik ben $naam!$br";
	echo $br;
	echo "<pre>\n";
	
	var_dump($kids);
	print_r($kids);
	
	var_dump($leeftijd);
	var_dump(TRUE);
	print_r(true);
	var_dump(NULL);
	print_r(null);
	// var_dump($empty);

	$lijst = array(12,34,45);

	define('PI', 3.1415);

	echo 'het getal pi is ' . PI;
	echo "het getal pi is " . PI;

	echo "</pre>\n";

	echo gettype($leeftijd);

	// if ( gettype($leeftijd) == 'integer' )
	// if ( is_int($leeftijd) )
		
	//	if ( isset($voornaam) && empty($voornaam)
	echo "<pre>\n";
	print_r($kids);	
	echo "</pre>\n";
?>
<!-- DIT IS MIJN DEBUG: <?php print_r($kids) ?> -->
<?php

$naam = 'Harald';
$leeftijd = '23';
$uurloon = 123.45;

echo "Hallo $naam, je bonus is €" . $leeftijd * $uurloon;
echo $br;
echo "Hallo $naam, je bonus is € $leeftijd * $uurloon";
echo $br;

$fruit = 'Apple';
echo "Do you like {$fruit}s";
echo $br;
echo "dit is {$kind1}s auto";
echo $br;
echo "dit is {$kids['primary']}s auto";



?>
</body>
</html>