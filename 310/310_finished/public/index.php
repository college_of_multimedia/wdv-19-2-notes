<?php require_once '../Library/settings.php'; ?>
<?php 
$page_name = 'home';
$page_title = 'Greenleaf Homepage';
require_once INC . '/header.php';
?>
			<h1 id="logo"><img src="images/logo-greenleaf.jpg" alt=""> Greenleaf - Garden Center</h1>
<?php require INC . '/nav.php'; ?>
			<header>
				<div id="pullquote">
					<h2>Are you prepared to be spellbound?</h2>
					<p>"Trevi" meaning three roads in Latin, offers discerning consumers three exciting ways to savor and enjoy the true spirit of Italy.</p>
				</div>
				<div id="slideshow"> 
					<p>A lovely chinese garden with a palace.</p>
				</div>
			</header>
			<section id="content">
				<article>
					<img src="images/leaf1.jpg" alt="">
					<img src="images/dwell.jpg" alt="Dwell">
					<h2>Dwell</h2>
					<p>Italian adventures for a select group of world travelers seeking authentic experiences layered with tailored activities.</p>
				</article>
				<article> 
					<img src="images/leaf2.jpg" alt="">
					<img src="images/decorate.jpg" alt="Decorate">
					<h2>Decorate</h2>
					<p>Exclusive Italian-designed, handmade home and giftware selections for style-conscious consumers.</p>
				</article>
				<article> 
					<img src="images/leaf3.jpg" alt="">
					<img src="images/discover.jpg" alt="Discover">
					<h2>Discover</h2>
					<p>A luxury vacation home in the undiscovered region of Puglia becomes a reality with the support of our multi-lingual, local team.</p>
				</article>
				<blockquote class="cta">
					<p>“Greenleaf takes you there”</p>
				</blockquote>
			</section>
<?php require INC . '/footer.php'; ?>	