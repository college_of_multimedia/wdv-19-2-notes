<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);
// error_reporting(E_ALL & ~E_NOTICE); // alles behalve de notices


define('DOCUMENT_ROOT',	realpath( $_SERVER['DOCUMENT_ROOT'] )	);
define('IMG_ROOT',		realpath( DOCUMENT_ROOT . '/images' ) 		);
define('SITE_ROOT',		realpath( DOCUMENT_ROOT . '/..' ) 		);
define('LIB_ROOT', 		realpath( SITE_ROOT . '/Library' ) 		);
define('INC', 			realpath( LIB_ROOT . '/includes' ) 		);

define('WEB_URL', 	'//' .$_SERVER['HTTP_HOST']	);
// auto http:// of https://
define('IMG_URL', 	WEB_URL . '/images'			);
define('CSS_URL', 	WEB_URL . '/css'			);
define('JS_URL', 	WEB_URL . '/js'				);
define('PAGE_URL', 	WEB_URL . '/pages'			);

// __DIR__ is het path van het huidige bestand!!
// __FILE__ is het path en naam van het huidige bestand!!

require_once 'functions.php';



