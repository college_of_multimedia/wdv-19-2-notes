
			<footer>
				<ul class="footerlist">
					<li><a href="mailto:info@garden-center.com">Email us</a></li>
					<li><a href="tel:+19789984092">work +1.978.998.4092</a></li>
					<li><a href="tel:+16179015512">mobile +1.617.901.5512</a></li>
					<li>fax +1.978.338.012</li>
				</ul>
				<ul class="footerlist">
					<li><a href="<?= WEB_URL ?>/index.php">Homepage</a></li>
					<li><a href="<?= WEB_URL ?>/pages/about.php">About Us</a></li>
					<li><a href="<?= WEB_URL ?>/pages/visit.php">Products to buy</a></li>
					<li><a href="<?= WEB_URL ?>/pages/contact.php">History of Greenleaf</a></li>
				</ul>
				<ul class="footerlist">
					<li>P.O. Box 297</li>
					<li>Prides Crossing</li>
					<li>MA 01965</li>
					<li>USA</li>
					<li>&copy; 2017-<?= date('Y') ?></li>
				</ul>
			</footer>
		</div>
		<script src="<?= JS_URL ?>/main.js"></script>
	</body>
</html>