 <?php

$nl = "\n";
$br = '<br>' . $nl;
$hr = '<hr>' . $nl;

/**
 * debuggen laat een nette dump van een variabel zien
 * eventueel met bestandsnaam en regelnummer
 * @param  mixed  $val  de tonen variabel
 * @param  string $file bestandsnaam van de dump
 * @param  string $line regelnummer van de dump			
 * @return void   geen  return dus
 */
function debuggen($val, $file='', $line=''){
	global $debug;
	if ($debug) {
		echo '<pre style="border : solid black 1px">' . $nl;
		if ( '' !== $file ) {
			echo "$file \nLine $line \n";
		}
		var_dump($val);
		echo '</pre>';
	} 
}
