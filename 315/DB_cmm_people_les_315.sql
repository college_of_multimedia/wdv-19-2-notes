-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 09, 2019 at 08:20 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cmm_people`
--
DROP DATABASE IF EXISTS `cmm_people`;
CREATE DATABASE `cmm_people` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `cmm_people`;

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
CREATE TABLE `people` (
  `userID` smallint(5) UNSIGNED NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` char(32) NOT NULL,
  `reg_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`userID`, `firstname`, `surname`, `username`, `email`, `pass`, `reg_date`) VALUES
(2, 'Harald', 'Schilling', 'teacher', 'Harald@cmm.nl', 'f5f46a62bde5dd979c0652f06d716a9e', '2019-12-09 00:00:00'),
(3, 'Piet', 'Schilling', 'teacher1', 'Piet@cmm.nl', 'e8636ea013e682faf61f56ce1cb1ab5c', '2019-12-09 00:00:00'),
(4, 'Klaas', 'Schilling', 'teacher2', 'Klaas@cmm.nl', 'e8636ea013e682faf61f56ce1cb1ab5c', '2019-12-09 00:00:00'),
(5, 'Johan', 'Schilling', 'teacher3', 'Johan@cmm.nl', 'e8636ea013e682faf61f56ce1cb1ab5c', '2019-12-09 00:00:00'),
(6, 'Peter', 'de Vries', 'teacher4', 'teacher@cmm.nl', 'e8636ea013e682faf61f56ce1cb1ab5c', '2019-12-09 00:00:00'),
(7, 'Jan', 'Jansen', 'teacher5', 'Peter@cmm.nl', 'e8636ea013e682faf61f56ce1cb1ab5c', '2019-12-09 00:00:00'),
(8, 'Bla', 'de Blah', 'teacher6', 'bla@cmm.nl', 'e8636ea013e682faf61f56ce1cb1ab5c', '2019-12-09 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `name` (`firstname`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `userID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
