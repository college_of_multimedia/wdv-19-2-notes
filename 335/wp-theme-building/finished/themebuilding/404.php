<?php get_header() ?>

<section class="about subpage sidebar text_column" id="about">
	<div id="content">

		<div id="post-0" class="post error404">
			<h2 class="post-title">Niet gevonden</h2>
			<div class="post-content">
				<p>Apologies, but we were unable to find what you were looking for. Perhaps  searching will help.</p>
			</div>
			<form id="error404-searchform" method="get" action="<?php echo esc_url( home_url('/') ); ?>">
				<div>
					<input id="error404-s" name="s" type="text" value="<?php echo esc_html(stripslashes($_GET['s']), true) ?>" size="40" />
					<input id="error404-searchsubmit" name="searchsubmit" type="submit" value="Find" />
				</div>
			</form>
		</div><!-- .post -->

	</div><!-- #content -->
</section>

<?php get_sidebar() ?>
<?php get_footer() ?>