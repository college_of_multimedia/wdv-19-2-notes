<?php

// disable gutenberg
add_filter('use_block_editor_for_post', '__return_false');

function custom_font_backend(){
	add_editor_style( 'css/style-admin.css' );
}
add_action( 'admin_init', 'custom_font_backend' );

// gebruik deze functie voor het ophalen van het post object en bekijk alle opties die de post heeft
function dump( $stuff ){
	
	echo '<pre>';	
	var_dump( $stuff );
	echo '</pre>';
}

if( !function_exists( 'the_field' ) ){
	//add_action( 'admin_notices', 'acf_notice' );
}

function acf_notice(){
	?>
		
		<div class="update-nag notice">
			<p>Advanced Custom Fields is required!</p>
		</div>
		
	<?php
}


function add_styles_and_scripts(){
	
	// CSS Styles
	wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css' );
	wp_enqueue_style( 'font', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300' );
	wp_enqueue_style( 'dashicons',  get_template_directory_uri() . '/dashicons/css/dashicons.min.css' );
	wp_enqueue_style( 'styles',  get_stylesheet_uri(), '', '1.0' );
	wp_enqueue_style( 'responsiveslides',  get_template_directory_uri() . '/css/responsiveslides.css' );
	wp_enqueue_style( 'sideslide',  get_template_directory_uri() . '/css/menu_sideslide.css' );
	
	// Scripts
	wp_enqueue_script( 'tweenmax', get_template_directory_uri() . '/js/TweenMax.min.js', array(), '', true );
	wp_enqueue_script( 'slides', get_template_directory_uri() . '/js/responsiveslides.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'functions', get_template_directory_uri() . '/js/functions.js', array(), '', true );
	
	
	
}
add_action( 'wp_enqueue_scripts', 'add_styles_and_scripts', 1 );

// stijlen inladen voor het login scherm
function custom_login_css(){
	
	wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/css/custom-login.css' );
	
}
add_action( 'login_enqueue_scripts', 'custom_login_css' );

function push_admin_bar(){
	
	// als de admin bar zichtbaar is
	if( is_admin_bar_showing() ){
		// dan moet je deze code uitvoeren
		echo '<style>
				.container, .sticky, .menu-button {
					top : 32px !important;
				}
			</style>';		
		}	

}
add_action( 'wp_head', 'push_admin_bar' );

// declareer je menu locaties
register_nav_menus( array(
	'primary' => 'Hoofdmenu',
	'secundairy' => 'Mobile Menu',
	'third' => 'Footermenu'
) );

// theme support
function tb_custom_logo_setup() {
    $defaults = array(
        'height'      => 53,
        'width'       => 70,
        'flex-height' => false,
        'flex-width'  => false,
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'tb_custom_logo_setup' );

// activeert de featured image bij paginas en posts
add_theme_support('post-thumbnails');

// activeert de excerpt mogelijkheid bij pages
add_post_type_support( 'page', 'excerpt' );

function my_widgets_init() {
    // home blocks
    register_sidebar( array(
        'name' =>  'Home Blocks',
        'id' => 'home_blocks',
        'description' => 'Min 3, max 3 Widgets',
        'before_widget' => '<article class="front text_column">',
		'after_widget'  => '</article>',
		'before_title'  => '<h1>',
		'after_title'   => '</h1>',
    ) );
    
    // de sidebar
    register_sidebar( array(
        'name' =>  'Sidebar',
        'id' => 'sidebar',
        'description' => 'The widgets voor de sidebar',
        'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'my_widgets_init' );


// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'df_disable_comments_post_types_support');

// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);

// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);

// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');

// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');

// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');

// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'df_disable_comments_admin_bar');

function cw_post_type_news() {
$supports = array(
'title', // post title
'editor', // post content
'author', // post author
'thumbnail', // featured images
'excerpt', // post excerpt
'revisions', // post revisions
'custom-fields' // post revisions
);
$labels = array(
'name' => _x('News', 'plural'),
'singular_name' => _x('News', 'singular'),
'menu_name' => _x('News', 'admin menu'),
'name_admin_bar' => _x('News', 'admin bar'),
'add_new' => _x('Add New', 'add new'),
'add_new_item' => __('Add New news'),
'new_item' => __('New News'),
'edit_item' => __('Edit News Item'),
'view_item' => __('View News Item'),
'all_items' => __('All News'),
'search_items' => __('Search news'),
'not_found' => __('No news found.'),
);
$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'news'),
'has_archive' => true,
'hierarchical' => false,
'menu_icon' => 'dashicons-book',
'menu_position' => 5,
);
register_post_type('news', $args);
}
add_action('init', 'cw_post_type_news');


// ACF support for theme
// 1. customize ACF path
add_filter('acf/settings/path', 'my_acf_settings_path');
 
function my_acf_settings_path( $path ) {
 
    // update path
    $path = get_stylesheet_directory() . '/plugins/advanced-custom-fields/';
    
    // return
    return $path;
    
}
 

// 2. customize ACF dir
add_filter('acf/settings/dir', 'my_acf_settings_dir');
 
function my_acf_settings_dir( $dir ) {
 
    // update path
    $dir = get_stylesheet_directory_uri() . '/plugins/advanced-custom-fields/';
    
    // return
    return $dir;
    
}
 

// 3. Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_false');


// 4. Include ACF
include_once( get_stylesheet_directory() . '/plugins/advanced-custom-fields/acf.php' );

?>