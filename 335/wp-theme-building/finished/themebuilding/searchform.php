	<form method="get" action="<?php echo esc_url( home_url('/') ); ?>">
			<div class="input-group">
   			<input type="text" name="s" id="search" placeholder="Zoeken naar..." value="<?php echo get_search_query(); ?>">
            <span class="input-group-btn">
                  <button class="btn" type="submit"><i class="dashicons dashicons-search small"></i></button>
            </span>
		 </div><!-- /input-group -->
	</form>