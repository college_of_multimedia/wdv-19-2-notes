<?php get_header() ?>


<section class="about subpage sidebar text_column" id="about">
	
	
	<div id="content">
		
		<?php the_post(); ?>
		
		<?php //dump($post); ?>
		<div id="post-<?php the_ID() ?>" class="post">
			<h1 class="post-title"><?php the_title() ?></h1>								
			
			<div class="post-content">
				<?php the_post_thumbnail('thumbnail'); ?>
				<?php the_content(); ?>
				
			</div>
			
			<?php if( function_exists( 'the_field' ) &&  the_field('auteur') ){ ?>
			<ul>
				<li>Auteur: <?php the_field('auteur'); ?></li>
				<li>Onderwerp: <?php the_field('onderwerp'); ?></li>
				<li>Locatie: <?php the_field('locatie'); ?></li>
				<li>Google: <?php the_field('map'); ?></li>
			</ul>
			<?php } ?>

			
			<div class="post-meta">Posted on <?php the_time('F j, Y'); ?> <!--in:--> <?php //the_category(', '); ?><!--<span class="sep">|</span><a href="#comments">Jump To Comments</a>--></div>
		</div><!-- .post -->
			
		<?php //comments_template(); ?>		

	</div><!-- #content -->
</section>

<?php get_sidebar() ?>
<?php get_footer() ?>
