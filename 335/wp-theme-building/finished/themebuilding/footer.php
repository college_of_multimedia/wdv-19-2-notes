
	<?php 
	
	if( is_front_page() ){
	
		get_template_part( 'template-parts/content', 'footer' ); 
		
	}	
		
	?>
	
  <!-- Copyrights Section -->
  <div class="copyright">&copy;<?php echo date('Y'); ?> - <strong><?php bloginfo('name'); ?></strong></div>
  </div>
</div><!-- Main Container Ends -->
	
	<?php wp_footer() ?>
	
</body>
</html>