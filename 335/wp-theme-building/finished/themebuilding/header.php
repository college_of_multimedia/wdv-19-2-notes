<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?> </title>
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	<?php
	// dit is een hook, dit is een soort event waar je op kan vasthaken
	wp_head() ?>
	
</head>
<body <?php body_class(); ?>>
<!-- Main Container -->
<div id="container"> 

    <!-- mobile menu -->
   <div class="menu-wrap">
        <nav class="menu">
          
          <?php 
          $defaults = array(
			'menu'            => '',
			'container'       => 'div',
			'container_class' => 'icon-list',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'theme_location'  => 'secundairy',
		);
          
          
          wp_nav_menu( $defaults ); ?>
          
        </nav>
        <button class="close-button" id="close-button">Close Menu</button>
      </div>
       <button class="menu-button" id="open-button">Open Menu</button>
  
  
  <div class="content-wrap">
  <header class="sticky">
   
    <div class="row"> 
	
	<?php 
		
		$custom_logo_id = get_theme_mod( 'custom_logo' );
		$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
		if ( has_custom_logo() ) {
				echo '<h4 class="logo"><a href="'. get_bloginfo('url') .'"><img src="' . esc_url( $logo[0] ) . '" width="70" height="53" id="logo"' . 'alt="' . get_bloginfo( 'name' ) . '"></a></h4>';
		} else {
				echo '<h4 class="logo"><a href="'. get_bloginfo('url') .'">'. get_bloginfo( 'name' ) .'</a></h4>';
		}
	
	?>

    <nav id="main-nav">
      <!-- Desktop Navigation -->
       <?php 
          $defaults = array(
			'menu'            => '',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'theme_location'  => 'primary',
		);
          
          
          wp_nav_menu( $defaults ); ?>
      
      
    </nav>
  </div>
  </header>
  
  <?php if( is_front_page() || is_page_template( array('templates/page-with-slider.php','templates/page-with-slider-no-sidebar.php')) ){ ?>
  
  
  <!-- Hero Section -->
  <section class="hero" id="hero">
  	

  	<?php
  	// add if and check for function exists the field and for banner1 field value, otherwise go to else
  	if( function_exists( 'the_field' ) && the_field('banner_1') ){ ?>
  	<!-- Slideshow 1 -->
  	<ul class="rslides" id="slider1">
  	  <li><img src="<?php the_field('banner_1'); ?>" alt=""></li>
  	  <li><img src="<?php the_field('banner_2'); ?>" alt=""></li>
  	</ul>
  	<?php } else { ?>
		
	<ul class="rslides" id="slider1">
  	  <li><img src="<?php bloginfo('template_url'); ?>/images/banner.jpg" alt=""></li>
  	  <li><img src="<?php bloginfo('template_url'); ?>/images/banner2.jpg" alt=""></li>
  	</ul>
	
	<?php } ?>
  </section>
  
  <?php } ?>
  
  
  