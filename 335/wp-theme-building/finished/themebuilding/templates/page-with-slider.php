<?php 
/*

	Template Name: Page with Slider and Sidebar

*/


get_header() ?>

	<section class="about subpage text_column sidebar" id="about">
		<?php the_post() ?>
		<div id="post-<?php the_ID(); ?>" class="post">
			<h1 class="post-title"><?php the_title(); ?></h1>
			<div class="post-content">
				<?php the_content() ?>
			</div>
		</div><!-- .post -->
	</section>
	
	
	<?php
	// haalt de template file sidebar.php op
	 get_sidebar(); ?>
	
	<?php if( function_exists( 'the_field' ) && the_field('parallax_titel') ){ ?>
	<!-- Parallax Section -->
	<section class="banner" style="<?php 


	if( the_field('background') ){
		echo 'background-image:url('. the_field('background') . ');'; 
		}

	?>">
		<h2 class="parallax"><?php the_field('parallax_titel'); ?></h2>
		<p class="parallax_description"><?php the_field('parallax_content'); ?></p>
	</section>
	<?php } else { ?>
	
		<!-- Parallax Section -->
	<section class="banner">
		<h2 class="parallax">Dummy Title</h2>
		<p class="parallax_description">Content</p>
	</section>
	
	<?php } ?>

<?php get_footer() ?>