<?php get_header() ?>

<section class="about subpage sidebar text_column" id="about">
	
	<h1><?php wp_title(''); ?></h1>
	
	<div id="content">
		
		<?php while ( have_posts() ) : the_post() ?>
		
		<?php //dump($post);
			
			// vraag de url op van de featured image
			$url = get_the_post_thumbnail_url( $post );
			$link = get_permalink( $post );
			//echo $link;
			//echo $url;
			
			$content = get_the_content();
    		$trimmed_content = wp_trim_words( $content, 10, '. Read more...' );
    	?>
		
		
			<div id="post-<?php the_ID() ?>" class="post news" style="background-image:url(<?php echo $url ?>);">
				<a href="<?php echo $link; ?>" class="blocklevel">
				<div class="cover">
				
					<h2 class="post-title"><?php the_title() ?></h2>								
			
					<div class="post-meta">Posted on the date <?php if( function_exists( 'get_field' ) ){the_field('datum');} ?> <!--in:--> <?php //the_category(', '); ?></div>
			
					<div class="post-content">
						<?php echo $trimmed_content; ?>
					</div>
					
				</div>
				</a>
			</div><!-- .post -->
		
		<?php endwhile ?>

		<div class="navigation">
			<div class="navleft"><?php next_posts_link('&laquo; Older Posts', '0') ?></div>
			<div class="navright"><?php previous_posts_link('Newer Posts &raquo;', '0') ?></div>
		</div>

	</div><!-- #content -->
</section>
<?php get_sidebar() ?>
<?php get_footer() ?>