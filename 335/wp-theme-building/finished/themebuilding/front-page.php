<?php get_header() ?>
	
  <!-- About Section -->
  <section class="about clearfix" id="about">
    <h2 class="hidden">About</h2>
	
	
	<?php
		
		// zijn er al widgets in de home blocks gezet?
		if( is_active_sidebar( 'home_blocks' ) ){
			// zo ja, plaats de widgets
			dynamic_sidebar('home_blocks');
			
		} else {
			// als de widget area leeg is
			echo '<article class="front">
		<h1>Widget Title</h1>
    	<p class="text_column"><span class="dashicons dashicons-groups"></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
	</article>
	<article class="front">
		<h1>Widget Title</h1>
    	<p class="text_column"><span class="dashicons dashicons-businessman"></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
	</article>
	<article class="front">
		<h1>Widget Title</h1>
    	<p class="text_column"><span class="dashicons dashicons-lightbulb"></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
	</article>';
		
		}
	
	?>
	
	
	
  </section>
  <!-- Stats Gallery Section -->
  <div class="gallery">
    <div class="thumbnail">
      <h1 class="stats">1500</h1>
      <h4>TITLE</h4>
      <p>One line description</p>
    </div>
    <div class="thumbnail">
      <h1 class="stats">2300</h1>
      <h4>TITLE</h4>
      <p>One line description</p>
    </div>
    <div class="thumbnail">
      <h1 class="stats">7500</h1>
      <h4>TITLE</h4>
      <p>One line description</p>
    </div>
    <div class="thumbnail">
      <h1 class="stats">9870</h1>
      <h4>TITLE</h4>
      <p>One line description</p>
    </div>
  </div>
  <!-- Parallax Section -->
  <section class="banner">
  	<?php the_post() ?>
    <h2 class="parallax"><?php the_title(); ?></h2>
    <p class="parallax_description"><?php the_content() ?></p>
  </section>

  <!-- More Info Section -->
  <section id="more-info">
    <article class="footer_column">
      
      <?php 
      	
      	$veld = 123562;
      	
      	if( function_exists( 'get_field' ) && get_field('blok_links') ){
      		$veld = get_field('blok_links'); // the_field();
      	}
      	
      	$get_content = get_post( $veld );
      	
      	//dump( $get_content );
      	
      	if( $get_content ){
      		// plaats de titel van de post 
      		echo '<h3>' . $get_content->post_title . '</h3>';
			
			// haal de url van de featured image op van deze post en zet die in een img tag
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $get_content->ID ), 'single-post-thumbnail' );
			echo '<img src="' . $image[0] . '" alt="" width="' . $image[1] . '" height="' . $image[2] . '" class="cards">';
			
			// zet de excerpt neer van de post
			if( $get_content->post_excerpt ){
				
				$trim = $get_content->post_excerpt;
				$trimmed = wp_trim_words( $trim, 50, '...' );
				echo '<p>' . $trimmed . '</p>';
				
			} else {
				echo '<p>Gebruik de post excerpt voor de content van deze text!</p>';
			}
			
			$link = get_permalink( $get_content );
			
			echo '<a href="' . $link . '"><div class="button blue">learn more</div></a>';
			  		
      	} else {
      		
      		//$url = get_bloginfo('template_url');
      		
      		echo '<h3>Dummy titel</h3>
      		<img src="https://via.placeholder.com/400x200" alt="" width="400" height="200" class="cards"/>
      		<p>Ga naar de homepage van je website en kies de pagina die je op deze plek wilt tonen. Gebruik de excerpt voor de content van dit blokje en vergeet niet een featured image voor de pagina te gebruiken!</p>
	    	<a href="#"><div class="button blue">learn more</div></a>';
      		
      	}
      
      ?>
      
    </article>
    <article class="footer_column">
      <?php 
      	
      	$veld = 123432;
      	
      	if( function_exists( 'get_field' ) && get_field('blok_rechts') ){
      		$veld = get_field('blok_rechts');
      	}
      	
      	$get_content = get_post( $veld );
      	
      	//dump( $get_content );
      	
      	if( $get_content ){
      		// plaats de titel van de post 
      		echo '<h3>' . $get_content->post_title . '</h3>';
			
			// haal de url van de featured image op van deze post en zet die in een img tag
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $get_content->ID ), 'single-post-thumbnail' );
			echo '<img src="' . $image[0] . '" alt="" width="' . $image[1] . '" height="' . $image[2] . '" class="cards">';
			
			// zet de excerpt neer van de post
			if( $get_content->post_excerpt ){
				echo '<p>' . $get_content->post_excerpt . '</p>';
			} else {
				echo '<p>Gebruik de post excerpt voor de content van deze text!</p>';
			}
			
			$link = get_permalink( $get_content );
			
			echo '<a href="' . $link . '"><div class="button blue">learn more</div></a>';
			  		
      	} else {
      		
      		//$url = get_bloginfo('template_url');
      		
      		echo '<h3>Dummy titel</h3>
      		<img src="https://via.placeholder.com/400x200" alt="" width="400" height="200" class="cards"/>
      		<p>Ga naar de homepage van je website en kies de pagina die je op deze plek wilt tonen. Gebruik de excerpt voor de content van dit blokje en vergeet niet een featured image voor de pagina te gebruiken!</p>
	    	<a href="#"><div class="button blue">learn more</div></a>';
      		
      	}
      
      ?>
    </article>
  </section>
	
<?php get_footer() ?>