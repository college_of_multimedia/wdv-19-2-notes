

// initialise slideshow
jQuery(function () {
	
  // Slideshow 1
  jQuery("#slider1").responsiveSlides({
	maxwidth: 1200,
	speed: 800
  }); 
});

TweenMax.from('.container header', 0.8, {left:100,opacity:0});
TweenMax.from('#hero', 0.5, {top:-200,opacity:0,ease:Elastic.easeInOut});
TweenMax.from('#main-nav', 1, {opacity:0,delay:0.5});
TweenMax.staggerFrom('.text_column', 0.6, {opacity:0,top:50}, 0.3);
TweenMax.staggerFrom('.dashicons', 0.5, {opacity:0,bottom:50,delay:0.4,ease:Elastic.easeOut}, 0.3);
TweenMax.staggerFrom('.gallery .thumbnail', 0.5, {opacity:0,top:70,delay:1.2,ease:Elastic.easeOut}, 0.3);
TweenMax.to('.button', 0.1, {opacity:1,top:0,ease:Elastic.easeOut});

// logo animation
var logo = document.getElementById("logo");
if(logo){
  logo.onmouseover = function(){
  	TweenMax.to(logo, 0.4, {width:'+=6', height:'+=6',ease:Elastic.easeOut});
  };
  logo.onmouseout = function(){
  	TweenMax.to(logo, 0.4, {width:'-=6', height:'-=6',ease:Elastic.easeOut});
  };
}
/**
 * main.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2014, Codrops
 * http://www.codrops.com
 */
(function() {

	var bodyEl = document.body,
		content = document.querySelector( '.content-wrap' ),
		openbtn = document.getElementById( 'open-button' ),
		closebtn = document.getElementById( 'close-button' ),
		isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		openbtn.addEventListener( 'click', toggleMenu );
		if( closebtn ) {
			closebtn.addEventListener( 'click', toggleMenu );
		}

		// close the menu element if the target it´s not the menu element or one of its descendants..
		content.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( isOpen && target !== openbtn ) {
				toggleMenu();
			}
		} );
	}

	function toggleMenu() {
		if( isOpen ) {
			classie.remove( bodyEl, 'show-menu' );
		}
		else {
			classie.add( bodyEl, 'show-menu' );
		}
		isOpen = !isOpen;
	}

	init();

})();

/*!
 * classie - class helper functions
 * from bonzo https://github.com/ded/bonzo
 * 
 * classie.has( elem, 'my-class' ) -> true/false
 * classie.add( elem, 'my-new-class' )
 * classie.remove( elem, 'my-unwanted-class' )
 * classie.toggle( elem, 'my-class' )
 */

/*jshint browser: true, strict: true, undef: true */
/*global define: false */

( function( window ) {

'use strict';

// class helper functions from bonzo https://github.com/ded/bonzo

function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}

// classList support for class management
// altho to be fair, the api sucks because it won't accept multiple classes at once
var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

var classie = {
  // full names
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  // short names
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

// transport
if ( typeof define === 'function' && define.amd ) {
  // AMD
  define( classie );
} else {
  // browser global
  window.classie = classie;
}

})( window );
