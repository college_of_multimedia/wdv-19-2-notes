<?php
$debug = true;
ini_set('display_errors', $debug);
error_reporting(E_ALL);
// error_reporting(E_ALL & ~E_NOTICE); // alles behalve de notices
$nl = "\n";
$br = '<br>' . $nl;
$hr = '<hr>' . $nl;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?php

// http://manual.phpdoc.org/

/**
 * multiply vermenigvuldigt 2 getalen met elkaar 
 * en geeft het antwoord terug
 * @param  integer $a een getal, default 0
 * @param  integer $b een getal, default 0
 * @return integer 
 */
function multiply($a = 0, $b = 0 ){
	return $a * $b;
}

$uitkomst = multiply(5,4);
echo $uitkomst . $br;
$uitkomst = multiply(5,5);
echo $uitkomst. $br;


/**
 * multiplyS vermenigvuldigt 2 getallen 
 * en stuurt een nette string terug met de uitkomst
 * @param  float  $a
 * @param  float  $b 
 * @return string
 */
function multiplyS($a, $b){
	$string = "$a x $b = ";
	$string .= $a * $b;
	$string .= "<br>\n";
	return $string;
}


/**
 * laatTafelZien roept 10 keer de functie multiplyS op 
 * om een nette tafel te kunen bouwen
 * @param  integer $tafel welke tafel willen we zien
 * @return string
 */
function laatTafelZien( $tafel = 1 ){
	$html = '';
	for ($i = 1; $i <= 10; $i++){
		$html .= multiplyS($i, $tafel);
	}
	return $html;
}

echo laatTafelZien(5) . $hr;
echo laatTafelZien(9) . $hr;
echo laatTafelZien(88) . $hr;

/**
 * [multiplyS description]
 * @param  int    $a [description]
 * @param  int    $b [description]
 * @return [type]    [description]
 */
function multiplyS2(int $a, int $b){
	global $nl;
	$string = "$a x $b = ";
	$string .= $a * $b;
	$string .= $GLOBALS['br'];
	$GLOBALS['uitkomst'] = $string;
}
// echo "<pre>";
// print_r($_GET);


$myArray = ['aap', 'noot', 'mies'];

function addTeun(&$array){
	$array[] = 'teun';
	// print_r($array);
}

// function bla($arg) dan is $arg de VALUE die je meegeeft en niet de variabel zelf
// oftwel een LOKALE variabel met de zelfde waarde als de global  
// function bla(&$arg) dan is $arg de variabel zelf
// oftwel de globale variabel  (pass by reference)

addTeun($myArray);

print_r($myArray);




?>
</body>
</html>
