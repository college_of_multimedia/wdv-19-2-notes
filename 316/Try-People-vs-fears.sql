USE CMM_test;

SHOW TABLES;
SELECT Name from people;

+------------------------+
| Name                   |
+------------------------+
| John Johnson           |
| Jane Jones             |
| Aloysius Snuffleupagus |
| ik                     |
+------------------------+


SELECT Name, fear from people, fears; 
#// het gaat mis alles koppelt aan alles

SELECT Name as Naam from people as p; 
#// met as maak je een Alias

SELECT p.Name, f.Fear 
FROM people as p, person_fear as pf, fears as f
WHERE p.personID = pf.personID 
AND pf.fearID = f.fearID
ORDER BY p.Name, f.Fear; 

+------------------------+-------------+
| Name                   | Fear        |
+------------------------+-------------+
| Aloysius Snuffleupagus | Black Cats  |
| Aloysius Snuffleupagus | Friday 13th |
| Aloysius Snuffleupagus | Peanut      |
| ik                     | Black Cats  |
| Jane Jones             | Flying      |
| Jane Jones             | Friday 13th |
| Jane Jones             | Peanut      |
| John Johnson           | Black Cats  |
| John Johnson           | Flying      |
| John Johnson           | Friday 13th |
+------------------------+-------------+

SELECT p.Name as Naam, COUNT(p.Name) as Angsten
FROM people as p, person_fear as pf, fears as f
WHERE p.personID = pf.personID 
AND pf.fearID = f.fearID
GROUP BY p.Name
ORDER BY p.Name; 

+------------------------+---------+
| Naam                   | Angsten |
+------------------------+---------+
| Aloysius Snuffleupagus |       3 |
| ik                     |       1 |
| Jane Jones             |       3 |
| John Johnson           |       3 |
+------------------------+---------+

SELECT f.Fear as Angst, COUNT(f.Fear) as Aantal
FROM people as p, person_fear as pf, fears as f
WHERE p.personID = pf.personID 
AND pf.fearID = f.fearID
GROUP BY Angst
ORDER BY Aantal DESC
LIMIT 3; 

+-------------+--------+
| Angst       | Aantal |
+-------------+--------+
| Black Cats  |      3 |
| Friday 13th |      3 |
| Flying      |      2 |
+-------------+--------+

##// Sneller omdat er nu maar 2 tabellen worden gekoppeld

SELECT f.Fear as Angst, COUNT(f.Fear) as Aantal
FROM person_fear as pf, fears as f
WHERE pf.fearID = f.fearID
GROUP BY Angst
ORDER BY Aantal DESC
LIMIT 3; 


SELECT Name, fearID
FROM people
LEFT JOIN person_fear 
ON mijnDB.postcodepersonID = person_fear.personID;

SELECT Name, fearID
FROM people
NATURAL JOIN person_fear;

+------------------------+--------+
| Name                   | fearID |
+------------------------+--------+
| John Johnson           |      1 |
| John Johnson           |      2 |
| John Johnson           |      5 |
| Jane Jones             |      2 |
| Jane Jones             |      3 |
| Jane Jones             |      5 |
| Aloysius Snuffleupagus |      1 |
| ik                     |      1 |
| Aloysius Snuffleupagus |      2 |
| Aloysius Snuffleupagus |      3 |
+------------------------+--------+

INSERT INTO `fears` (`Fear`) VALUES
('Examns'),
('Clowns');


INSERT INTO `people` (`Name`, `Sofi`) VALUES
('Boris Belts', '545-456-789'),
('Mister T', '987-555-123'),
('Tony Starch', '564-789-555');



INSERT INTO `person_fear` (`PersonID`, `FearID`) VALUES
(6, 1),
(5, 6),
(5, 2),
(5, 5);

SELECT Name, fearID FROM people RIGHT JOIN person_fear  ON people.personID = person_fear.personID;
+------------------------+--------+
| Name                   | fearID |
+------------------------+--------+
| John Johnson           |      1 |
| John Johnson           |      2 |
| John Johnson           |      5 |
| Jane Jones             |      2 |
| Jane Jones             |      3 |
| Jane Jones             |      5 |
| Aloysius Snuffleupagus |      1 |
| ik                     |      1 |
| Aloysius Snuffleupagus |      2 |
| Aloysius Snuffleupagus |      3 |
| Mister T               |      1 |
| Boris Belts            |      6 |
| Boris Belts            |      2 |
| Boris Belts            |      5 |
+------------------------+--------+

SELECT Name, fearID FROM people LEFT JOIN person_fear  ON people.personID = person_fear.personID;
+------------------------+--------+
| Name                   | fearID |
+------------------------+--------+
| John Johnson           |      1 |
| John Johnson           |      2 |
| John Johnson           |      5 |
| Jane Jones             |      2 |
| Jane Jones             |      3 |
| Jane Jones             |      5 |
| Aloysius Snuffleupagus |      1 |
| ik                     |      1 |
| Aloysius Snuffleupagus |      2 |
| Aloysius Snuffleupagus |      3 |
| Mister T               |      1 |
| Boris Belts            |      6 |
| Boris Belts            |      2 |
| Boris Belts            |      5 |
| Tony Starch            |   NULL |
+------------------------+--------+


SELECT f.Fear as Angst, COUNT(f.Fear) as Aantal 
FROM person_fear as pf
LEFT JOIN Fears as f
ON pf.fearID = f.fearID GROUP BY Angst ORDER BY Aantal ASC LIMIT 3;

