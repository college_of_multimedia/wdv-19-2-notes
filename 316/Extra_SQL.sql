-- Useful Link:
-- https://www.w3schools.com/sql/default.asp


-- #############################################
-- ### Jokers of Wildcard karakters in MySQL ### 
-- #############################################

/*
Symbool:		%
Description:	Representeert nul of meer karakters
Example:		bl% vindt bl, black, blue, en blob

Symbool:		_
Description:	Representeert één enkel karakter
Example:		h_t vindt hot, hat, en hit

Symbool:		[]
Description:	Representeert één enkel karakter in de set 
Example:		h[oa]t vindt hot en hat, maar niet hit

Symbool:		^
Description:	Representeert elk karakter NIET in de set
Example:		h[^oa]t vindt hit, maar niet hot en hat

Symbool:		-
Description:	Representeert een bereik van karakters in de set
Example:		c[a-c]t vindt cat, cbt en cct 

Deze kunnen ook gecombineerd worden:
*/

WHERE naam LIKE 'T_%_%'	
-- vindt elke waarde die start met "T" en minstens 3 karakters lang is
-- bijv Toyota en Tesla
  
WHERE naam LIKE 'T%a'	
-- vindt elke waarde die start met "T" en eindigt op "a"
-- bijv Toyota en Tesla

WHERE naam LIKE 'T%a' AND NOT naam LIKE '%l%';
-- vindt elke waarde die start met "T" en eindigt op "a", maar niet de waardes met een "l"
-- bijv Toyota


-- #########################
-- ### Tellen, Joins etc ### 
-- #########################

-- even 2 autos erbij met wisselende prijzen om het wat leuker te maken
INSERT INTO `autos` (`merk_id`, `titel`, `type`, `kleur`, `brandstof`, `zitplaatsen`, `prijs`) VALUES 
(2, 'Polo', 'GTX', 'Blauw', 'Benzine', 4 , 1500),
(2, 'Polo', 'Luxe', 'Goud', 'Benzine', 2 , 125000);

-- toon alle titels in de table
SELECT `titel` FROM `autos`; 

-- toon alleen de verschillende (unieke) titels in de table
SELECT DISTINCT `titel` FROM `autos`; 

-- tel het aantal autos (records) in de table
SELECT COUNT(*) as 'Aantal autos' FROM `autos`;

-- tel het aantal verschillende (unieke) titels in de table
SELECT COUNT(DISTINCT `titel`) FROM `autos`

-- tel het aantal zitplaatsen in de table bij elkaar
SELECT SUM(autos.zitplaatsen) AS 'Aantal zitplaatsen' FROM `autos`;

-- toon de hoogste prijs in de table
SELECT MAX(`prijs`) AS HoogstePrijs FROM `autos`;

-- toon de laagste prijs in de table, met kolomnaam met spaties
SELECT MIN(`prijs`) AS "Goedkoopste Prijs" FROM `autos`;

-- toon alle duurste titels en prijzen in de table
SELECT `titel`, MAX(`prijs`) FROM `autos` GROUP BY `titel`;

-- toon alle goedkoopste titels en prijzen in de table
SELECT `titel`, MIN(`prijs`) FROM `autos` GROUP BY `titel`;

-- toon alleen de duurste titel en prijs in de table (de 1ste van de duurste)
SELECT `titel`, `prijs` FROM `autos` ORDER BY `prijs` DESC LIMIT 0,1;

-- toon alleen de goedkoopste titel en prijs in de table (de 1ste van de goedkoopste)
SELECT `titel`, `prijs` FROM `autos` ORDER BY `prijs` ASC LIMIT 0,1;

-- samenvoegen van velden ( CONCAT ) en ALIAS ( AS ) 
SELECT CONCAT(m.naam,' ', a.titel) AS Auto, a.kleur AS Kleur 
FROM `autos` AS a 
LEFT JOIN `merken` AS m 
ON a.merk_id = m.id 
ORDER BY Auto;

/*
JOIN 		 == INNER JOIN
LEFT JOIN 	 == LEFT OUTER JOIN
RIGHT JOIN 	 == RIGHT OUTER JOIN
FULL JOIN 	 == FULL OUTER JOIN

NATURAL JOIN == INNER, LEFT of RIGHT JOIN met USING() keyword

Meestal gebruiken we echter ON keyword
*/


-- stel, in merken heet de primary `merk_id` ipv `id` dan zou dit werken:
SELECT `naam`, `titel`, `kleur` FROM `autos` NATURAL JOIN `merken`;
-- of dit, je geeft aan dmv USING welke kolom zou moeten matchen:
SELECT `naam`, `titel`, `kleur` FROM `autos` INNER JOIN `merken` USING(`merk_id`);

-- maar in onze tabel merken heet de primary `id` en dan is er een ON nodig
SELECT `naam`, `titel`, `kleur` FROM `autos` INNER JOIN `merken`
ON autos.merk_id = merken.id;
+------------+---------+-------+
| naam       | titel   | kleur |
+------------+---------+-------+
| Ford       | Mustang | Rood  |
| Ford       | Focus   | Blauw |
| Volkswagen | Polo    | Geel  |
| Renault    | Clio    | Rood  |
| Peugeot    | 307     | Grijs |
| Tesla      | Model-T | Rood  |
| Volkswagen | Polo    | Blauw |
| Volkswagen | Polo    | Goud  |
+------------+---------+-------+
8 rows in set (0.00 sec) -- de beste oplossing 
-- als je alle autos die ook een merk hebben wilt


-- test ook met de LEFT JOIN
SELECT `naam`, `titel`, `kleur` FROM `autos` LEFT JOIN `merken`
ON autos.merk_id = merken.id;
+------------+-------------+--------+
| naam       | titel       | kleur  |
+------------+-------------+--------+
| Ford       | Mustang     | Rood   |
| Ford       | Focus       | Blauw  |
| Volkswagen | Polo        | Geel   |
| Renault    | Clio        | Rood   |
| Peugeot    | 307         | Grijs  |
| Tesla      | Model-T     | Rood   |
| Volkswagen | Polo        | Blauw  |
| Volkswagen | Polo        | Goud   |
| NULL       | Winkelwagen | Zilver |
+------------+-------------+--------+
9 rows in set (0.00 sec) -- Links zijn de merken, maar de Winkelwagen heeft geen merk

-- test ook met de RIGHT JOIN
SELECT `naam`, `titel`, `kleur` FROM `autos` RIGHT JOIN `merken`
ON autos.merk_id = merken.id;
+------------+---------+-------+
| naam       | titel   | kleur |
+------------+---------+-------+
| Ford       | Mustang | Rood  |
| Ford       | Focus   | Blauw |
| Volkswagen | Polo    | Geel  |
| Renault    | Clio    | Rood  |
| Peugeot    | 307     | Grijs |
| Tesla      | Model-T | Rood  |
| Volkswagen | Polo    | Blauw |
| Volkswagen | Polo    | Goud  |
| Toyota     | NULL    | NULL  |
+------------+---------+-------+
9 rows in set (0.00 sec) -- Rechts zijn de merken, maar Toyota heeft geen auto

-- test ook met de FULL JOIN (Let op hier geen ON !!)
SELECT `naam`, `titel`, `kleur` FROM `autos` FULL JOIN `merken`;
+------------+-------------+--------+
| naam       | titel       | kleur  |
+------------+-------------+--------+
| Ford       | Mustang     | Rood   |
| Volkswagen | Mustang     | Rood   |
// etc....
| Peugeot    | Winkelwagen | Zilver |
| Toyota     | Winkelwagen | Zilver |
| Tesla      | Winkelwagen | Zilver |
+------------+-------------+--------+
54 rows in set (0.01 sec) -- 9 x 6 records, de relatie is B#llsh!t !!!




