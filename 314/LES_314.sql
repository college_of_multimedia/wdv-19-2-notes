CREATE DATABASE IF NOT EXISTS `cmm_people`;
# BLA BLA

USE cmm_people;

CREATE TABLE `people` (
	userID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	email VARCHAR(50),
	pass CHAR(32) NOT NULL,
	reg_date DATETIME NOT NULL,
	reg_date_ts TIMESTAMP NOT NULL,
	PRIMARY KEY (userID)
);

INSERT INTO `people` 
	VALUES (NULL, 'me', 'me@home.nl', md5('worst'), NOW(), now() );

INSERT INTO `people` (name, email, pass, reg_date) VALUES
	('ik', 'ik@home.nl', md5('ik'), NOW() ),
	('jij', 'jij@home.nl', md5('jij'), NOW() ),
	('hij', 'hij@home.nl', md5('hij'), NOW() ),
	('wij', 'wij@home.nl', md5('wij'), NOW() ),
	('ons', 'ons@home.nl', md5('ons'), NOW() ),
	('ook ik', 'ook_ik@home.nl', md5('ook ik'), NOW() );


SELECT * FROM people;

SELECT userID, name, email FROM people;

SELECT userID, name, email FROM people WHERE userID = 1;

SELECT userID, name, email FROM people WHERE name = 'ik';

SELECT userID, name, email FROM people WHERE name LIKE '%ij%';

SELECT userID, name, email FROM people WHERE name LIKE '%ij%' LIMIT 2;

SELECT userID, name, email FROM people WHERE name LIKE '%ij%' OR userID = 1 LIMIT 2;

SELECT userID, name, email FROM people WHERE name LIKE '%ij%' AND userID = 3 LIMIT 2;

SELECT userID, name, email FROM people WHERE email LIKE '%home.nl' LIMIT 5,5;

SELECT userID, name, email FROM people WHERE email LIKE '%home.nl' LIMIT 4,2;

