<?php
// Dit bestand stelt een REST API voor in de root van de server
// in het echt zou deze gegevens met de database uitwisslen
// de response is via JSON

// voorbeeld POST http://192.168.33.34/handle_rest.php/News/123

$method = $_SERVER['REQUEST_METHOD'];
// POST / GET / DELETE / PUT
$response['method'] =  $method;


$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));
// News/123 --> ['News','123']
$response['request'] = $request;

$response['succes'] = true;

switch ($method) {
  case 'PUT':
    //do_something_with_put($request);  
    break;
  case 'POST':
    $data = $_POST; 
    $response['data'] = $data;
    //do_something_with_post($request);  
    break;
  case 'GET':
    $data = $_GET; 
    $response['data'] = $data;
    //do_something_with_get($request);  
    break;  
  case 'DELETE':
    //do_something_with_delete($request);  
    break;
  default:
    $response['succes'] = false;
    //handle_error($request);  
    break;
}

// return the response as JSON
header("Content-Type:application/json");
// echo 'Response: ' . json_encode($response);
echo json_encode($response);