<?php
/**
 * voorbeeld API
 * 
 * Voor complete lijst
 * action=get_app_list
 *
 * Voor enkel item (app)
 * action=get_app&id=1
 * 
 */

/**
 * Haal een app op door middel van een id;
 * @param  string $id 
 * @return array     
 */
function get_app_by_id ($id) {
	// in het echt komt deze uit de database

	$app_info = [];

	switch ($id) {
		case 1:
			$app_info = ['app_name' => 'Web Demo', 'app_price' => 'free', 'app_version' => '2.0'];
			break;
		case 2:
			$app_info = ['app_name' => 'Audio Countdown', 'app_price' => 'free', 'app_version' => '1.1'];
			break;
		case 3:
			$app_info = ['app_name' => 'The Tab Key', 'app_price' => '12.45', 'app_version' => '3.2'];
			break;
		case 5:
			$app_info = ['app_name' => 'Music Sleep Timer', 'app_price' => 'free', 'app_version' => '1.9'];
			break;
	}
	return $app_info;
}

function get_app_list () {
	$app_list = [
		['id'=> 1, 'name' => 'Web Demo'],
		['id'=> 2, 'name' => 'Audio Countdown'],
		['id'=> 3, 'name' => 'The Tab Key'],
		['id'=> 5, 'name' => 'Music Sleep Timer'],
	];
	return $app_list;
}

$possible_url = ['get_app','get_app_list'];
$value = 'An error has ocurred!<br>';

/*
 * is de call correct uitgevoerd?
 */
if ( ! isset($_GET['action']) || ! in_array($_GET['action'], $possible_url) ) {
	echo $value;
	echo 'Try: <a href="api.php?action=get_app_list">action=get_app_list</a><br>';
	echo 'Or: <a href="api.php?action=get_app&id=1">action=get_app&id=1</a><br>';
	die();
}

switch ($_GET['action']) {
	case 'get_app_list':
		$value = get_app_list();
		break;
	case 'get_app':
		if (!isset($_GET['id'])){
			$value = 'missing argument';
			break;
		}
		$value = get_app_by_id($_GET['id']);
		break;
}

echo json_encode($value);







