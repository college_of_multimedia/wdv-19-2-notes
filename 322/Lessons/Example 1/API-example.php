<?php

/**
 * Maak een berichten array
 */
$berichten = [
    [
        'title'     => 'vandaag is het zaterdag',
        'content'   => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ullamcorper leo mauris, non tincidunt nibh dignissim nec. Nunc elit risus, tempor ac est vitae, eleifend finibus est. Praesent convallis molestie odio, id suscipit lectus rhoncus ut. Maecenas auctor sapien magna, vel elementum mi molestie sit amet. Mauris venenatis massa pharetra urna dapibus, non finibus lacus feugiat. Pellentesque porta ultrices volutpat. Duis efficitur tortor sit amet urna bibendum, in ultrices ante imperdiet. Mauris nec nibh non arcu ornare efficitur vel non ex. Cras lectus quam, eleifend sagittis iaculis vitae, congue a est. Mauris turpis neque, maximus nec accumsan ut, vestibulum id magna. Quisque sagittis lorem odio, quis interdum nibh gravida a. Proin nec viverra magna. Praesent consectetur convallis orci id faucibus.',
        'datum'     => '2018-01-05 09:55',
        'published' => true,
    ],
    [
        'title'     => 'Duis elementum congue risus',
        'content'   => 'Duis elementum congue risus. Nulla non convallis est, non rutrum magna. Aenean maximus faucibus sapien a luctus. Suspendisse nisl felis, consectetur ut feugiat in, porttitor vitae nisl. Ut in tempor nisi. Vestibulum placerat blandit eros, eget suscipit augue placerat nec. Morbi lobortis posuere elit eget commodo. Aliquam quis magna imperdiet, accumsan massa ac, lobortis urna.',
        'datum'     => '2018-01-04 09:55',
        'published' => false,
    ],
];
