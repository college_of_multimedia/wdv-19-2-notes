<?php

/*
// simpel voorbeeld van de API
// In dit geval laat ik wat nieuws berichten zien.
$news = array();
$news[] = array( 1, 'Titel 1');
$news[] = array( 9, 'nog een titel');

echo json_encode( $news );
*/


// Hier maak ik de settings voor dit project
// op deze server

if ('www.deslager.test' == $_SERVER['SERVER_NAME']) {
    // deslager.test via Scotch-box
    define('DB_HOST', '127.0.0.1');
    define('DB_USER', 'root');
    define('DB_PASS', 'root');
    define('DATABASE', 'cmm_wd_api_news');

} else {
    // test omgeving cmm-students.localhost via Homestead
    define('DB_HOST', 'localhost');
    define('DB_USER', 'homestead');
    define('DB_PASS', 'secret');
    define('DATABASE', 'cmm_wd_api_news');
}


/**
 * Class newsClass
 *
 * Deze nieuws class zal alle functionaliteit van de nieuwsberichten regelen
 * hier handel ik alle database dingen af
 * deze class geeft een nieuwsbericht array terug
 */
class newsClass
{

    private $_all_news_item_array;    // Alle nieuws berichten
    private $_mysqli;                // Het mysqli object


    /**
     * De __construct class word altijd aangeroepen ( en uitgevoerd ) bij het aanmaken van een class
     * zoals in dit geval 'new newsClass()'
     */
    function __construct()
    {

        // maak een database verbinding en stop als het mislukt
        $this->_mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DATABASE);

        // controleer of het gelukt is om een verbinding met de database te maken
        if ($this->_mysqli->connect_errno) {
            throw new Exception('Failed to connect to MySQL: (' . $this->_mysqli->connect_errno . ') ' . $this->_mysqli->connect_error);
        };
    }


    /**
     * Haal ALLE nieuws berichten op uit de database
     * plaats deze de propperty: _all_news_item_array
     *
     * @param string $sort
     *
     * @return mixed
     * @throws Exception
     */
    public function getAllNewsItems($sort = 'id')
    {
        // Maak de query aan
        $query = 'SELECT id, title, post_url FROM posts ORDER BY ' . $sort;

        // Haal de nieuwsberichten uit de Database
        if ( ! $prepare = $this->_mysqli->prepare($query)) {
            throw new Exception('Prepare is mislukt: (' . $this->_mysqli->errno . ') ' . $this->_mysqli->error);
        }

        // execute query
        if ( ! $prepare->execute()) {
            throw new Exception('Execute failed: (' . $this->_mysqli->errno . ') ' . $this->_mysqli ->error);
        }

        // haal de gegevens op uit het mysqli object
        $result = $prepare->get_result();

        // controleer of er een nieuws bericht is gevonden
        if (empty($result->num_rows)) {
            die('Geen berichten gevonden');
        }

        // Sla de nieuws berichten op in dit object
        while ($newsItem = $result->fetch_assoc()) {
            $this->_all_news_item_array[] = $newsItem;
        }

        return $this->_all_news_item_array;

    }

    /**
     * Haal 1 bericht op op basis van het ID
     * 
     * @param $id
     *
     * @return mixed
     * @throws \Exception
     */
    public function getNewsItem($id)
    {
        // Maak de query aan
        $query = 'SELECT id, title, post_url FROM posts WHERE id=?';

        // Haal de nieuwsberichten uit de Database
        if ( ! $prepare = $this->_mysqli->prepare($query)) {
            throw new Exception('Prepare is mislukt: (' . $this->_mysqli->errno . ') ' . $this->_mysqli->error);
        }

        // Koppel het ID
        $prepare->bind_param('i', $id);

        // execute query
        if ( ! $prepare->execute()) {
            throw new Exception('Execute failed: (' . $this->_mysqli->errno . ') ' . $this->_mysqli ->error);
        }

        // haal de gegevens op uit het mysqli object
        $result = $prepare->get_result();

        // controleer of er een nieuws bericht is gevonden
        if (empty($result->num_rows)) {
            die('Geen berichten gevonden');
        }

        // Sla de nieuws berichten op in dit object
        while ($newsItem = $result->fetch_assoc()) {
            $this->_all_news_item_array[] = $newsItem;
        }

        return $this->_all_news_item_array;
    }

}


$possible_url = ['get_news_list', 'get_news'];
/*
 * Als er geen action meegegeven is dan haal ik alle berichten op
 */
$action = 'get_news_list';
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}


// Maak het news object aan
$newsObject = new newsClass();


// Bekijk wat er gedaan moet worden
switch ($action) {
    case 'get_news_list':
        $order = (isset($_GET['order'])) ? $_GET['order'] : 'publish_date';
        $value = $newsObject->getAllNewsItems($order);
        break;
    case 'get_news':
        if ( ! isset($_GET['id'])) {
            $value = 'Missing argument';
            break;
        }
        $value = $newsObject->getNewsItem($_GET['id']);
        break;
}

//return JSON array
header('Content-Type: application/json');
echo json_encode($value);
exit();

// http://www.deslager.test/cmm_wd_api_news.php
// http://www.deslager.test/cmm_wd_api_news.php?action=get_news&id=3
// http://www.deslager.test/cmm_wd_api_news.php?action=get_news_list&order=titel
