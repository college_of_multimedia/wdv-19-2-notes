<h1>Maak een API om nieuwsberichten op te halen</h1>
Het moet mogelijk zijn om alle berichten op te vragen of 1 bericht op basis van het ID van het bericht
<h2>Opgave 1</h2>
<ul>
    <li>Maak php pagina: news.php.</li>
    <li>Maak een array met minimaal 3 nieuwsberichten.</li>
    <li>Zorg dat elk bericht een key heeft, dit gebruiken wij als id</li>
</ul>

<h2>Opgave 2</h2>
<ul>
    <li>Als er geen parameter verzonden is wil ik alle berichten weergeven</li>
    <li>Geef de berichten weer volgens de JSON structuur uit opgave 1</li>
    <li>Voorbeeld call: http://localhost/news.php</li>
</ul>
<h2>Opgave 3</h2>
<ul>
    <li>Als de parameter id is meegegeven dan wil ik alleen het bericht zien met deze key</li>
    <li>Mocht een bericht niet bestaan dan moet er een foutmelding verschijnen</li>
    <li>Voorbeeld call: http://localhost/news.php?id=2</li>
</ul>
