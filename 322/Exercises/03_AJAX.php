<h1>Dynamisch content laden.</h1>
In een simpele html pagina moet de inhoud van een div vervangen worden d.m.v. een AJAX call
<h2>Opgave 1</h2>
<ul>
    <li>Maak php pagina: nieuwsbericht.php.</li>
    <li>Zorg dat op deze pagina een HTML div element staat.</li>
    <li>Deze div moet de class 'nieuwsbericht' hebben</li>
    <li>Binnen deze div moeten de volgende elementen komen: titel, content en id</li>
</ul>

<h2>Opgave 2</h2>
<ul>
    <li>Maak een AJAX call om bericht 1 uit opgave 2 op te halen:</li>
    <li>Zorg dat elementen in de nieuwsbericht div gevuld worden met de corresponderende elementen uit de AJAX call.</li>
</ul>

<pre>
Voorbeeld script:
&lt;script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
&lt;script language="javascript" type="text/javascript">
	$.ajax( {
		url      : 'http://localhost/news/123',
		method   : 'GET',
		dataType : 'json',
		success  : function ( data ) {
			// kleine controle voor de ontwikkelaar
			console.log( data );
			// Vervang de titel van het nieuwsbericht
			$( '.news_holder.title' ).text( data.bericht.title );
		}
	} );
&lt;/script>
</pre>