<?php

class View
{
	private $_model;
	private $_controller;

	public function __construct($controller, $model)
	{
		$this->_controller = $controller;
		$this->_model = $model;
	}

	public function output()
	{
		return '<p>' . $this->_model->string . '</p>';
	}

}
