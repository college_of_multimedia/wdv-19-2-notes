<?php

namespace News;

/**
 * Mijn newsModel class, hierin worden alle gegevens per post opgeslagen
 * Dit is het model van een news object
 */
class Item
{
    /*
     * hier defineer ik een variabele die binnen deze gehele class gebruikt kunnen worden
     * deze variabelen zijn alleen binnen deze class te gebruiken en aan te passen
     *
     */

    /**
     * @var String
     */
    protected $_title;
    /**
     * @var String
     */
    protected $_message;

    /**
     * Sla de title waarde op in dit model
     *
     * @param $value
     */
    public function setTitle($value)
    {
        $this->_title = $value;
    }

    /**
     * Haal de titel op die eerder opgeslagen is in dit model
     *
     * @return String
     */
    public function getTitle()
    {
        if (empty($this->_title)) {
            return '';
        }

        return $this->_title;
    }

    /**
     * Sla de tekst van het bericht op in dit model
     *
     * @param $value
     */
    public function setMessage($value)
    {
        $this->_message = $value;
    }

    /**
     * Haal de tekst van dit bericht op die eerder opgeslagen is in dit model
     *
     * @return String
     */
    public function getMessage()
    {
        if (empty($this->_message)) {
            return '';
        }

        return $this->_message;
    }
}