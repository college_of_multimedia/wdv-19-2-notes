<?php
class MessageController {
	/**
	 * @var \DatabaseClass
	 */
	private $_db;
	
	/**
	 * @param \DatabaseClass $database (Dependency Injection)
	 */
	function __construct($database) {
		$this->_db = $database;
	}

	/**
	 * read a Message 
	 * @param  integer $id 
	 * @return MessageObject
	 */
	public function readMessage( $id ) {
		// zoek in de database naar bericht met id=$id
		// en stuur dit bericht(?) terug 
	}

	/**
	 * store a Message 
	 * @param  MessageObject $message 
	 * @return integer
	 */
	public function storeMessage( $message ) {
		// bewaar in de database het bericht 
		// en stuur het id terug 
	}

}

// $mc = new MessageController($deDatabase);
// $mc->readMessage(12);
// $mc->readMessage( $mc->storeMessage('Bla bla') );
