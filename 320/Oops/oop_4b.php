<?php
$debug = true;

ini_set('display_errors', (int)$debug);
error_reporting(E_ALL);

$nl = "\n";
$br = '<br>' . $nl;

echo '<h2>OOP\'s part  4 b </h2>';

require_once 'ExtendedUser.php';

$user2 = new ExtendedUser('Tom', 12);

echo $user2->getUsername() . $br;

$user2->setUsername('Thomas');

echo $user2->getUsername() . $br;


// echo $user2->_username . $br; ## mag niet is protected of private

class BaseUser {}
class GuestUser extends BaseUser {}
class RegisteredUser extends BaseUser {}
class CustomerUser extends RegisteredUser {}
