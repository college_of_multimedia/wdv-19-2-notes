<?php

require_once 'ProtectedUser.php'; // dependency!

class ExtendedUser extends ProtectedUser { // Class extends BaseClass
	/**
	 * @var integer
	 */
	private $_premiumId;

	/**
	 * constructor van de ExtendeUser overruled de constructor van de protectedUser
	 * @param string $username 
	 * @param integer $id      
	 */
	public function __construct($username, $id)
	{
		$this->setData($username, $id);
	}

	/**
	 * Store de data van de premium user
	 * @param string $username 
	 * @param integer $id      
	 */
	public function setData($username, $id)
	{
		$this->_username = $username;
		$this->_premiumId = $id;
	}

	/**
	 * Deze method Overruled de getUsername method van de parent class (ProtectedUser)
	 * get the username en de premiumId
	 * @return string 
	 */
	public function getUsername () {
		return $this->_username . ' heeft als id:' . $this->_premiumId;
	} 

}