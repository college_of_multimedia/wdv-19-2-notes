<?php

class ProtectedUser { // base class

	/**
	 * @var string
	 */
	protected $_username;

	/**
	 * User constructor
	 * @param string $name 
	 */
	public function __construct( $name )
	{
		$this->setUsername( $name );
	}

	/**
	 * set the username 
	 * @param string $name 
	 */
	public function setUsername ( $name ) {
		$this->_username = $name;
	} 

	/**
	 * get the username 
	 * @return string 
	 */
	public function getUsername () {
		return $this->_username;
	} 

}