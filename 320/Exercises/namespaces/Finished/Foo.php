<?php

namespace Acme\Tools;
// Acme\Tools\Bar
use DateTime; 
use Exception;

class Foo
{
    /**
     * @uses \DateTime
     * @uses \Exception
     * @throws \Exception
     */
    public function doAwesomeThings()
    {
        echo 'Hallo luisteraars!';

       	$dt = new DateTime();
         
       	// DateTime bestaat niet in deze namespace
		print_r ($dt);
    }
}