<html>
<head>
    <title>MVC Example</title>
</head>

<body>
<p>
    Hier zie je een lijst met verschillende boeken.
</p>
<table>
    <tr>
        <th>Titel</th>
        <th>Auteur</th>
        <th>Omschrijving</th>
    </tr>
    <?php
    foreach ($books as $title => $book) {
        echo '<tr><td><a href="?book='.$book->title.'">'.$book->title.'</a></td><td>'.$book->author.'</td><td>'.$book->description.'</td></tr>';
    }
    ?>
</table>

</body>
</html>