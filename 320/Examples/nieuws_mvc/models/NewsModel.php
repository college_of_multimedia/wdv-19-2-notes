<?php
// de NewsModel

class NewsModel 
{
	/**
	 * @var string
	 */
	protected $_title;

	/**
	 * @var string
	 */
	protected $_message;

	/**
	 * @var integer
	 */
	protected $_id;

	public function setId ( $id ) {
		// valideren als nodig!
		$this->_id = $id;
	}

	public function setTitle ( $title ) {
		// valideren als nodig!
		$this->_title = $title;
	}

	public function setMessage ( $message ) {
		// valideren als nodig!
		$this->_message = $message;
	}

	public function getId () {
		return $this->_id;
	}

	public function getTitle () {
		if (empty ($this->_title)) {
			return '';
		}
		return $this->_title;
	}

	public function getMessage () {
		if (empty ($this->_message)) {
			return '';
		}
		return $this->_message;
	}

}